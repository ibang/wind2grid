$(function() {
	//send params by post
	$("a[data-s2]").click(function(e) {
		e.preventDefault();
		var link = $(this);
		$('form#replacer').remove();
		$('body').append($('<form/>')
		  .attr({'action': link.attr('href'), 'method': 'post', 'id': 'replacer'})
		  .append($('<input/>')
			.attr({'type': 'hidden', 'name': 's2', 'value': link.data('s2')})
		  )
		  .append($('<input/>')
			.attr({'type': 'hidden', 'name': 'type', 'value': link.data('type')})
		  )
		).find('#replacer').submit();
		return false;
	});
});	