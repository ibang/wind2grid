function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

$(function() {
	
	var rand = new Array(1,2,3,4,5,6,7,8);
	rand = shuffle(rand);
	var active = rand[0];
	if (active > rand[1]){
		active = rand[1];
	}
	if (active > rand[2]){
		active = rand[2];
	}
	$("div[data-bloque='testimonials'] div.item.item"+active).addClass("active");
	for (var ind = 3, len = rand.length; ind < len; ind++) {
		$("div[data-bloque='testimonials'] div.item.item"+rand[ind]).remove();
	}
});	