$(document).ready(function(){
	$('.typeahead').each(function(){
		var theForm = $(this).closest('form');
		var lang = theForm.find('#lang').val();
		$(this).typeahead({
			minLength: 1,
			source: function (query, result) {
				$.ajax({
					url: "add.php",
					data: 'fn=categorysuggest&lang='+lang+'&query=' + query,            
					dataType: "json",
					type: "POST",
					success: function (data) {
						result($.map(data, function (item) {
							return item;
						}));
					}
				});
			}
		});
	});
	
	$(".typeahead").focus(function() {
		var theForm = $(this).closest('form');
		var lang = theForm.find('#lang').val();
		console.log('focus');
		$(this).typeahead('open');
	});
	
	// type change location and date label change
	$("select[data-change='type']").on('change', function() {
		var theForm = $(this).closest('form');
		var val = this.value;
		if (val=='eventos'){
			$('label#date-'+val).removeClass('d-none');
			$('label#date-news').removeClass('d-none').addClass('d-none');
			$('#location').each(function(){
				$(this).attr('required', true);
				var theLocationContainer = $(this).closest('div.form-group');
				theLocationContainer.removeClass('d-none');
			});
		}
		else{
			$('label#date-'+val).removeClass('d-none');
			$('label#date-eventos').removeClass('d-none').addClass('d-none');
			$('#location').each(function(){
				$(this).attr('required', false);
				var theLocationContainer = $(this).closest('div.form-group');
				theLocationContainer.removeClass('d-none').addClass('d-none');
			});
		}
	});
});