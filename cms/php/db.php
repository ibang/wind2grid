<?php
include_once ("{$DOC_ROOT_CMS}php/ez_sql_core.php");
//include_once ("{$DOC_ROOT_CMS}php/ez_sql_mysql.php");
//include_once ("{$DOC_ROOT_CMS}php/ez_sql_mysqli.php");
include_once ("{$DOC_ROOT_CMS}php/ez_sql_pdo.php");

class DB {

    var $db;

    public function __construct($dbusername='', $userpass='', $dbname='', $dbhost='') {
		if (empty($dbusername) AND empty($dbname)){
			//$this->db = new ezSQL_mysql( DBUSERNAME, DBUSERPASS, DBNAME, DBHOST, 'utf8' );
			//$this->db = new ezSQL_mysqli( DBUSERNAME, DBUSERPASS, DBNAME, DBHOST, 'utf8' );
			$this->db = new ezSQL_pdo( 'mysql:host='.DBHOST.';dbname='.DBNAME.';charset=utf8', DBUSERNAME, DBUSERPASS);
        }
		else{
			//$this->db = new ezSQL_mysql( $dbusername, $userpass, $dbname, $dbhost, 'utf8' );
			//$this->db = new ezSQL_mysqli( $dbusername, $userpass, $dbname, $dbhost, 'utf8' );
			$this->db = new ezSQL_pdo( 'mysql:host='.$dbhost.';dbname='.$dbname.';charset=utf8', $dbusername, $userpass);
		}
		$this->db->debug_all = DB_DEBUG;
    }
    function DB($dbusername='', $userpass='', $dbname='', $dbhost='') {
		self::__construct($dbusername, $userpass, $dbname, $dbhost);
    }
	function get_langs() {
	   	$sql = "SELECT * FROM langs ORDER BY ID ASC";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }
	
	function get_active_langs() {
		$results = array();
	   	$sql = "SELECT * FROM langs WHERE active='Yes' ORDER BY ID ASC";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }
	
	function get_lang_code($lang){
		$sql = "SELECT lang as lang_code FROM langs where id='$lang' ";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0]['lang_code'];
	}
	
	function get_default_lang(){
		$sql = "SELECT lang as lang_code FROM langs WHERE `default`='Yes' ORDER BY id ASC limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0]['lang_code'];
	}
	
	function get_lang_by_code($lang_code){
		$sql = "SELECT * FROM langs WHERE lang='$lang_code' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0];
	}
	
	function get_active_langs_array(){
		$sql = "SELECT lang as lang_code FROM langs WHERE active='Yes' ORDER BY id ASC";
		$langs = $this->db->get_results( $sql, ARRAY_A );
	    $active_langs = array();
		if (!empty($langs)){
			foreach ($langs as $lang_aux){
				$active_langs[] = $lang_aux['lang_code'];
			}
		}
		return $active_langs;
	}
	
	function get_active_international_langs_array(){
		$sql = "SELECT lang as lang_code, lang as localization_url FROM langs WHERE active='Yes' and international='Yes' ORDER BY id ASC";
		$langs = $this->db->get_results( $sql, ARRAY_A );
		return $langs;
	}
	
	function get_active_localizations_code_list(){
		$sql = "SELECT localization_code FROM localizations_langs WHERE active='Yes' ORDER BY id ASC";
		$localizations = $this->db->get_results( $sql, ARRAY_A );
	    $active_localizations = array();
		if (!empty($localizations)){
			foreach ($localizations as $localizations_aux){
				$active_localizations[] = $localizations_aux['localization_code'];
			}
		}
		return $active_localizations;
	}
	
	function get_active_localizations_url_list(){
		$sql = "SELECT DISTINCT localization_url FROM localizations_langs WHERE active='Yes' ORDER BY id ASC";
		$localizations = $this->db->get_results( $sql, ARRAY_A );
	    $active_localizations = array();
		if (!empty($localizations)){
			foreach ($localizations as $localizations_aux){
				$active_localizations[] = $localizations_aux['localization_url'];
			}
		}
		$langs = $this->get_active_langs_array();
		return array_unique(array_merge($active_localizations, $langs));
	}
	
	// para optimizar el tiempo de carga en class.alternate.php
	function get_active_localizations_url_list_data(){
		$sql = "SELECT DISTINCT ll.localization_url, ll.lang, l.lang as lang_code FROM localizations_langs ll INNER JOIN langs l ON ll.lang=l.id WHERE ll.active='Yes' ORDER BY ll.id ASC";
		$localizations = $this->db->get_results( $sql, ARRAY_A );
	    $active_localizations = array();
		if (!empty($localizations)){
			foreach ($localizations as $localizations_aux){
				$active_localizations[$localizations_aux['localization_url']] = $localizations_aux;
			}
		}
		$langs = $this->get_active_langs_array();
		if (!empty($langs)){
		foreach ($langs as $lang){
			if (empty($active_localizations[$lang])){
				$active_localizations[$lang] = array('localization_url'=>$lang, 'lang'=>'', 'lang_code'=>$lang);
				}
			}
		}
		return $active_localizations;
	}
	
	function get_url_from_localization_code($localization_code){
		$sql = "SELECT localization_url FROM localizations_langs WHERE localization_code='$localization_code' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    if (empty($results)){
			return $localization_code;
		}
		return $results[0]['localization_url'];
	}
	
	function get_code_from_localization_url($localization_url){
		$sql = "SELECT localization_code FROM localizations_langs WHERE localization_url='$localization_url' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    if (empty($results)){
			return $localization_url;
		}
		return $results[0]['localization_code'];
	}
	
	function get_lang_from_localization_url($localization_url){
		$sql = "SELECT langs.lang FROM localizations_langs ll INNER JOIN langs ON ll.lang=langs.id WHERE localization_url='$localization_url' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    if (empty($results)){
			return false;
		}
		return $results[0]['lang'];
	}
	# three digits iso code
	function get_available_languages_for_country($country_code){
		$sql = "SELECT DISTINCT langs.lang as lang_code, ll.localization_url FROM localizations_langs ll INNER JOIN langs ON ll.lang=langs.id WHERE country_code='$country_code' AND ll.active='Yes' AND langs.active='Yes' ";
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
	}
	
	# two digits iso code
	function get_available_languages_for_country_iso($iso_country){
		$sql = "SELECT DISTINCT langs.lang as lang_code, ll.localization_url FROM localizations_langs ll INNER JOIN langs ON ll.lang=langs.id WHERE localization_code LIKE '%-".$iso_country."' AND ll.active='Yes' AND langs.active='Yes' ";
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
	}
	
	function get_country_from_localization_url($localization_url){
		$sql = "SELECT country.Code, country.LocalName, country.Name FROM localizations_langs ll INNER JOIN country ON ll.country_code=country.Code WHERE localization_url='$localization_url' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    if (empty($results)){
			return false;
		}
		return $results[0];
	}
	
	function get_country_from_localization_code($localization_code){
		if ($localization_code){
			//addToLog('LOCALIZATION CODE: '. print_r($localization_code, true));
			//Get lang and country code if it is in localization_code value; I use array_pad to avoid NOTICE when only one value is returned by explode, and fill it with "" default value.
			list($lang_code, $code) = array_pad(explode('-', $localization_code, 2), 2, "");
			if ($code){
				$sql = "SELECT country.Code, country.LocalName, country.Name FROM country WHERE country.Code ='$code' OR country.Code2='$code' limit 1";
				$results = $this->db->get_results( $sql, ARRAY_A );
				if (empty($results)){
					return false;
				}
				return $results[0];
			}
		}
		return false;
	}
	
	function get_localization_data($localization_code){
		$sql = "SELECT * FROM localizations_langs WHERE localization_code='$localization_code' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results[0];
	}
	
	function getLocationUnits($lang_location, $language, $field='units_load'){
		if ($lang_location){
			$localization = $this->get_localization_data($lang_location);
			if (!empty($localization[$field])){
				return $localization[$field];
			}
		}
		if ($language){
			$language_data = $this->get_lang_by_code($language);
			if (!empty($language_data[$field])){
				return $language_data[$field];
			}
		}
		return false;
	}
	
	function get_localization_data_from_url($localization_url){
		$sql = "SELECT * FROM localizations_langs WHERE localization_url='$localization_url' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results[0];
	}
	
	function getLocationUnits_from_url($location_url, $language, $field='units_load'){
		if ($location_url){
			$localization = $this->get_localization_data_from_url($location_url);
			if (!empty($localization[$field])){
				return $localization[$field];
			}
		}
		if ($language){
			$language_data = $this->get_lang_by_code($language);
			if (!empty($language_data[$field])){
				return $language_data[$field];
			}
		}
		return false;
	}
	
	function getUnitData($unit='m'){
		$sql = "SELECT * FROM units WHERE unit='$unit' limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0];
	}
	
	function getCountryName($country2code=''){
		if (!trim($country2code)){
			return false;
		}
		$sql = "SELECT LocalName FROM country WHERE Code2='".strtoupper($country2code)."' limit 1";
		$results = $this->db->get_row( $sql, ARRAY_A );
		if (empty($results)){
			return false;
		}
	    return $results['LocalName'];
		
	}
	
// --------------------- Frontend Home ----------------------

	function get_news_home($lang, $type='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				$order_by = ' n.`date` ASC, ';
				$where_type.= ' AND n.`date` > NOW() ';
			}
		}
		$sql = "SELECT n.*, l.lang AS lang_code FROM news n LEFT JOIN langs l ON (l.id = n.lang) WHERE l.lang='$lang' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` DESC LIMIT 0,2"; //active=1 -> Publiqued, active=2 -> Draft
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }		

	function get_projects_home($lang, $quantity=2) {
		$results = array();
		if (!empty($lang) AND !empty($quantity)){
			$sql = "SELECT p.*, l.lang AS lang_code, 'projects' AS type FROM projects p LEFT JOIN langs l ON (l.id=p.lang) WHERE l.lang='$lang' AND p.active='Yes' ORDER BY p.year DESC, p.code DESC LIMIT 0,".$quantity; //active=1 -> Publiqued, active=2 -> Draft
			//echo $sql;
			$results = $this->db->get_results( $sql, ARRAY_A );
	    }
		return $results;
    }
	
	function get_front_industries_list($lang, $in_project=false) {
		if ($in_project){
			$sql = "SELECT i.*, l.lang AS lang_code FROM projects_has_industries phi INNER JOIN industries i ON (i.code=phi.industries_code) INNER JOIN projects p ON (p.code=phi.projects_code) LEFT JOIN langs l ON (l.id=i.lang AND l.id=p.lang) WHERE l.lang='$lang' AND i.active='Yes' AND p.active='Yes' GROUP BY i.code ORDER BY i.order ASC";
		}
		else{
			$sql = "SELECT p.*, langs.lang AS lang_code FROM industries p LEFT JOIN langs ON (langs.id=p.lang) WHERE langs.lang='$lang' AND p.active='Yes' ORDER BY p.code ASC";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }

// --------------------- Frontend Downloads ----------------------
	
    function count_front_downloads_list($lang, $logged=false) {
        $sql = "SELECT downloads.id, langs.lang as lang_code FROM downloads n left join langs on (langs.id=downloads.lang) WHERE langs.lang='$lang' AND downloads.active='Yes' AND n.is_public IN('all',".($logged?"'private'":"'public'").") ORDER BY `order` asc, `date` desc, `added` desc";
            //echo $sql;
            $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }
    
    function get_front_downloads_list_OLD($lang,$limit, $logged=false) {
        $sql = "SELECT d.*, l.lang as lang_code FROM downloads d INNER JOIN downloadscategories dc ON (dc.code=d.downloadscategories_code AND d.lang=dc.lang) left join langs l on (l.id=d.lang) WHERE l.lang='$lang' AND d.is_public IN('all',".($logged?"'private'":"'public'").") AND d.active='Yes' ORDER BY d.`order` asc,  d.`date` desc, d.`added` desc $limit";
        //echo $sql;
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }
    
    function count_downloadscategory_downloads_list($lang, $downloadscategory_code, $search='', $all = true, $logged=false) {
        $results = array();
        $categoriesLang_array = $this->get_lang_by_code($lang);
        $categoriesLang = $categoriesLang_array['id'];
        if ($all){
            // get all subcategories of given category
            $categories = $this->get_all_downloadscategories_descendants($downloadscategory_code);
            $sql = "SELECT n.id, l.lang as lang_code FROM downloads n INNER JOIN downloadscategories pc ON (n.downloadscategories_code=pc.code) left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND pc.code IN ($categories) AND n.is_public IN('all',".($logged?"'private'":"'public'").") AND pc.lang='".$categoriesLang."' AND pc.active='Yes' AND n.active='Yes' ".(!empty($search)?"AND (n.headline LIKE '%".$search."%' OR n.slug LIKE '%".$search."%' OR n.file1 LIKE '%".$search."%')":"")." GROUP BY n.id ORDER BY n.`date` DESC, pc.lft ASC, n.added desc";
        }
        else{
            $sql = "SELECT n.id, l.lang as lang_code FROM downloads n INNER JOIN downloadscategories pc ON (n.downloadscategories_code=pc.code) left join langs l on (l.id=n.lang) WHERE l.lang='$lang' and pc.code=$downloadscategory_code AND n.is_public IN('all',".($logged?"'private'":"'public'").") AND pc.lang='".$categoriesLang."' AND pc.active='Yes' AND n.active='Yes' ".(!empty($search)?"AND (n.headline LIKE '%".$search."%' OR n.slug LIKE '%".$search."%' OR n.file1 LIKE '%".$search."%')":"")." GROUP BY n.id ORDER BY n.`date` DESC, pc.lft ASC, n.added desc";
        }
        //addToLog('downloads IN CATEGORY: '.$sql);
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }

    function get_front_downloads_list($lang, $limit='', $search='') {
        $results = array();
        $categoriesLang_array = $this->get_lang_by_code($lang);
        $categoriesLang = $categoriesLang_array['id'];
        
        $sql = "SELECT n.*, l.lang as lang_code FROM downloads n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND n.active='Yes' ".(!empty($search)?"AND (n.headline LIKE '%".$search."%' OR n.slug LIKE '%".$search."%' OR n.file1 LIKE '%".$search."%')":"")." GROUP BY n.id ORDER BY n.`date` DESC, n.added desc $limit";
        //echo $sql;
        //addToLog('DOWNLOADSCATEGORY DOWNLOADS LIST: '.$sql);
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }
    
    function get_downloadscategory_downloads_list($lang, $downloadscategory_code, $limit='', $search='', $all=true, $logged=false) {
        $results = array();
        $categoriesLang_array = $this->get_lang_by_code($lang);
        $categoriesLang = $categoriesLang_array['id'];
        if ($all){
            // get all subcategories of given category
            $categories = $this->get_all_downloadscategories_descendants($downloadscategory_code);
            $sql = "SELECT n.*, l.lang as lang_code FROM downloads n INNER JOIN downloadscategories pc ON (pc.code=n.downloadscategories_code) left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND n.is_public IN('all',".($logged?"'private'":"'public'").") AND pc.code IN ($categories) AND pc.lang='".$categoriesLang."' AND pc.active='Yes' AND n.active='Yes' ".(!empty($search)?"AND (n.headline LIKE '%".$search."%' OR n.slug LIKE '%".$search."%' OR n.file1 LIKE '%".$search."%')":"")." GROUP BY n.id ORDER BY n.`date` DESC, pc.lft ASC, n.added desc $limit";
        }
        else{
            $sql = "SELECT n.*, l.lang as lang_code FROM downloads n INNER JOIN downloadscategories pc ON (pc.code=n.downloadscategories_code) left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND n.is_public IN('all',".($logged?"'private'":"'public'").") AND pc.code=$downloadscategory_code AND pc.lang='".$categoriesLang."' AND pc.active='Yes' AND n.active='Yes' ".(!empty($search)?"AND (n.headline LIKE '%".$search."%' OR n.slug LIKE '%".$search."%' OR n.file1 LIKE '%".$search."%')":"")." GROUP BY n.id ORDER BY n.`date` DESC, pc.lft ASC, n.added desc $limit";
        }
        //echo $sql;
        //addToLog('DOWNLOADSCATEGORY DOWNLOADS LIST: '.$sql);
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }
    
    function get_downloads_sheet($slug,$lang, $logged=false) {
        $sql = "SELECT * from downloads n left join langs on (langs.id=n.lang) WHERE langs.lang='$lang' AND n.is_public IN('all',".($logged?"'private'":"'public'").") AND n.slug='$slug'  AND n.active='Yes'";
        //echo $sql;
        $results = $this->db->get_row( $sql, ARRAY_A );
        return $results;
    }
    
    function get_downloadscategory_sheet($slug,$lang, $strict=false) {
        $results = array();
        $sql = "SELECT pc.*, l.lang as lang_code from downloadscategories pc left join langs l on (l.id=pc.lang) WHERE pc.`slug`='$slug'  AND pc.`active`='Yes'";
        $results_aux = $this->db->get_results( $sql, ARRAY_A );
        
        if ($strict){
            if (!empty($results_aux)){
                foreach($results_aux as $current){
                    if ($current['lang_code']==$lang){
                        return $current;
                    }
                }
            }
        }
        elseif(!empty($results_aux)){
            foreach($results_aux as $current){
                if ($current['lang_code']==$lang){
                    $results = $current;
                }
            }
            if (empty($results)){
                return $results_aux[0];
            }
        }
        return $results;
    }
    
    function get_downloadscategory_langs($lang, $downloadscategory){
        $langs = $this->get_active_langs();
        $downloadscategory_lang_slug[$lang] = $downloadscategory;
        $sql = "SELECT c.id, c.code, l.lang as lang_code FROM downloadscategories c left join langs l on (l.id=c.lang) WHERE c.`slug`='$downloadscategory' ORDER BY c.code desc";
        $downloadscategory_lang = $this->db->get_results( $sql, ARRAY_A );
        if (!empty($downloadscategory_lang)){
            $downloadscategory_lang_slug[$downloadscategory_lang[0]['lang_code']] = $downloadscategory;
            foreach ($langs as $lang_aux){
                if ($lang_aux['lang']!=$downloadscategory_lang[0]['lang_code']){
                    foreach ($downloadscategory_lang as $downloadscategory){
                        if ($the_category = $this->get_downloadscategory_lang($lang_aux['id'],$downloadscategory['code']) ){
                            $downloadscategory_lang_slug[$lang_aux['lang']] = $the_category['slug'];
                            break;
                        }
                    }
                }
            }
        }
        
        return $downloadscategory_lang_slug;
    }
    
    function get_downloads_sheet_langs($lang, $slug, $logged=false){
        $langs = $this->get_active_langs();
        $article_lang_slug[$lang] = $slug;
        $article = $this->get_downloads_sheet($slug,$lang, $logged);
        if (!empty($article)){
            foreach ($langs as $lang_aux){
                if ($lang_aux['lang']!=$lang){
                    if ($the_article = $this->get_active_downloads_lang($lang_aux['id'],$article['code'], $logged=false) ){
                        $article_lang_slug[$lang_aux['lang']] = $the_article['slug'];
                    }
                }
            }
        }
        return $article_lang_slug;
    }
    
    function get_active_downloads_lang($lang,$code, $logged=false) {
        if ($lang!='general'){
            $sql = "SELECT * FROM downloads where lang=$lang and code=$code and active='Yes' AND is_public IN('all',".($logged?"'private'":"'public'").")";
        }
        else{
            $sql = "SELECT * FROM downloads where code=$code and active='Yes' AND is_public IN('all',".($logged?"'private'":"'public'").") limit 1";
        }
        //echo $sql;
        $results = $this->db->get_row( $sql, ARRAY_A );
        return $results;
    }
    
    function get_downloadscategory_lang($lang, $code, $strict=false){
        if ($lang!='general'){
            $sql = "SELECT * FROM downloadscategories where lang=$lang and code=$code ";
            $results = $this->db->get_row( $sql, ARRAY_A );
            if (empty($results) AND !$strict){
                $langaux = $this->get_downloadscategories_langaux();
                if ($langaux and $langaux!=$lang){
                    $sql = "SELECT * FROM downloadscategories where lang=$langaux and code=$code ";
                    $results = $this->db->get_row( $sql, ARRAY_A );
                    if (!empty($results)){
                        $results['lang'] = $lang;
                        $results['active'] = 'Draft';
                    }
                }
            }
        }
        else{
            $sql = "SELECT * FROM downloadscategories where code=$code limit 1";
            $results = $this->db->get_row( $sql, ARRAY_A );
        }
        return $results;
    }
    
    function get_downloads_downloadscategories_list($downloads_list){
        $results = array();
        $downloads_downloadscategories = array();
        if (!empty($downloads_list)){
            foreach($downloads_list as $i => $result){
                if (empty($downloads_downloadscategories[$result['downloadscategories_code']])){
                    $downloads_downloadscategories[$result['downloadscategories_code']] = $this->get_downloadscategory_lang($result['lang'], $result['downloadscategories_code']);
                }
                $result['downloadscategories'] = $downloads_downloadscategories[$result['downloadscategories_code']];
                
                $results[$i] = $result;
            }
        }
        return $results;
    }

    function get_downloadscategories($lang) {
        $sql = "SELECT d.category, l.lang AS lang_code FROM dowloads d LEFT JOIN langs l ON (l.id=d.lang ) WHERE l.lang='$lang'  AND d.active='Yes' GROUP BY category";
        //echo $sql;
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }

// --------------------- Frontend News ----------------------	
	
	function get_news_categories($lang, $type='') {
		$where_type = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
		}
		$sql = "SELECT n.category, n.category_slug, l.lang AS lang_code FROM news n LEFT JOIN langs l ON (l.id=n.lang ) WHERE l.lang='$lang'  AND n.active='Yes' ".$where_type." GROUP BY category";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }	
	
	function get_latest_news($lang, $type='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				$order_by = ' n.`date` ASC, ';
				$where_type.= ' AND n.`date` > NOW() ';
			}
		}
		$sql = "SELECT n.id, n.headline, n.slug, l.lang AS lang_code FROM news n LEFT JOIN langs l ON (l.id=n.lang) WHERE l.lang='$lang' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` DESC LIMIT 0,5";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }
	
	function count_front_news_list($lang, $type='', $past='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				if ($past!='past'){
					$order_by = ' n.`date` ASC, ';
					$where_type.= ' AND n.`date` > NOW() ';
				}
				else{
					$order_by = ' n.`date` DESC, ';
					$where_type.= ' AND n.`date` < NOW() ';
				}
			}
		}
		$sql = "SELECT n.id, l.lang as lang_code FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` desc";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_front_news_list($lang,$limit, $type='', $past='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				if ($past!='past'){
					$order_by = ' n.`date` ASC, ';
					$where_type.= ' AND n.`date` > NOW() ';
				}
				else{
					$order_by = ' n.`date` DESC, ';
					$where_type.= ' AND n.`date` < NOW() ';
				}
			}
		}
		$sql = "SELECT n.*, l.lang as lang_code FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` desc $limit";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function count_category_news_list($lang,$category, $type='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				$order_by = ' n.`date` ASC, ';
				$where_type.= ' AND n.`date` > NOW() ';
			}
		}
		$sql = "SELECT n.id, l.lang as lang_code FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' and n.category_slug='$category' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` desc";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_category_news_list($lang,$category,$limit, $type='') {
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				$order_by = ' n.`date` ASC, ';
				$where_type.= ' AND n.`date` > NOW() ';
			}
		}
		$sql = "SELECT n.*, l.lang as lang_code FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' and n.category_slug='$category' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` desc $limit";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_news_article($slug,$lang, $type='') {
		$where_type = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
		}
		$sql = "SELECT n.*, l.lang as lang_code from news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' and n.slug='$slug' AND n.active='Yes' ".$where_type.";";
		//echo $sql;
		$results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }

	function get_news_category_langs($lang, $category, $type=''){
		$where_type = '';
		$order_by = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
			if ($type=='news'){
				$order_by = ' n.`date` DESC, ';
			}
			else{
				$order_by = ' n.`date` ASC, ';
				$where_type.= ' AND n.`date` > NOW() ';
			}
		}
		$langs = $this->get_active_langs();
		$category_lang_slug[$lang] = $category;
		$sql = "SELECT n.id, n.code, l.lang as lang_code FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' and n.category_slug='$category' AND n.active='Yes' ".$where_type." ORDER BY ".$order_by." n.`code` desc";
		$category_lang_news = $this->db->get_results( $sql, ARRAY_A );
		if (!empty($category_lang_news)){
			foreach ($langs as $lang_aux){
				if ($lang_aux['lang']!=$lang){
					foreach ($category_lang_news as $new){
						if ($the_new = $this->get_active_news_lang($lang_aux['id'],$new['code']) ){
							$category_lang_slug[$lang_aux['lang']] = $the_new['category_slug'];
							break;
						}
					}
				}
			}
		}
		
		return $category_lang_slug;
	}

	function get_news_article_langs($lang, $slug, $type=''){
		$where_type = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
		}
		$langs = $this->get_active_langs();
		$article_lang_slug[$lang] = $slug;
		$new = $this->get_news_article($slug, $lang, $type);
		if (!empty($new)){
			foreach ($langs as $lang_aux){
				if ($lang_aux['lang']!=$lang){
					if ($the_new = $this->get_active_news_lang($lang_aux['id'],$new['code']) ){
						$article_lang_slug[$lang_aux['lang']] = $the_new['slug'];
					}
				}
			}
		}
		return $article_lang_slug;
	}
	
	function get_active_news_lang($lang,$code) {
		if ($lang!='general'){
			$sql = "SELECT * FROM news where lang=$lang and code=$code and active='Yes'";
		}
		else{
			$sql = "SELECT * FROM news where code=$code and active='Yes' limit 1";
		}
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }
	
	// news images
	function get_news_images_list($news_list){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_images = $this->get_news_images($result['lang'], $result['code']);
				if (!empty($news_images)){
					$result['image1'] = $news_images[0]['image'];
					$result['alt-image1'] = $news_images[0]['alt-image'];
					unset($news_images[0]);
				}
				if (!empty($news_images)){
					$result['images']=$news_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_news_images($lang, $code){
		if ($lang!='general'){
			$sql = "SELECT * FROM news_images where lang=$lang and news_code=$code ORDER BY `is-main` ASC";
		}
		else{
			$sql = "SELECT * FROM news_images where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );
		if ($lang=='general'){
			$langaux = 0;
			if (!empty($results)){
				foreach ($results as $id => $current){
					if (!$langaux){
						$langaux = $current['lang'];
					}
					if ($current['lang']!=$langaux){
						unset($results[$id]);
					}
				}
			}
		}
		return $results;
	}
	
	// news files
	function get_news_files_list($news_list){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_files = $this->get_news_files($result['lang'], $result['code']);
				if (!empty($news_files)){
					$result['file1'] = $news_files[0]['file'];
					$result['link-file1'] = $news_files[0]['link-file'];
					unset($news_files[0]);
				}
				if (!empty($news_files)){
					$result['files']=$news_files;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_news_files($lang, $code){
		if ($lang!='general'){
			$sql = "SELECT * FROM news_files where lang=$lang and news_code=$code ORDER BY `is-main` ASC";
		}
		else{
			$sql = "SELECT * FROM news_files where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );
		if ($lang=='general'){
			$langaux = 0;
			if (!empty($results)){
				foreach ($results as $id => $current){
					if (!$langaux){
						$langaux = $current['lang'];
					}
					if ($current['lang']!=$langaux){
						unset($results[$id]);
					}
				}
			}
		}
		return $results;
	}


// --------------------- Frontend Projects ----------------------		
	function get_latest_projects($lang) {
	   	$sql = "SELECT p.id, p.headline, p.slug, p.fase, p.estado, p.unidades, langs.lang as lang_code, p.`image1`, p.`alt-image1` FROM projects p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang'  AND p.active='Yes' ORDER BY year DESC, p.code DESC LIMIT 0,5";
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_random_featured_projects($lang, $quantity=2) {
		$results = array();
		if (!empty($lang) AND !empty($quantity)){
			$sql = "SELECT p.*, l.lang as lang_code, 'projects' as type FROM projects p left join langs l on (l.id=p.lang) WHERE l.lang='$lang' AND p.active='Yes' AND p.featured='Yes' ORDER BY RAND() LIMIT 0,".$quantity; //active=1 -> Publiqued, active=2 -> Draft
			//echo $sql;
			$results = $this->db->get_results( $sql, ARRAY_A );
	    }
		return $results;
    }
	
	function get_featured_projects($lang, $quantity=2) {
		$results = array();
		if (!empty($lang) AND !empty($quantity)){
			$sql = "SELECT p.*, l.lang as lang_code, 'projects' as type FROM projects p left join langs l on (l.id=p.lang) WHERE l.lang='$lang' AND p.active='Yes' AND p.featured='Yes' ORDER BY p.year DESC, p.code DESC LIMIT 0,".$quantity; //active=1 -> Publiqued, active=2 -> Draft
			//echo $sql;
			$results = $this->db->get_results( $sql, ARRAY_A );
	    }
		return $results;
    }
	
	function count_front_projects_list($lang, $industry_code='') {
		$group_by = '';
		$extra_join = '';
		if ($industry_code){
			if ($industry_code){
				$extra_join.=" INNER JOIN projects_has_industries phi ON (phi.projects_code=p.code AND phi.industries_code='$industry_code') ";
				
			}
		}
		$sql = "SELECT p.id, l.lang as lang_code FROM projects p ".$extra_join." LEFT JOIN langs l ON (l.id=p.lang) WHERE l.lang='$lang' AND p.active='Yes' ".$group_by." ORDER BY year desc, p.code DESC";
		//addToLog($sql);
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
	}
	
	function get_front_projects_list($lang,$limit, $industry_code='') {
		$group_by = '';
		$extra_join = '';
		if ($industry_code){
			if ($industry_code){
				$extra_join.=" INNER JOIN projects_has_industries phi ON (phi.projects_code=p.code AND phi.industries_code='$industry_code') ";
				
			}
		}
		$sql = "SELECT p.*, l.lang as lang_code FROM projects p ".$extra_join." LEFT JOIN langs l ON (l.id=p.lang) WHERE l.lang='$lang' AND p.active='Yes' ".$group_by." ORDER BY year DESC, p.code DESC $limit";
		
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
	}
	
	function get_projects_sheet($slug,$lang) {
		$sql = "SELECT p.* from projects p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' and slug='$slug'  AND p.active='Yes'";
		$results = $this->db->get_row( $sql, ARRAY_A );
		return $results;
    }

	function get_projects_sheet_langs($lang, $slug){
		$langs = $this->get_active_langs();
		$article_lang_slug[$lang] = $slug;
		$project = $this->get_projects_sheet($slug,$lang);
		if (!empty($project)){
			foreach ($langs as $lang_aux){
				if ($lang_aux['lang']!=$lang){
					if ($the_project = $this->get_active_projects_lang($lang_aux['id'],$project['code']) ){
						$article_lang_slug[$lang_aux['lang']] = $the_project['slug'];
					}
				}
			}
		}
		return $article_lang_slug;
	}
	
	function get_active_projects_lang($lang,$code) {
		if ($lang!='general'){
			$sql = "SELECT * FROM projects where lang=$lang and code=$code and active='Yes' ";
	    }
		else{
				$sql = "SELECT * FROM projects where code=$code and active='Yes' limit 1";
		}
		//echo $sql;
		$results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_projects_images_list($projects_list){
		$results = array();
		if (!empty($projects_list)){
			foreach($projects_list as $i => $result){
				$projects_images = $this->get_projects_images($result['lang'], $result['code']);
				if (!empty($projects_images)){
					// si no tiene imagen principal, cogemos la primera imagen de la galería.
					if (empty($result['image1'])){
						$result['image1'] = $projects_images[0]['image'];
						$result['alt-image1'] = $projects_images[0]['alt-image'];
						unset($projects_images[0]);
					}
				}
				if (!empty($projects_images)){
					$result['images']=$projects_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_projects_images($lang, $code){
		if ($lang!='general'){
			$sql = "SELECT * FROM projects_images where lang=$lang and projects_code=$code ORDER BY `is-main` ASC";
		}
		else{
			$sql = "SELECT * FROM projects_images where projects_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );
		if ($lang=='general'){
			$langaux = 0;
			if (!empty($results)){
				foreach ($results as $id => $current){
					if (!$langaux){
						$langaux = $current['lang'];
					}
					if ($current['lang']!=$langaux){
						unset($results[$id]);
					}
				}
			}
		}
		return $results;
	}
	
	function count_industries_projects_list($lang,$industry) {
	   $sql = "SELECT n.id, langs.lang as lang_code FROM projects_has_industries nc INNER JOIN industries c ON (nc.industries_code=c.code) INNER JOIN projects n ON n.code=nc.projects_code left join langs on (langs.id=n.lang) WHERE langs.lang='$lang' and c.slug='$industry' AND n.active='Yes' GROUP BY n.id  ORDER BY n.`date` desc, n.`code` desc";
	   //echo $sql;
	   $results = $this->db->get_results( $sql, ARRAY_A );
	   return $results;
    }
	
	function get_industries_projects_list($lang,$industry,$limit='') {
		$results = array();
		$sql = "SELECT n.*, langs.lang as lang_code, 'projects' as type FROM projects_has_industries nc INNER JOIN projects n ON(nc.projects_code=n.code) INNER JOIN industries c ON (c.code=nc.industries_code) left join langs on (langs.id=n.lang) WHERE langs.lang='$lang' and c.`slug`='$industry' AND n.active='Yes' GROUP BY n.id ORDER BY n.`date` desc, n.`code` desc $limit";
		//echo $sql;
		$results_aux = $this->db->get_results( $sql, ARRAY_A );
		if (!empty($results_aux)){
			foreach ($results_aux as $element){
				$thetime = strtotime($element['date']);
				while (!empty($results[$thetime])){
					$thetime=$thetime-1;
				}
				$results[$thetime] = $element;
			}
		}
	    return $results;
    }
	
	function get_industries_sheet($slug,$lang) {
		$sql = "SELECT p.* from industries p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' and slug='$slug'  AND p.active='Yes'";
		$results = $this->db->get_row( $sql, ARRAY_A );
		return $results;
    }
	
	function get_industries_langs($lang, $industry){
		$langs = $this->get_active_langs();
		$industry_lang_slug[$lang] = $industry;
		$sql = "SELECT c.id, c.code, langs.lang as lang_code FROM industries c left join langs on (langs.id=c.lang) WHERE langs.lang='$lang' and c.`slug`='$industry' ORDER BY c.code desc";
		$industry_lang = $this->db->get_results( $sql, ARRAY_A );
		if (!empty($industry_lang)){
			foreach ($langs as $lang_aux){
				if ($lang_aux['lang']!=$lang){
					foreach ($industry_lang as $industry){
						if ($the_industry = $this->get_industry_lang($lang_aux['id'],$industry['code']) ){
							$industry_lang_slug[$lang_aux['lang']] = $the_industry['slug'];
							break;
						}
					}
				}
			}
		}
		
		return $industry_lang_slug;
	}
	
	function get_projects_industries_list($projects_list){
		$results = array();
		if (!empty($projects_list)){
			foreach($projects_list as $i => $result){
				$projects_industries = $this->get_projects_industries($result['lang'], $result['code']);
				/*
				if (!empty($projects_industries)){
					foreach ($projects_industries as $current){
						$result['industries'][$current['type']][]=$current;
						if ($current['type']=='Chef' OR $current['type']=='Person'){
							$result['industries']['Human'][]=$current;
						}
					}
				}
				*/
				if (!empty($projects_industries)){
					$result['main-industry'] = $projects_industries[0];
					unset($projects_industries[0]);
				}
				if (!empty($projects_industries)){
					$result['industries']=$projects_industries;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_projects_industries($lang, $code){
		if ($lang!='general'){
			$sql = "SELECT * FROM projects_has_industries ic INNER JOIN industries c ON (c.code=ic.industries_code AND c.lang=$lang) where ic.projects_code=$code ORDER BY c.`headline` ASC";
		}
		else{
			$sql = "SELECT * FROM projects_has_industries ic INNER JOIN industries c ON (c.code=ic.industries) where ic.industries_code=$code ORDER BY c.`lang` ASC, c.`headline` ASC";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );
		if ($lang=='general'){
			$langaux = 0;
			if (!empty($results)){
				foreach ($results as $id => $current){
					if (!$langaux){
						$langaux = $current['lang'];
					}
					if ($current['lang']!=$langaux){
						unset($results[$id]);
					}
				}
			}
		}
		return $results;
	}
	
	
	function get_related_projects($article, $lang){
		$where_products='';
		
		$quedan = 3;
		$notIn = $article['code'].',';
		
		$notIn = rtrim($notIn, ',');
		if ($quedan){
			$sql = "SELECT distinct(p.id) as idpry, p.*, langs.lang as lang_code, 'projects' as type from projects p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' and p.code NOT IN (".$notIn.") AND p.active='Yes' ORDER BY p.year desc, p.code desc LIMIT ".$quedan;
			$results_b = $this->db->get_results( $sql, ARRAY_A );
			if (!empty($results)){
				if (!empty($results_b)){
					$results = array_merge($results, $results_b);
				}
			}
			else{
				$results = $results_b;
			}
		}
	    return $results;
	}
	
	function get_related_industries_projects($lang, $industry_code=''){
		$where_industries='';
		if (!empty($industry_code)){
			$where_industries.=" AND nhp.industries_code='".$industry_code."'";
		}
		$sql = "SELECT distinct(p.id) as idpry, p.*, langs.lang as lang_code, 'projects' as type from projects_has_industries nhp INNER JOIN projects p ON p.code=nhp.projects_code left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' ".$where_industries." AND p.active='Yes' ORDER BY p.year desc, p.code desc LIMIT 3";
	    //echo $sql;
	    $results = $this->db->get_results( $sql, ARRAY_A );
		$quedan = 3;
		$notIn = '0';
		if (!empty($results)){
			$quedan = 3 - count($results);
			if ($quedan){
				foreach ($results as $result){
					$notIn.= $result['code'].',';
				}
			}
		}
		$notIn = rtrim($notIn, ',');
		if ($quedan){
			$sql = "SELECT distinct(p.id) as idpry, p.*, langs.lang as lang_code, 'projects' as type from projects p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' and p.code NOT IN (".$notIn.") AND p.active='Yes' ORDER BY p.year desc, p.code desc LIMIT ".$quedan;
			$results_b = $this->db->get_results( $sql, ARRAY_A );
			if (!empty($results)){
				if (!empty($results_b)){
					$results = array_merge($results, $results_b);
				}
			}
			else{
				$results = $results_b;
			}
		}
	    return $results;
	}

// --------------------- Frontend contacts ----------------------	

function add_contacts($contacts) {
	$result = false;
	if (empty($contacts['id'])){
		$sql = "insert into contacts (`firstname`,`lastname`, `email`, `sector`,`message`, `send-date`, `about`, `lang_name`, `company`, `type`, `location_name`, `dataid`, `googlecookie`, `gclid`, `gmode`, `request`, `ip`, `policies_code`, `policies_revision_added`) values ('".$contacts["firstname"]."','".$contacts["lastname"]."','".$contacts["email"]."','".$contacts["sector"]."','".$contacts["message"]."',NOW(), '".$contacts["about"]."', '".$contacts["lang_name"]."','".$contacts["company"]."', '".$contacts["type"]."', '".$contacts["location_name"]."', '".$contacts["dataid"]."', ".(!empty($contacts['googlecookie'])?"'".$contacts['googlecookie']."'":"NULL").", ".(!empty($contacts['gclid'])?"'".$contacts['gclid']."'":"NULL").", ".(!empty($contacts['gmode'])?"'".$contacts['gmode']."'":"NULL").", '".MAINHOST.$contacts['request']."', '".$contacts["ip"]."', ".(!empty($contacts["policies_code"])?"'".$contacts["policies_code"]."'":"NULL").", '".$contacts["policies_revision_added"]."');";
	}
	else{ //activate
		$sql = "update contacts set `firstname`='".$contacts["firstname"]."', `lastname`='".$contacts["lastname"]."', `email`='".$contacts["email"]."', `sector`='".$contacts["sector"]."', `message`='".$contacts["message"]."', `send-date`=NOW(), `about`='".$contacts["about"]."', `lang_name`='".$contacts["lang_name"]."', `company`='".$contacts["company"]."', `type`='".$contacts["type"]."', `location_name`='".$contacts["location_name"]."', `request`='".MAINHOST.$contacts['request']."', `googlecookie`=".(!empty($contacts['googlecookie'])?"'".$contacts['googlecookie']."'":"NULL").", `gclid`=".(!empty($contacts['gclid'])?"'".$contacts['gclid']."'":"NULL").", `gmode`=".(!empty($contacts['gmode'])?"'".$contacts['gmode']."'":"NULL").", `ip`='".$contacts["ip"]."', `policies_code`=".(!empty($contacts['policies_code'])?"'".$contacts['policies_code']."'":"NULL").", `policies_revision_added`='".$contacts["policies_revision_added"]."' WHERE `id`=".$contacts['id']." AND `dataid`='".$contacts["dataid"]."';";
		
	}
	$result = $this->db->query( $sql );
	//addToLog($sql);
	return ($result!==false);
}

function formContactExists($dataid = 0){
	if ($dataid){
		$sql = "SELECT id FROM contacts WHERE dataid IS NOT NULL AND dataid='".$dataid."' limit 1";
		$results = $this->db->get_row( $sql, ARRAY_A );
		if (empty($results)){
			return false;
		}
	    return $results['id'];
	}
	return false;
}

function formContactCreate($dataid=0){
	if ($dataid){
		$sql = "INSERT into contacts (`send-date`, `dataid`) VALUES (NOW(), '".$dataid."');";
		$result = $this->db->query( $sql );
		if ($result){
			return $this->db->insert_id;
		}
	}
	return false;
}

function clearContact($dataid=0){
	if ($dataid){
		$sql = "delete from contacts where dataid IS NOT NULL AND dataid='".$dataid."' ";
		$result = $this->db->query( $sql );
		return ($result!==false);
	}
}


function get_country_list(){
	$sql = "SELECT c.Code, c.LocalName FROM country c order by c.LocalName asc ";
	// echo $sql;
	$results = $this->db->get_results( $sql, ARRAY_A );
	return $results;
}

function get_country_provinces($country_code=''){
	$where_country = $country_code?" WHERE p.country_code ='".$country_code."' ":"";
	$sql = "SELECT p.id, p.province FROM provinces p ".$where_country." order by p.province asc ";
	// echo $sql;
	$results = $this->db->get_results( $sql, ARRAY_A );
	return $results;
}

function get_country($country_code=''){
	$sql = "SELECT * FROM country WHERE Code='".$country_code."' limit 1";
	$results = $this->db->get_results( $sql, ARRAY_A );
	return $results[0];
}

function get_province($province_code=''){
	$sql = "SELECT * FROM provinces WHERE id='".$province_code."' limit 1";
	$results = $this->db->get_results( $sql, ARRAY_A );
	return $results[0];
}

// --------------------- Frontend Policies -------------------------
	
function get_policies_row_langs($lang, $slug){
	$langs = $this->get_active_langs();
	$policies_lang_slug[$lang] = $slug;
	$sql = "SELECT c.id, c.code, langs.lang as lang_code FROM policies c left join langs on (langs.id=c.lang) WHERE langs.lang='$lang' and c.`slug`='$slug' ORDER BY c.active ASC, c.added DESC, c.code DESC";
	$policies_lang = $this->db->get_results( $sql, ARRAY_A );
	if (!empty($policies_lang)){
		foreach ($langs as $lang_aux){
			if ($lang_aux['lang']!=$lang){
				foreach ($policies_lang as $current){
					if ($the_policies = $this->get_policies_row_lang($lang_aux['id'],$current['code']) ){
						$policies_lang_slug[$lang_aux['lang']] = $the_policies['slug'];
						break;
					}
				}
			}
		}
	}
	
	return $policies_lang_slug;
}

function get_policies_row_lang($lang, $code){
	if ($lang!='general'){
		$sql = "SELECT * FROM policies where lang=$lang and code=$code ";
	}
	else{
		$sql = "SELECT * FROM policies where code=$code ORDER BY active ASC, added DESC limit 1";
	}
   
	$results = $this->db->get_row( $sql, ARRAY_A );
	return $results;
}

function get_policies_article($lang){
	$sql = "SELECT a.*, l.lang as lang_code from policies a left join langs l on (l.id=a.lang) WHERE l.lang='$lang' AND a.active='Yes' ORDER BY a.added DESC, a.code DESC LIMIT 1";
	
	$results = $this->db->get_row( $sql, ARRAY_A );
	return $results;
}
	

// --------------------- CMS news ----------------------	

	function get_news_codes($type) {
		$where_type = '';
		if (!empty($type)){
			$where_type = " WHERE n.`type`='".$type."' ";
		}
	   	$sql = "SELECT DISTINCT n.code FROM news n ".$where_type." order by n.`date` desc, n.code desc";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_news_list($code) {
	   	$sql = "SELECT news.id, news.code, news.lang, news.headline, news.`date`, news.category, news.location, news.`file-linktext`, langs.lang as lang_code, news.active, news.file, news.type FROM news left join langs on (news.lang=langs.id) where code=$code order by news.lang asc, news.`date` desc, news.code desc ";
	    // echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_news_limit_list($limit='', $type="") {
		$where_type = '';
		if (!empty($type)){
			$where_type = " WHERE n.`type`='".$type."' ";
		}
	   	$sql = "SELECT n.id, n.code, n.lang, n.headline, n.`date`, n.category, l.lang as lang_code, n.active, n.file, n.type, n.location, n.`file-linktext` FROM news n left join langs l on (n.lang=l.id) ".$where_type." group by n.code order by n.`date` desc, n.code desc $limit ";
	    // echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	
	function get_news($code) {
	   	$sql = "SELECT news.*,langs.lang as lang_code FROM news left join langs on (news.lang=langs.id) where code=$code";
	    //echo $sql;
	    $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_news_id($id){
		$sql = "SELECT * FROM news where id=$id";
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function get_news_lang($lang='general',$code) {
		if (empty($code)){
			return false;
		}
		if ($lang!='general'){
			$sql = "SELECT * FROM news where lang=$lang and code=$code";
		}
		else{
			$sql = "SELECT * FROM news where code=$code limit 1";
		}
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }
	
	function count_news_list($lang='all', $bycode=false, $type='') {
		$where_type = '';
		if (!empty($type)){
			$where_type = " AND n.`type`='".$type."' ";
		}
		$groupby = "";
		if ($bycode){
			$groupby = " group by n.code ";
		}
		if ($lang!='all'){
			$sql = "SELECT n.id FROM news n left join langs l on (l.id=n.lang) WHERE l.lang='$lang' ".$where_type." $groupby";
	    }
		else{
			$sql = "SELECT n.id FROM news n WHERE 1 ".$where_type." $groupby;";
		}
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function update_news($news) {
		
		$result = false;
		// --- If language tab, update language data ---//
		$headline = ((isset($news["headline"]) and isset($news["slug"]))?"`headline`='".$news["headline"]."', `slug`='".$news["slug"]."',":"");
	   	$sql = "UPDATE news SET $headline `modifiedby`='".$_SESSION['user']['id']."', `category`='".$news["category"]."', `category_slug`='".$news["category_slug"]."', `intro`='".$news["intro"]."', `body`='".$news["body"]."',`meta-title`='".$news["meta-title"]."', `meta-description`='".$news["meta-description"]."', `meta-keywords`='".$news["meta-keywords"]."', `file-linktext`='".$news["file-linktext"]."', `active`='".$news["active"]."', `link`='".$news["link"]."', `link-text`='".$news["link-text"]."' where lang=".$news["lang"]. " and code=".$news["code"];
	    // echo $sql;
	    $result = $this->db->query( $sql );
		if ($result !== false){
			// --- update general data --- //
			$sql= "UPDATE news SET `type`='".$news["type"]."', `file` = '".$news["file"]."', `modifiedby`='".$_SESSION['user']['id']."', `date`='".$news["date"]."', `location`='".$news["location"]."', `link`='".$news["link"]."', `link-text`='".$news["link-text"]."' WHERE code=".$news["code"]." AND (`date`!='".$news["date"]."' OR `file`!='".$news["file"]."' OR `type`!='".$news["type"]."' OR `location`!='".$news["location"]."')"; 
			$result = $this->db->query( $sql );
		}
	   return ($result !== false);
    }
	
	function add_news($news) {
		$result = false;
	   	$sql = "INSERT INTO news (`code`,`lang`,`headline`, `slug`, `date`,`category`,`category_slug`,`intro`,`body`, `meta-title`, `meta-description`, `meta-keywords`, `active`, `added`, `addedby`, `modifiedby`, `file`, `type`, `location`, `file-linktext`, `link`, `link-text`) values ('".$news["code"]."','".$news["lang"]."','".$news["headline"]."','".$news["slug"]."','".$news["date"]."','".$news["category"]."','".$news["category_slug"]."','".$news["intro"]."','".$news["body"]."','".$news["meta-title"]."','".$news["meta-description"]."','".$news["meta-keywords"]."', '".$news["active"]."', NOW(), '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."', '".$news["file"]."', '".$news["type"]."', '".$news["location"]."', '".$news["file-linktext"]."', '".$news["link"]."', '".$news["link-text"]."' );";
	    // echo $sql;
		$result = $this->db->query( $sql );
		
		if ($result !== false){
			// --- update general data --- //
			$sql= "UPDATE news SET `type`='".$news["type"]."', `file` = '".$news["file"]."', `modifiedby`='".$_SESSION['user']['id']."', `date`='".$news["date"]."', `location`='".$news["location"]."', `link`='".$news["link"]."', `link-text`='".$news["link-text"]."' WHERE code=".$news["code"]." AND (`date`!='".$news["date"]."' OR `file`!='".$news["file"]."' OR `type`!='".$news["type"]."' OR `location`!='".$news["location"]."' )"; 
			$result = $this->db->query( $sql );
		}
		
	    return ($result !== false);
    }
	
	function headline_news_exist($headline='', $slug='', $lang='', $code=''){
		$sql = "SELECT news.`id` FROM `news` WHERE (`headline`='$headline' OR `slug`='$slug') ".($lang?" AND `lang`='$lang' ":"").($code?" AND `code`!='$code' ":"");
		$results = $this->db->get_results( $sql, ARRAY_A );	
		return count($results);	
	}
	
	function get_news_last_code() {
	   	$sql = "SELECT code FROM news ORDER BY code DESC LIMIT 1";
		//echo $sql;
		$results = $this->db->get_var( $sql );
		return $results;
    }
	
	function delete_news_lang($code,$lang) {
	   	$sql = "DELETE from news where code=$code and lang=$lang";
	    // echo $sql;
	    $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_news_lang_images($code,$lang) {
	   	$sql = "DELETE from news_images where news_code=$code and lang=$lang";
	     // echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_news_lang_files($code,$lang) {
	   	$sql = "DELETE from news_files where news_code=$code and lang=$lang";
	     // echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_news($code) {
	   	$sql = "DELETE from news where code=$code";
	      //echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	

	function get_id_news_images_list($news_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_images = $this->get_id_news_images($result['lang'], $result['code'], $mergeifnoexist);
				if (!empty($news_images)){
					$result['images']=$news_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_id_news_images($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all interactions_categories rows for all languages
		$sql = "SELECT *, code as `code-image`, CONCAT('js-file-', `code`) as `id-image`, 'news/' as `path-image` FROM news_images where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0);
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				$current['aux_id'] = $id; // we need to maintain actual id in array
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results_aux[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results_aux[$current['code']]) AND $current['lang']!=$langaux){
							$current['alt-image'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results_aux[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results_aux[$current['code']] = $current;
				}
			}
		}
		
		//we generate array with original index values
		if (!empty($results_aux)){
			foreach ($results_aux as $id => $current){
				$results[$current['aux_id']] = $current;
			}
		}
		
		return $results;
	}
	
	function get_code_news_images_list($news_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_images = $this->get_code_news_images($result['lang'], $result['code'], $mergeifnoexist);
				if (!empty($news_images)){
					$result['images']=$news_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_code_news_images($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all interactions_categories rows for all languages
		$sql = "SELECT *, code as `code-image`, CONCAT('js-file-', `code`) as `id-image`, 'news/' as `path-image` FROM news_images where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0); 
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results[$current['code']]) AND $current['lang']!=$langaux){
							$current['alt-image'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results[$current['code']] = $current;
				}
				
			}
		}
		
		return $results;
	}
	
	/// FILES
	
	function get_id_news_files_list($news_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_files = $this->get_id_news_files($result['lang'], $result['code'], $mergeifnoexist);
				if (!empty($news_files)){
					$result['files']=$news_files;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_id_news_files($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all interactions_categories rows for all languages
		$sql = "SELECT *, code as `code-file`, CONCAT('js-filepdf-', `code`) as `id-file`, 'news/' as `path-file` FROM news_files where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0);
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				$current['aux_id'] = $id; // we need to maintain actual id in array
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results_aux[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results_aux[$current['code']]) AND $current['lang']!=$langaux){
							$current['link-file'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results_aux[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results_aux[$current['code']] = $current;
				}
			}
		}
		
		//we generate array with original index values
		if (!empty($results_aux)){
			foreach ($results_aux as $id => $current){
				$results[$current['aux_id']] = $current;
			}
		}
		
		return $results;
	}
	
	function get_code_news_files_list($news_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($news_list)){
			foreach($news_list as $i => $result){
				$news_files = $this->get_code_news_files($result['lang'], $result['code'], $mergeifnoexist);
				if (!empty($news_files)){
					$result['files']=$news_files;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_code_news_files($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all interactions_categories rows for all languages
		$sql = "SELECT *, code as `code-file`, CONCAT('js-filepdf-', `code`) as `id-file`, 'news/' as `path-file` FROM news_files where news_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0); 
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results[$current['code']]) AND $current['lang']!=$langaux){
							$current['link-file'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results[$current['code']] = $current;
				}
				
			}
		}
		
		return $results;
	}
	
	//
	
	function get_categories_suggests($keyword='', $lang=''){
		$results = array();
		$keyword = trim($keyword);
		if (!empty($keyword)){
			$where_lang = "";
			if (!empty($lang)){
				$where_lang = " AND lang = '".$lang."' ";
			}
			$keyword = '';
			$where_keyword = " 1 ";
			if (!empty($keyword)){
				$where_keyword = " category LIKE '".$keyword."%' ";
			}
			$sql = "SELECT category FROM news WHERE ".$where_keyword." ".$where_lang." ORDER BY category ASC;";
			$results = $this->db->get_results( $sql, ARRAY_A );
		}
		return $results;
		
	}
	
// --------------------- CMS NEWS IMAGES -------------------

	function get_news_images_last_code() {
	   	$sql = "SELECT code FROM news_images ORDER BY code DESC LIMIT 1";
		//echo $sql;
		$results = $this->db->get_var( $sql );
		return $results;
    }

	function update_news_images($news){
		$result = false;
		
		if (!empty($news['images'])){
			foreach($news['images'] as $current_image){
				if ($current_image['code-image']){ //exists
					switch($current_image['status']){
						case 'D':
							$sql = "DELETE from news_images where code=".$current_image['code-image'];
							 // echo $sql;
							$result = $this->db->query( $sql );
							break;
						case 'M':
						default:
							$is_main = $current_image['is-main']?'Yes':'No';
							
							$sql = "INSERT INTO news_images (`code`, `news_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code-image"]."','".$news["code"]."','".$current_image["lang"]."','".$current_image["image"]."','".$current_image["alt-image"]."', NOW(), '".$is_main."') ON DUPLICATE KEY UPDATE `alt-image`='".$current_image["alt-image"]."', `is-main`='".$is_main."';";
							
							$result = $this->db->query( $sql );
							
							//-- update all others languages ---
							$sql= "UPDATE news_images SET `image`='".$current_image["image"]."', `is-main`='".$is_main."' WHERE code=".$current_image["code-image"]." AND (`image`!='".$current_image["image"]."' OR `is-main`!='".$is_main."')"; 
							$result = $this->db->query( $sql );
							break;
					}
				}
				else{ //new
					$last_code= $this->get_news_images_last_code ();
					$current_image["code"] = $last_code+1;
					
					$is_main = $current_image['is-main']?'Yes':'No';
					
					$sql = "INSERT INTO news_images (`code`, `news_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code"]."','".$news["code"]."','".$current_image["lang"]."','".$current_image["image"]."','".$current_image["alt-image"]."', NOW(), '".$is_main."');";
	
					// echo $sql;
					$result = $this->db->query( $sql );
					
					//$langs = $this->get_cms_active_langs();
					//foreach ($langs as $current_lang){
					$news_langs = $this->get_news($news["code"]); // la creo para cada uno de los idiomas en los que esté publicada la noticia.
					if (!empty($news_langs)){
						foreach ($news_langs as $current_lang){
							if ($current_lang['lang']!=$current_image["lang"]){
								
								$sql = "INSERT INTO news_images (`code`, `news_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code"]."','".$news["code"]."','".$current_lang["lang"]."','".$current_image["image"]."','', NOW(), '".$is_main."');";
		
								// echo $sql;
								$result = $this->db->query( $sql );
							}
						}
					}
					//}
				}
			}
		}
	    return ($result!==false);
	}
	
	function delete_all_news_images($code){
		$sql = "DELETE from news_images where news_code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
	}
	
	function delete_news_images($code){
		$sql = "DELETE from news_images where code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
	}
	
// --------------------- CMS NEWS FILES -------------------

	function get_news_files_last_code() {
	   	$sql = "SELECT code FROM news_files ORDER BY code DESC LIMIT 1";
		//echo $sql;
		$results = $this->db->get_var( $sql );
		return $results;
    }

	function update_news_files($news){
		$result = false;
		
		if (!empty($news['files'])){
			foreach($news['files'] as $current_file){
				if ($current_file['code-file']){ //exists
					switch($current_file['status']){
						case 'D':
							$sql = "DELETE from news_files where code=".$current_file['code-file'];
							 // echo $sql;
							$result = $this->db->query( $sql );
							break;
						case 'M':
						default:
							$is_main = $current_file['is-main']?'Yes':'No';
							
							$sql = "INSERT INTO news_files (`code`, `news_code`, `lang`,`file`, `link-file`, `added`,`is-main`) values ('".$current_file["code-file"]."','".$news["code"]."','".$current_file["lang"]."','".$current_file["file"]."','".$current_file["link-file"]."', NOW(), '".$is_main."') ON DUPLICATE KEY UPDATE `link-file`='".$current_file["link-file"]."', `is-main`='".$is_main."';";
							
							$result = $this->db->query( $sql );
							
							//-- update all others languages ---
							$sql= "UPDATE news_files SET `file`='".$current_file["file"]."', `is-main`='".$is_main."' WHERE code=".$current_file["code-file"]." AND (`file`!='".$current_file["file"]."' OR `is-main`!='".$is_main."')"; 
							$result = $this->db->query( $sql );
							break;
					}
				}
				else{ //new
					$last_code= $this->get_news_files_last_code ();
					$current_file["code"] = $last_code+1;
					
					$is_main = $current_file['is-main']?'Yes':'No';
					
					$sql = "INSERT INTO news_files (`code`, `news_code`, `lang`,`file`, `link-file`, `added`,`is-main`) values ('".$current_file["code"]."','".$news["code"]."','".$current_file["lang"]."','".$current_file["file"]."','".$current_file["link-file"]."', NOW(), '".$is_main."');";
	
					// echo $sql;
					$result = $this->db->query( $sql );
					
					//$langs = $this->get_cms_active_langs();
					//foreach ($langs as $current_lang){
					$news_langs = $this->get_news($news["code"]); // la creo para cada uno de los idiomas en los que esté publicada la noticia.
					if (!empty($news_langs)){
						foreach ($news_langs as $current_lang){
							if ($current_lang['lang']!=$current_file["lang"]){
								
								$sql = "INSERT INTO news_files (`code`, `news_code`, `lang`,`file`, `link-file`, `added`,`is-main`) values ('".$current_file["code"]."','".$news["code"]."','".$current_lang["lang"]."','".$current_file["file"]."','', NOW(), '".$is_main."');";
		
								// echo $sql;
								$result = $this->db->query( $sql );
							}
						}
					}
					//}
				}
			}
		}
	    return ($result!==false);
	}
	
	function delete_all_news_files($code){
		$sql = "DELETE from news_files where news_code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
	}
	
	function delete_news_files($code){
		$sql = "DELETE from news_files where code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
	}

// --------------------- CMS proyectos ----------------------
	
	function get_projects_codes() {
	   	$sql = "SELECT DISTINCT code FROM projects order by `year` desc, code desc";
	        //echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_projects_list($code) {
	   	$sql = "SELECT p.id, p.code, p.lang, p.headline, p.`year`, p.area, p.fase, p.estado, p.unidades, langs.lang as lang_code, p.active, p.`image1`, p.`alt-image1` FROM projects p left join langs on (p.lang=langs.id) where code=$code order by p.lang asc, p.year desc, p.code desc";
	       // echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_projects_limit_list($limit='') {
	   	$sql = "SELECT p.id, p.code, p.lang, p.headline, p.`year`, p.area, p.fase, p.estado, p.unidades, langs.lang as lang_code, p.active, p.`image1`, p.`alt-image1` FROM projects p left join langs on (p.lang=langs.id) group by p.code order by p.year desc, p.code desc $limit ";
	       // echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_projects($code) {
	   	$sql = "SELECT p.*,langs.lang as lang_code FROM projects p left join langs on (p.lang=langs.id) where code=$code";
	        //echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_projects_id($id){
		$sql = "SELECT * FROM projects where id=$id";
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function get_projects_lang($lang='general',$code) {
		if (empty($code)){
			return false;
		}
		if ($lang!='general'){
			$sql = "SELECT * FROM projects where lang=$lang and code=$code";
	    }
		else{
				$sql = "SELECT * FROM projects where code=$code limit 1";
		}
		//echo $sql;
		$results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }
	
	function count_projects_list($lang='all', $bycode=false) {
		$groupby = "";
		if ($bycode){
			$groupby = " group by p.code ";
		}
		if ($lang!='all'){
			$sql = "SELECT p.id FROM projects p left join langs on (langs.id=p.lang) WHERE langs.lang='$lang' $groupby";
	    }
		else{
			$sql = "SELECT p.id FROM projects p $groupby;";
		}
		//echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function update_projects($projects) {
		
		$result = false;
		// --- If language tab, update language data ---//
		$headline = ((isset($projects["headline"]) and isset($projects["slug"]))?"`headline`='".$projects["headline"]."', `slug`='".$projects["slug"]."',":"");
		$sql = "update projects SET $headline `modifiedby`='".$_SESSION['user']['id']."', `area`='".$projects["area"]."', `subtitle`='".$projects["subtitle"]."', `necessity`='".$projects["necessity"]."',`meta-title`='".$projects["meta-title"]."', `meta-description`='".$projects["meta-description"]."', `meta-keywords`='".$projects["meta-keywords"]."', `active`='".$projects["active"]."', `alt-image1`='".$projects["alt-image1"]."', `link`='".$projects["link"]."' where lang=".$projects["lang"]. " and code=".$projects["code"];
	  // echo $sql;
		$result = $this->db->query( $sql );
		if ($result!==false){
			// --- update general data --- //
			$sql= "UPDATE projects SET `modifiedby`='".$_SESSION['user']['id']."', `location`='".$projects["location"]."', `year`='".$projects["year"]."', fase='".$projects["fase"]."', estado='".$projects["estado"]."', unidades='".$projects["unidades"]."', image1='".$projects["image1"]."', `link`='".$projects["link"]."', `link-text`='".$projects["link-text"]."' WHERE code=".$projects["code"]." AND (`year`!='".$projects["year"]."' OR `location`!='".$projects["location"]."' OR fase!='".$projects["fase"]."' OR estado!='".$projects["estado"]."' OR unidades!='".$projects["unidades"]."' OR image1!='".$projects["image1"]."')"; 
			$result = $this->db->query( $sql );
		}
	    return ($result!==false);
    }
	
	function add_projects($projects) {
		$result = false;
		
		$sql = "insert into projects (`code`,`lang`,`headline`, `slug`,`area`,`subtitle`,`necessity`, `meta-title`, `meta-description`, `meta-keywords`, `active`, fase, estado, unidades, `year`, `location`, `added`, `addedby`, `modifiedby`, `image1`, `alt-image1`, `link`, `link-text`) values ('".$projects["code"]."','".$projects["lang"]."','".$projects["headline"]."','".$projects["slug"]."','".$projects["area"]."','".$projects["subtitle"]."','".$projects["necessity"]."','".$projects["meta-title"]."','".$projects["meta-description"]."','".$projects["meta-keywords"]."', '".$projects["active"]."', '".$projects["fase"]."', '".$projects["estado"]."', '".$projects["unidades"]."', '".$projects["year"]."','".$projects["location"]."', NOW(), '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."', '".$projects["image1"]."', '".$projects["alt-image1"]."', '".$projects["link"]."', '".$projects["link-text"]."' );";
		// echo $sql;
		$result = $this->db->query( $sql );
		
		if ($result!==false){
			// --- update general data --- //
			$sql= "UPDATE projects SET `modifiedby`='".$_SESSION['user']['id']."', `location`='".$projects["location"]."', `year`='".$projects["year"]."', fase='".$projects["fase"]."', estado='".$projects["estado"]."', unidades='".$projects["unidades"]."', image1='".$projects["image1"]."',  link='".$projects["link"]."',  link-text='".$projects["link-text"]."' WHERE code=".$projects["code"]." AND (`year`!='".$projects["year"]."' OR `location`!='".$projects["location"]."' OR fase!='".$projects["fase"]."' OR estado!='".$projects["estado"]."' OR unidades!='".$projects["unidades"]."' OR image1!='".$projects["image1"]."')";  
			$result = $this->db->query( $sql );
		}
		
	    return ($result!==false);
    }
	
	function headline_projects_exist($headline='', $slug='', $lang='', $code=''){
		$sql = "SELECT p.`id` FROM `projects` p WHERE (`headline`='$headline' OR `slug`='$slug') ".($lang?" AND `lang`='$lang' ":"").($code?" AND `code`!='$code' ":"");
		$results = $this->db->get_results( $sql, ARRAY_A );	
		return count($results);	
	}
	
	function get_projects_last_code() {
	   	$sql = "SELECT code FROM projects ORDER BY code DESC LIMIT 1";
	        //echo $sql;
	        $results = $this->db->get_var( $sql );
	        return $results;
    }
	
	function delete_projects_lang($code,$lang) {
	   	$sql = "DELETE from projects where code=$code and lang=$lang";
	     // echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_projects_lang_images($code,$lang) {
	   	$sql = "DELETE from projects_images where projects_code=$code and lang=$lang";
	     // echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_projects($code) {
	   	$sql = "DELETE from projects where code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function get_id_projects_industries_list($projects_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($projects_list)){
			foreach($projects_list as $i => $result){
				$projects_industries = $this->get_code_projects_industries($result['lang'], $result['code'], $mergeifnoexist);
				if (!empty($projects_industries)){
					$result['industries']=$projects_industries;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	
	
	
	function get_id_projects_images_list($projects_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($projects_list)){
			foreach($projects_list as $i => $result){
				$projects_images = $this->get_id_projects_images($result['lang'], $result['code'], $mergeifnoexist);
				$result['images'] = array();
				if (!empty($projects_images)){
					$result['images']=$projects_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_id_projects_images($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		$sql = "SELECT *, code as `code-image`, CONCAT('js-file-', `code`) as `id-image`, 'projects/' as `path-image` FROM projects_images where projects_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0);
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				$current['aux_id'] = $id; // we need to maintain actual id in array
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results_aux[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results_aux[$current['code']]) AND $current['lang']!=$langaux){
							$current['alt-image'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results_aux[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results_aux[$current['code']] = $current;
				}
			}
		}
		
		//we generate array with original index values
		if (!empty($results_aux)){
			foreach ($results_aux as $id => $current){
				$results[$current['aux_id']] = $current;
			}
		}
		
		return $results;
	}
	
	function get_code_projects_images_list($projects_list, $mergeifnoexist = true){
		$results = array();
		if (!empty($projects_list)){
			foreach($projects_list as $i => $result){
				$projects_images = $this->get_code_projects_images($result['lang'], $result['code'], $mergeifnoexist);
				$result['images']= array();
				if (!empty($projects_images)){
					$result['images']=$projects_images;
				}
				$results[$i] = $result;
			}
		}
		return $results;
	}
	
	function get_code_projects_images($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all rows for all languages
		$sql = "SELECT *, code as `code-image`, CONCAT('js-file-', `code`) as `id-image`, 'projects/' as `path-image` FROM projects_images where projects_code=$code ORDER BY `lang` ASC, `is-main` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0); 
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results[$current['code']]) OR $current['lang']==$langaux ){
						
						if (empty($results[$current['code']]) AND $current['lang']!=$langaux){
							$current['alt-image'] = ''; //if lang row not exist, we reset lang fields
						}
						
						$results[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results[$current['code']] = $current;
				}
				
			}
		}
		
		return $results;
	}
	
	function get_code_projects_industries($lang='', $code='', $mergeifnoexist = true){
		$results = array();
		
		// we load all projects_industries rows for all languages
		$sql = "SELECT nc.*, c.* FROM projects_has_industries nc INNER JOIN industries c ON (c.code=nc.industries_code) where nc.projects_code=$code order by c.`headline` ASC";
		
		$all_results = $this->db->get_results( $sql, ARRAY_A );
		
		$langaux = ($lang!='general'?$lang:0); 
		
		if (!empty($all_results)){
			foreach ($all_results as $id => $current){
				
				if (!$langaux){ // in case 'general' lang default lang is the first row lang
					$langaux = $current['lang'];
				}
				
				if ($mergeifnoexist){ // we nedd all data for the aux langs (general field)
					if ( empty($results[$current['code']]) OR $current['lang']==$langaux ){
						$results[$current['code']] = $current;
					}
				}
				elseif($current['lang']==$langaux){ // we need only existing data in aux lang (exclusive lang field)
					$results[$current['code']] = $current;
				}
			}
		}

		return $results;
	}
	
	function update_projects_industries($projects){
		$result = false;
		$this->delete_projects_industries($projects['code']);
		if (!empty($projects['industries'])){
			foreach($projects['industries'] as $current => $value){
				$sql = "INSERT INTO projects_has_industries (`projects_code`, `industries_code`) values ('".$projects["code"]."', '".$current."');";
				// echo $sql;
				$result = $this->db->query( $sql );
			}
		}
		return ($result !== false);
	}
	
	function delete_projects_industries($code){
		$sql = "DELETE from projects_has_industries where projects_code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result !== false);
	}
	
	
// --------------------- CMS PROJECTS IMAGES -------------------

	function get_projects_images_last_code() {
	   	$sql = "SELECT code FROM projects_images ORDER BY code DESC LIMIT 1";
	        //echo $sql;
	        $results = $this->db->get_var( $sql );
	        return $results;
    }

	function update_projects_images($projects){
		$result = false;
		
		if (!empty($projects['images'])){
			foreach($projects['images'] as $current_image){
				if ($current_image['code-image']){ //exists
					switch($current_image['status']){
						case 'D':
							$sql = "DELETE from projects_images where code=".$current_image['code-image'];
							 // echo $sql;
							$result = $this->db->query( $sql );
							break;
						case 'M':
						default:
							$is_main = $current_image['is-main']?'Yes':'No';
							$sql= "INSERT INTO projects_images (`code`, `projects_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code-image"]."','".$projects["code"]."','".$current_image["lang"]."','".$current_image["image"]."','".$current_image["alt-image"]."', NOW(), '".$is_main."') ON DUPLICATE KEY UPDATE `alt-image`='".$current_image["alt-image"]."', `is-main`='".$is_main."';"; 
							
							$result = $this->db->query( $sql );
							
							//-- update all others languages ---
							$sql= "UPDATE projects_images SET `image`='".$current_image["image"]."', `is-main`='".$is_main."' WHERE code=".$current_image["code-image"]." AND (`image`!='".$current_image["image"]."' OR `is-main`!='".$is_main."')"; 
							$result = $this->db->query( $sql );
							break;
					}
				}
				else{ //new
					$last_code= $this->get_projects_images_last_code ();
					$current_image["code"] = $last_code+1;
					
					$is_main = $current_image['is-main']?'Yes':'No';
					
					$sql = "INSERT INTO projects_images (`code`, `projects_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code"]."','".$projects["code"]."','".$current_image["lang"]."','".$current_image["image"]."','".$current_image["alt-image"]."', NOW(), '".$is_main."');";
	
					// echo $sql;
					$result = $this->db->query( $sql );
					
					//$langs = $this->get_cms_active_langs();
					//foreach ($langs as $current_lang){
					$projects_langs = $this->get_projects($projects["code"]); // la creo para cada uno de los idiomas en los que esté publicada la noticia.
					if (!empty($projects_langs)){
						foreach ($projects_langs as $current_lang){
							if ($current_lang['lang']!=$current_image["lang"]){
							
								$sql = "INSERT INTO projects_images (`code`, `projects_code`, `lang`,`image`, `alt-image`, `added`,`is-main`) values ('".$current_image["code"]."','".$projects["code"]."','".$current_lang["lang"]."','".$current_image["image"]."','', NOW(), '".$is_main."');";
		
								// echo $sql;
								$result = $this->db->query( $sql );
							}
						}
					}
					//}
				}
			}
		}
	    return ($result!==false);
	}
	
	function delete_all_projects_images($code){
		$sql = "DELETE from projects_images where projects_code=$code";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
	}
	
	function delete_projects_images($code){
		$sql = "DELETE from projects_images where code=$code";
	      //echo $sql;
	     $result =  $this->db->query( $sql );
	     return ($result!==false);
	}
	
// --------------------- CMS industries ----------------------	
	
	function get_all_industries_list($lang='', $type=''){
		$where = "WHERE 1";
		$where .= ($lang?" AND e.`lang`='$lang' ": "");
		if (!empty($type)){
			$typeList = explode(",", $type);
			if (!empty($typeList)){
				$type = "'".implode("','", $typeList)."'";
			}
			else{
				$type='';
			}
		}
		$where .= ($type?" AND e.`type` IN (".$type.") ": "");
		$sql = "SELECT e.headline, e.id, e.code, l.lang FROM industries e left join langs l on (l.id=e.lang) ".$where." ORDER BY e.`lang` ASC, e.headline ASC, e.code ASC";
		$results = $this->db->get_results( $sql, ARRAY_A );	
		$industries_lang = array();
		
		if (empty($results)){
			return $industries_lang;
		}
		// we get all stored industries-lang list
		foreach ($results as $industries_aux){
			$industries_lang[$industries_aux['code']][$industries_aux['lang']]=$industries_aux;
		}
		
		// web define the industries-lang entries for no defined lang row values
		$langs_list = $this->get_langs();
		foreach ($langs_list as $current){
			foreach($industries_lang as $code => $current_langs_list){
				if (empty($industries_lang[$code][$current['lang']])){ 
					//if no defined in current language, we get the first one
					$industries_lang[$code][$current['lang']] = reset($industries_lang[$code]);
				}
			}
		}
		
		return $industries_lang;
	}
	
	function get_industries_codes() {
	   	$sql = "SELECT DISTINCT code FROM industries order by `code` desc";
	        //echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_industries_list($code) {
	   	$sql = "SELECT industries.type, industries.id, industries.code, industries.`order`, industries.lang, industries.headline, industries.country, industries.city, industries.image, industries.`alt-image`, langs.lang as lang_code, industries.active, industries.company FROM industries left join langs on (industries.lang=langs.id) where code=$code order by industries.lang asc, industries.`headline` asc, industries.`order` asc";
	       // echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_industries_limit_list($limit='') {
	   	$sql = "SELECT industries.type, industries.id, industries.code, industries.`order`, industries.lang, industries.headline, industries.country, industries.city, industries.image, industries.`alt-image`, langs.lang as lang_code, industries.active, industries.company FROM industries left join langs on (industries.lang=langs.id) group by industries.code order by industries.`headline` asc, industries.`order` asc, industries.`code` desc, industries.id desc $limit ";
	       // echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	
	function get_industries($code) {
	   	$sql = "SELECT industries.*,langs.lang as lang_code FROM industries left join langs on (industries.lang=langs.id) where code=$code";
	    //echo $sql;
	    $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_industries_id($id){
		$sql = "SELECT * FROM industries where id=$id";
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function get_industries_lang($lang="general",$code) {
		if (empty($code)){
			return false;
		}
		if ($lang!='general'){
			$sql = "SELECT * FROM industries where lang=$lang and code=$code";
	    }
		else{
				$sql = "SELECT * FROM industries where code=$code limit 1";
		}
		//echo $sql;
		$results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
    }
	
	function count_industries_list($lang='all', $bycode=false) {
		$groupby = "";
		if ($bycode){
			$groupby = " group by industries.code ";
		}
		if ($lang!='all'){
			$sql = "SELECT industries.id FROM industries left join langs on (langs.id=industries.lang) WHERE langs.lang='$lang' $groupby";
	    }
		else{
			$sql = "SELECT industries.id FROM industries $groupby;";
		}
		//echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function update_industries($industries) {
		
		$result = false;
		// --- If language tab, update language data ---//
		$headline = ((isset($industries["headline"]) and isset($industries["slug"]))?"`headline`='".$industries["headline"]."', `slug`='".$industries["slug"]."',":"");
		$sql = "update industries SET $headline `modifiedby`='".$_SESSION['user']['id']."', `alt-image`='".$industries["alt-image"]."', `type`='".$industries["type"]."', `city`='".$industries["city"]."',`meta-title`='".$industries["meta-title"]."', `meta-description`='".$industries["meta-description"]."', `meta-keywords`='".$industries["meta-keywords"]."', `subtitle`='".$industries["subtitle"]."', `country`='".$industries["country"]."', `active`='".$industries["active"]."' where lang=".$industries["lang"]. " and code=".$industries["code"];
		$result = $this->db->query( $sql );
		if ($result !== false){
			// --- update general data --- //
			$sql= "UPDATE industries SET `modifiedby`='".$_SESSION['user']['id']."', `image`='".$industries["image"]."', `company`='".$industries["company"]."' WHERE code=".$industries["code"]." AND (`image`!='".$industries["image"]."' OR `company`!='".$industries["company"]."') "; 
			$result = $this->db->query( $sql );
		}
		return ($result !== false);
    }
	
	function add_industries($industries) {
		$result = false;
		
		$sql = "insert into industries (`code`,`lang`,`headline`, `slug`, `subtitle`, `alt-image`, `country`, `city`, `company`, `type`, `active`, `image`, `added`, `order`, `meta-title`, `meta-description`, `meta-keywords`, `addedby`, `modifiedby`) values ('".$industries["code"]."','".$industries["lang"]."','".$industries["headline"]."','".$industries["slug"]."','".$industries["subtitle"]."','".$industries["alt-image"]."','".$industries["country"]."','".$industries["city"]."','".$industries["company"]."', '".$industries["type"]."', '".$industries["active"]."','".$industries["image"]."', NOW(), '".$industries["order"]."','".$industries["meta-title"]."','".$industries["meta-description"]."','".$industries["meta-keywords"]."', '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."' );";
		// echo $sql;
		$result = $this->db->query( $sql );
		
		if ($result !== false){
			// --- update general data --- //
			$sql= "UPDATE industries SET `modifiedby`='".$_SESSION['user']['id']."', `image`='".$industries["image"]."', `order`='".$industries["order"]."', `company`='".$industries["company"]."' WHERE code=".$industries["code"]." AND (`image`!='".$industries["image"]."' OR `order`!='".$industries["order"]."' OR `company`!='".$industries["company"]."' ) "; 
			$result = $this->db->query( $sql );
		}
		
	   return ($result !== false);
    }
	
	function update_industries_code_position($code, $position) {
		$result = false;
		$sql = "UPDATE industries SET `modifiedby`='".$_SESSION['user']['id']."', `order`=$position where code=".$code;
	    $result = $this->db->query( $sql );
		return ($result !== false);
	}
	
	function headline_industries_exist($headline='', $slug='', $lang='', $code=''){
		$sql = "SELECT industries.`id` FROM `industries` WHERE (`headline`='$headline' OR `slug`='$slug') ".($lang?" AND `lang`='$lang' ":"").($code?" AND `code`!='$code' ":"");
		$results = $this->db->get_results( $sql, ARRAY_A );	
		return count($results);	
	}
	
	function get_industries_last_code() {
	   	$sql = "SELECT code FROM industries ORDER BY code DESC LIMIT 1";
	        //echo $sql;
	        $results = $this->db->get_var( $sql );
	        return $results;
    }
	
	function get_industries_last_position() {
	   	$sql = "SELECT `order` FROM industries ORDER BY `order` DESC LIMIT 1";
		//echo $sql;
		$results = $this->db->get_var( $sql );
		return $results;
    }
	
	function delete_industries_lang($code,$lang) {
	   	$sql = "DELETE from industries where code=$code and lang=$lang";
	     // echo $sql;
	    $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	function delete_industries($code) {
	   	$sql = "DELETE from industries where code=$code";
	      //echo $sql;
	     $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
// --------------------- CMS policies ----------------------	

	function get_policies_codes() {
	   	$sql = "SELECT DISTINCT code FROM policies ORDER BY code DESC";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_policies_list($code) {
	   	$sql = "SELECT p.id, p.code, p.lang, p.headline, l.lang as lang_code, p.active, p.body, p.added FROM policies p LEFT JOIN langs l ON (p.lang = l.id) WHERE p.code=$code ORDER BY p.lang ASC, p.active ASC, p.added DESC, p.code DESC";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_policies_limit_list($limit='') {
	   	$sql = "SELECT p.id, p.code, p.lang, p.headline, l.lang as lang_code, p.active, p.body, p.added FROM policies p LEFT JOIN langs l ON (p.lang = l.id) WHERE 1 GROUP BY p.code ORDER BY p.active ASC, p.added DESC, p.code DESC $limit ";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	
	function get_policies($code) {
	   	$sql = "SELECT p.*, l.lang AS lang_code FROM policies p LEFT JOIN langs l ON (p.lang = l.id) WHERE code = $code";
	    $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results;
    }
	
	function get_policies_id($id){
		$sql = "SELECT * FROM policies WHERE id = $id";
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function get_all_policies_list($lang=''){
		$sql = "SELECT e.headline, e.id, e.code, l.lang, e.`body`, e.`added` FROM policies e LEFT JOIN langs l ON (l.id=e.lang) WHERE 1 ".($lang?" AND e.`lang`='$lang' ":"")." ORDER BY e.`lang` ASC, e.active ASC, e.added DESC, e.code DESC";
		$results = $this->db->get_results( $sql, ARRAY_A );	
		$policies_lang = array();
		
		// we get all stored policies-lang list
		foreach ($results as $policies_aux){
			$policies_lang[$policies_aux['code']][$policies_aux['lang']]=$policies_aux;
		}
		
		// web define the policies-lang entries for no defined lang row values
		$langs_list = $this->get_langs();
		foreach ($langs_list as $current){
			foreach($policies_lang as $code => $current_langs_list){
				if (empty($policies_lang[$code][$current['lang']])){ 
					//if no defined in current language, we get the first one
					$policies_lang[$code][$current['lang']] = reset($policies_lang[$code]);
				}
			}
		}
		
		return $policies_lang;
	}
	
	function get_policies_lang($lang='general', $code){
		if (empty($code)){
			return false;
		}
		if ($lang!='general'){
			$sql = "SELECT * FROM policies WHERE lang=$lang AND code=$code ";
		}
		else{
			$sql = "SELECT * FROM policies WHERE code=$code ORDER BY active ASC, added DESC LIMIT 1";
		}
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function get_active_policies_lang($lang='general'){
		if ($lang!='general'){
			$sql = "SELECT * FROM policies WHERE lang=$lang ORDER BY active ASC, added DESC, code DESC LIMIT 1";
		}
		else{
			$sql = "SELECT * FROM policies ORDER BY active ASC, added DESC, code DESC LIMIT 1";
		}
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
	function count_policies_list($lang='all', $bycode=false) {
		$groupby = "";
		if ($bycode){
			$groupby = " GROUP BY p.code ";
		}
		if ($lang!='all'){
			$sql = "SELECT p.id FROM policies p LEFT JOIN langs l ON (l.id = p.lang) WHERE l.lang='$lang' $groupby";
	    }
		else{
			$sql = "SELECT p.id FROM policies p WHERE 1 $groupby;";
		}
		$results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function update_policies($policies) {
		
		$result = false;
		// --- If language tab, update language data ---//
		$headline = ((isset($policies["headline"]) and isset($policies["slug"]))?"`headline`='".$policies["headline"]."', `slug`='".$policies["slug"]."',":"");
	   	$sql = "UPDATE policies SET $headline `modifiedby`='".$_SESSION['user']['id']."', `active`='".$policies["active"]."', `body`='".$policies["body"]."' WHERE lang=".$policies["lang"]. " AND code=".$policies["code"];
	    //addToLog($sql);
	    $result = $this->db->query( $sql );
		if ($result !== false){
			// --- update general data --- //
			//$sql= "UPDATE policies SET `modifiedby`='".$_SESSION['user']['id']."' WHERE code=".$policies["code"]." AND (`date`!='".$policies["date"]."')"; 
			//$result = $this->db->query( $sql );
			//addToLog($sql);
		}
	    return ($result !== false);
    }
	
	function add_policies(&$policies) {
		$result = false;
	   	$sql = "INSERT INTO policies (`code`, `lang`, `headline`, `body`, `slug`, `active`, `added`, `addedby`, `modifiedby`) values ('".$policies["code"]."','".$policies["lang"]."','".$policies["headline"]."', '".$policies["body"]."','".$policies["slug"]."', '".$policies["active"]."', NOW(), '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."');";
		$result = $this->db->query( $sql );
		if ($result !== false){
			$policies['id'] = $this->db->insert_id;
			$langs = $this->get_active_langs();
			if (!empty($langs)){
				foreach ($langs as $current){
					if ($policies["lang"]!=$current['id']){
						$current_row = $this->get_active_policies_lang($current['id']);
						if (!empty($current_row) AND $current_row['code']!=$policies['code']){ //no existe en el idioma, lo creamos zombi
							$sql = "INSERT INTO policies (`code`,`lang`, `headline`, `body`, `slug`, `active`, `added`, `addedby`, `modifiedby`) values ('".$policies["code"]."','".$current['id']."','".$current_row["headline"]."','".$current_row["body"]."','".$current_row["slug"]."', '".$current_row["active"]."', '".$current_row["added"]."', '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."');";
							$result = $this->db->query( $sql );
						}
					}
				}
			}
			// --- update general data --- //
			//$sql= "UPDATE policies SET `modifiedby`='".$_SESSION['user']['id']."', `date`='".$policies["date"]."' WHERE code=".$policies["code"]." AND (`date`!='".$policies["date"]."')"; 
			//$result = $this->db->query( $sql );
			
			if ($result !== false){
				$sql= "UPDATE policies SET `active`='Draft' WHERE code!=".$policies["code"]; 
				$result = $this->db->query($sql);
			}
		}
		
	    return ($result !== false);
    }
	
	function save_policies(&$policies, $policies_org = false) {
		$result = false;
		if (empty($policies_org) OR ($policies['lang']!=$policies_org['lang'])){ //new or new non existing lang row
			//new
			$policies['added'] = date('Y-m-d H:i:s');
			return $this->add_policies($policies);
		}
		elseif (trim(strip_tags($policies["body"]))!=trim(strip_tags($policies_org['body'])) AND $policies['lang']==$policies_org['lang']){ // update existing lang row with body changes, generate a new revision
			//new revision
			$last_code=$this->get_policies_last_code();
			$policies["code"]=$last_code+1;
			$policies["id"] = '';
			$policies['added'] = date('Y-m-d H:i:s');
			return $this->add_policies($policies);
		}
		else{ //update existing lang row with short modification, not body modification
			//short modification, no need new revision
			return $this->update_policies($policies);
		}
    }
	
	function headline_policies_exist($headline='', $slug='', $lang='', $code=''){
		$sql = "SELECT policies.`id` FROM `policies` WHERE (`headline`='$headline' OR `slug`='$slug') ".($lang?" AND `lang`='$lang' AND `active`='Yes' ":"").($code?" AND `code`!='$code' ":"");
		$results = $this->db->get_results( $sql, ARRAY_A );	
		return count($results);	
	}
	
	function get_policies_last_code() {
	   	$sql = "SELECT code FROM policies ORDER BY code DESC LIMIT 1";
		$results = $this->db->get_var( $sql );
		return $results;
    }
	
	function delete_policies_lang($code, $lang) {
	   	$sql = "DELETE FROM policies WHERE code=$code AND lang=$lang";
	    $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
	
	function delete_policies($code) {
	   	$sql = "DELETE FROM policies WHERE code=$code";
	    $result = $this->db->query( $sql );
		return ($result !== false);
    }
	
// --------------------------- CMS CONTACTS -----------------------------------------	
	function count_contacts_list($type='') {
		if (!empty($type)){
			$sql = "SELECT contacts.id FROM contacts WHERE type='$type';";
		}
		else{
			$sql = "SELECT contacts.id FROM contacts";
		}
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function get_contacts_list($type='', $limit='') {
		switch ($type){
			case 'servicios':
				$fields = "id, firstname, lastname, email, message, `send-date`, lang_name, location_name, type";
				break;
			case 'industria':
				$fields = "id, firstname, lastname, email, `send-date`, lang_name, location_name, about, type";
				break;
			case 'moreinfo':
				$fields = "id, firstname, lastname, email, `send-date`, lang_name, location_name, about, type";
				break;
			case 'contacto':
				$fields = "id, firstname, lastname, email, sector, country, province, company, message, `send-date`, lang_name, location_name, type";
				break;
			default:
				$fields = "*";
		}
		$sql = "SELECT $fields FROM contacts WHERE `email`!=''".(!empty($type)?" AND type='$type' ":"")."ORDER BY contacts.`send-date` desc $limit";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }
	
	function get_contacts_id($id){
		$sql = "SELECT * FROM contacts where id=$id";
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
// --------------------------- CMS USERS -----------------------------------------	
	function get_users($id) {
	   	$sql = "SELECT users.* FROM users where id=$id";
	    //echo $sql;
	    $results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0];
    }
	
	function get_login_user($email, $password) {
	   	$sql = "SELECT users.* FROM users where email='$email' AND active='Yes' limit 1";
	    //echo $sql;
	    $results = $this->db->get_results( $sql, ARRAY_A );
		if ($results[0]['password'] == $password){
			return $results[0];
		}
	    return false;
    }
	
	function delete_users($id) {
	   	$sql = "DELETE from users where id=$id";
	      //echo $sql;
	    $result = $this->db->query( $sql );
		return ($result!==false);
    }
	
	function headline_users_exist($nickname='', $email='', $id=''){
		$sql = "SELECT users.`id` FROM `users` WHERE (`nickname`='$nickname' OR `email`='$email') ".($id?" AND `id`!='$id' ":"");;
		$results = $this->db->get_results( $sql, ARRAY_A );	
		return count($results);	
	}
	
	function update_users($users) {
		
		$result = false;
		// --- If language tab, update language data ---//
		$headline = (isset($users["password"]) ?"`password`='".$users["password"]."',":"");
		$sql = "update users SET $headline `modifiedby`='".$_SESSION['user']['id']."', `nickname`='".$users["nickname"]."', `name`='".$users["name"]."', `lastname`='".$users["lastname"]."', `email`='".$users["email"]."', `avatar`='".$users["avatar"]."', `perm_front`='".$users["perm_front"]."', `perm_products`='".$users["perm_products"]."', `perm_productscategories`='".$users["perm_productscategories"]."', `perm_downloads`='".$users["perm_downloads"]."', `perm_downloadscategories`='".$users["perm_downloadscategories"]."', `perm_projects`='".$users["perm_projects"]."', `perm_news`='".$users["perm_news"]."', `perm_contact`='".$users["perm_contact"]."', `perm_users`='".$users["perm_users"]."', `active`='".$users["active"]."' where id=".$users["id"];
		$result = $this->db->query( $sql );
	    return ($result!==false);
    }
	
	function add_users($users) {
		$result = false;
		
		$sql = "insert into users (`id`,`nickname`, `name`, `lastname`, `email`, `password`, `avatar`, `perm_front`, `perm_products`, `perm_productscategories`, `perm_downloads`, `perm_downloadscategories`, `perm_projects`, `perm_news`, `perm_contact`, `perm_users`, `active`, `added`, `addedby`, `modifiedby`) values ('".$users["id"]."','".$users["nickname"]."','".$users["name"]."','".$users["lastname"]."','".$users["email"]."','".$users["password"]."','".$users["avatar"]."', '".$users["perm_front"]."','".$users["perm_products"]."','".$users["perm_productscategories"]."','".$users["perm_downloads"]."','".$users["perm_downloadscategories"]."','".$users["perm_projects"]."', '".$users["perm_news"]."', '".$users["perm_contact"]."', '".$users["perm_users"]."', '".$users["active"]."', NOW(), '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."' );";
		// echo $sql;
		$result = $this->db->query( $sql );
		
		if ($result){
			return $this->db->insert_id;
		}
		
	    return ($result!==false);
    }
	
	function count_users_list() {
		$sql = "SELECT users.id FROM users;";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );	
	    return count($results);	
	}
	
	function get_users_list($limit='') {
		$sql = "SELECT users.* FROM users ORDER BY users.`added` desc $limit";
		//echo $sql;
		$results = $this->db->get_results( $sql, ARRAY_A );
		return $results;
    }
	
	function get_users_id($id){
		$sql = "SELECT * FROM users where id=$id";
	    //echo $sql;
	    $results = $this->db->get_row( $sql, ARRAY_A );
	    return $results;
	}
	
//---------------------------- CMS  ------------------------------------------------
	function delete_img_db ($table , $id, $field){
		$sql = "UPDATE $table SET `$field`='', `alt-$field`='' WHERE id=$id";
		$result = $this->db->query( $sql );
		return ($result!==false);
	}
	
	function delete_img_code_db ($table , $code, $field){
		$sql = "UPDATE $table SET `$field`='', `alt-$field`='' WHERE `code`=$code";
		$result = $this->db->query( $sql );
		return ($result!==false);
	}
	
	function get_cms_default_lang(){
		$sql = "SELECT lang as lang_code FROM langs WHERE `cms_default`='Yes' ORDER BY id ASC limit 1";
		$results = $this->db->get_results( $sql, ARRAY_A );
	    return $results[0]['lang_code'];
	}
	
	function get_cms_active_langs() {
	   	$sql = "SELECT * FROM langs WHERE cms_active='Yes' ORDER BY ID ASC";
	        //echo $sql;
	        $results = $this->db->get_results( $sql, ARRAY_A );
	        return $results;
    }
	
	function get_cms_active_langs_array(){
		$sql = "SELECT lang as lang_code FROM langs WHERE cms_active='Yes' ORDER BY id ASC";
		$langs = $this->db->get_results( $sql, ARRAY_A );
	    $active_langs = array();
		foreach ($langs as $lang_aux){
			$active_langs[] = $lang_aux['lang_code'];
		}
		return $active_langs;
	}

    // --------------------- CMS downloads ----------------------	

    function get_all_downloads_list($lang='', $type=''){
        $where = "WHERE 1";
        $where .= ($lang?" AND e.`lang`='$lang' ": "");
        if (!empty($type)){
            $typeList = explode(",", $type);
            if (!empty($typeList)){
                $type = "'".implode("','", $typeList)."'";
            }
            else{
                $type='';
            }
        }
        $where .= ($type?" AND e.`type` IN (".$type.") ": "");
        $sql = "SELECT e.headline, e.file1, e.id, e.code, l.lang, e.category FROM downloads e left join langs l on (l.id=e.lang) ".$where." ORDER BY e.`lang` ASC, e.headline ASC, e.code ASC";
        $results = $this->db->get_results( $sql, ARRAY_A );	
        $downloads_lang = array();
        
        if (empty($results)){
            return $downloads_lang;
        }
        // we get all stored downloads-lang list
        foreach ($results as $downloads_aux){
            $downloads_lang[$downloads_aux['code']][$downloads_aux['lang']]=$downloads_aux;
        }
        
        // web define the downloads-lang entries for no defined lang row values
        $langs_list = $this->get_langs();
        foreach ($langs_list as $current){
            foreach($downloads_lang as $code => $current_langs_list){
                if (empty($downloads_lang[$code][$current['lang']])){ 
                    //if no defined in current language, we get the first one
                    $downloads_lang[$code][$current['lang']] = reset($downloads_lang[$code]);
                }
            }
        }
        
        return $downloads_lang;
    }

    function get_downloads_codes() {
        $sql = "SELECT DISTINCT code FROM downloads order by `code` desc";
            //echo $sql;
            $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }

    function get_downloads_list($code) {
        $sql = "SELECT downloads.id, downloads.code, downloads.`order`, downloads.lang, downloads.headline, downloads.`date`, downloads.`filelanguage`, downloads.`is_public`, downloads.`category`, langs.lang as lang_code, downloads.active FROM downloads left join langs on (downloads.lang=langs.id) where code=$code order by downloads.lang asc, downloads.`order` asc";
        // echo $sql;
            $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }

    function get_downloads_limit_list($limit='') {
        $sql = "SELECT downloads.id, downloads.code, downloads.`order`, downloads.lang, downloads.headline, downloads.`date`, downloads.`filelanguage`, downloads.`is_public`, downloads.`category`, langs.lang as lang_code, downloads.active FROM downloads left join langs on (downloads.lang=langs.id) group by downloads.code order by downloads.`order` asc, downloads.`code` desc, downloads.id desc $limit ";
        // echo $sql;
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }


    function get_downloads($code) {
        $sql = "SELECT downloads.*,langs.lang as lang_code FROM downloads left join langs on (downloads.lang=langs.id) where code=$code";
        //echo $sql;
        $results = $this->db->get_results( $sql, ARRAY_A );
        return $results;
    }

    function get_downloads_id($id){
        $sql = "SELECT * FROM downloads where id=$id";
        //echo $sql;
        $results = $this->db->get_row( $sql, ARRAY_A );
        return $results;
    }

    function get_downloads_lang($lang="general",$code) {
        if (empty($code)){
            return false;
        }
        if ($lang!='general'){
            $sql = "SELECT * FROM downloads where lang=$lang and code=$code";
        }
        else{
            $sql = "SELECT * FROM downloads where code=$code limit 1";
        }
        //echo $sql;
        $results = $this->db->get_row( $sql, ARRAY_A );
        return $results;
    }

    function count_downloads_list($lang='all', $bycode=false) {
        $groupby = "";
        if ($bycode){
            $groupby = " group by downloads.code ";
        }
        if ($lang!='all'){
            $sql = "SELECT downloads.id FROM downloads left join langs on (langs.id=downloads.lang) WHERE langs.lang='$lang' $groupby";
        }
        else{
            $sql = "SELECT downloads.id FROM downloads $groupby;";
        }
        //echo $sql;
            $results = $this->db->get_results( $sql, ARRAY_A );	
        return count($results);	
    }

    function update_downloads($downloads) {
        
        $result = false;
        // --- If language tab, update language data ---//
        $headline = ((isset($downloads["headline"]) and isset($downloads["slug"]))?"`headline`='".$downloads["headline"]."', `slug`='".$downloads["slug"]."',":"");
        $filelanguage = (isset($downloads["filelanguage"])?"`filelanguage`='".$downloads["filelanguage"]."',":"");
        if (empty($downloads['date'])){
            $downloads['date'] = date("Y-m-d");
        }
        $sql = "UPDATE downloads SET $headline $filelanguage `modifiedby`='".$_SESSION['user']['id']."', `subtitle`='".$downloads["subtitle"]."', `active`='".$downloads["active"]."' where lang=".$downloads["lang"]. " and code=".$downloads["code"];
        //addToLog('Downloads UPDATE: '.$sql);
        $result = $this->db->query( $sql );
        if ($result!==false){
            // --- update general data --- //
            $sql= "UPDATE downloads SET `modifiedby`='".$_SESSION['user']['id']."', `date`='".$downloads["date"]."', `image1`='".$downloads["image1"]."', `file1`='".$downloads["file1"]."', `filesize1`='".$downloads["filesize1"]."', `fileformat1`='".$downloads["fileformat1"]."', `category`='".$downloads["category"]."', `is_public`='".$downloads["is_public"]."' WHERE code=".$downloads["code"]." AND (`image1`!='".$downloads["image1"]."' OR `date`!='".$downloads["date"]."' OR `file1`!='".$downloads["file1"]."' OR `filesize1`!='".$downloads["filesize1"]."' OR `fileformat1`!='".$downloads["fileformat1"]."' OR `category`!='".$downloads["category"]."' OR `is_public`!='".$downloads["is_public"]."') "; 
            $result = $this->db->query( $sql );
            //addToLog('Downloads UPDATE2: '.$sql);
        }
        return ($result!==false);
    }

    function add_downloads($downloads) {
        $result = false;
        $downloads["date"] = date('Y-m-d');
        $sql = "INSERT INTO downloads (`code`,`lang`,`headline`, `slug`, `date`, `filelanguage`, `subtitle`, `image1`, `file1`, `filesize1`, `fileformat1`, `active`, `is_public`,`added`, `order`, `addedby`, `modifiedby`, `category`) values ('".$downloads["code"]."','".$downloads["lang"]."','".$downloads["headline"]."','".$downloads["slug"]."','".$downloads["date"]."','".$downloads["filelanguage"]."','".$downloads["subtitle"]."','".$downloads["image1"]."','".$downloads["file1"]."','".$downloads["filesize1"]."','".$downloads["fileformat1"]."', '".$downloads["active"]."', '".$downloads["is_public"]."', NOW(), '".$downloads["order"]."', '".$_SESSION['user']['id']."', '".$_SESSION['user']['id']."', '".$downloads["category"]."' );";
        // echo $sql;
        $result = $this->db->query( $sql );
        
        if ($result!==false){
            // --- update general data --- //
            $sql= "UPDATE downloads SET `modifiedby`='".$_SESSION['user']['id']."', `date`='".$downloads["date"]."', `image1`='".$downloads["image1"]."', `file1`='".$downloads["file1"]."', `filesize1`='".$downloads["filesize1"]."', `fileformat1`='".$downloads["fileformat1"]."', `order`='".$downloads["order"]."', `category`='".$downloads["category"]."', `is_public`='".$downloads["is_public"]."' WHERE code=".$downloads["code"]." AND (`image1`!='".$downloads["image1"]."' OR `date`!='".$downloads["date"]."' OR `file1`!='".$downloads["file1"]."' OR `filesize1`!='".$downloads["filesize1"]."' OR `fileformat1`!='".$downloads["fileformat1"]."' OR `order`!='".$downloads["order"]."' OR `category`!='".$downloads["category"]."' OR `is_public`!='".$downloads["is_public"]."') "; 
            $result = $this->db->query( $sql );
        }
        
        return ($result!==false);
    }

    function update_downloads_code_position($code, $position) {
        $result = false;
        $sql = "UPDATE downloads SET `modifiedby`='".$_SESSION['user']['id']."', `order`=$position where code=".$code;
        $result = $this->db->query( $sql );
        return $result;
    }

    function headline_downloads_exist($headline='', $slug='', $lang='', $code=''){
        $sql = "SELECT downloads.`id` FROM `downloads` WHERE (`headline`='$headline'  OR `slug`='$slug') ".($lang?" AND `lang`='$lang' ":"").($code?" AND `code`!='$code' ":"");
        $results = $this->db->get_results( $sql, ARRAY_A );	
        return count($results);	
    }

    function get_downloads_last_code() {
        $sql = "SELECT code FROM downloads ORDER BY code DESC LIMIT 1";
            //echo $sql;
            $results = $this->db->get_var( $sql );
            return $results;
    }

    function get_downloads_last_position() {
        $sql = "SELECT `order` FROM downloads ORDER BY `order` DESC LIMIT 1";
        //echo $sql;
        $results = $this->db->get_var( $sql );
        return $results;
    }

    function delete_downloads_lang($code,$lang) {
        $sql = "DELETE from downloads where code=$code and lang=$lang";
        // echo $sql;
        $result = $this->db->query( $sql );
        return ($result!==false);
    }

    function delete_downloads($code) {
        $sql = "DELETE from downloads where code=$code";
        //echo $sql;
        $result = $this->db->query( $sql );
        return ($result!==false);
    }
	
//--------------------------------------	

}

$db = new DB();
