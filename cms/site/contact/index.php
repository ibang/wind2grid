<?
require_once("../../php/init.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-contacts.php");
$section="contact";
checkPermissions($section);
$lngtype = !empty($type)?$type:'all';
$title=$txt->contact->{$lngtype}->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="contact">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<h1><?=$txt->contact->{$lngtype}->h1?></h1>
		</div>
		<div class="col-sm-4 col-md-4">
			<p class="link link-add"><a class="btn btn-primary down" href="<?=$URL_ROOT?>site/contact/?type=<?=$type?>&action=export"><?=$txt->contact->{$lngtype}->link?></a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?if(!empty($contacts)){?>
			<div class="table-responsive">
				<table class="table table-striped">
				     <thead>
				       <tr>
				         <th><?=$txt->contact->{$lngtype}->table->date?></th>
						 <th><?=$txt->contact->{$lngtype}->table->type?></th>
				         <th><?=$txt->contact->{$lngtype}->table->firstname?></th>
				         <th><?=$txt->contact->{$lngtype}->table->email?>/<?=$txt->contact->{$lngtype}->table->phone?></th>
				         <th><?=$txt->contact->{$lngtype}->table->language?></th>
				       </tr>
				     </thead>
				     <tbody>
						
						<?foreach ($contacts as $contact){?>
				     	<tr>
				     	  <td><a href="<?=$URL_ROOT?>site/contact/add.php?type=<?=$type?>&id=<?=$contact["id"]?>"><?=$contact["send-date"]?></a></td>
						  <td><?=htmlspecialchars($contact["type"])?></td>
				     	  <td><?=htmlspecialchars($contact["firstname"])?> <?=htmlspecialchars($contact["lastname"])?></td>
				     	  <td><?=htmlspecialchars($contact["email"])?><?=(!empty($contact["phone"])?' / '.htmlspecialchars($contact["phone"]):'')?></td>
				     	  <td><?=htmlspecialchars($contact["lang_name"])?><?=(($contact["lang_name"] AND $contact["location_name"])?' - ':'').htmlspecialchars($contact["location_name"])?></td>
				     	</tr>
						<?}?>
				     </tbody>
				   </table>
			</div>
			<?}?>
		</div>
	</div>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
			  <ul class="pager">
			    <? if ($actual_page<$total_pages){?><li class="previous"><a href="<?=$URL_ROOT?>site/contact/<?='?page='.($actual_page+1)?>&type=<?=$type?>"><?=$txt->nav->prev?></a></li><?}?>
				 <? if ($actual_page>1){?><li class="next"><a href="<?=$URL_ROOT?>site/contact/?type=<?=$type?><?if (($actual_page-1)>1){echo '&page='.($actual_page-1);};?>"><?=$txt->nav->next?></a></li><?}?>
			  </ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>