<?
$is_public = true;
require_once("../php/init.php");
require_once("../site/php/inc-login.php");
$section="home";
$url=$txt->home->url;
$title=$txt->home->title;
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>	
<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-title" content="<?=$txt->logo?>">
<meta name="robots" content="noindex" />
	
<title><?=$title?></title>

<link rel="shortcut icon" href="<?=$URL_ROOT?>assets/ico/favicon.ico" />
<link rel="apple-touch-icon" href="<?=$URL_ROOT?>assets/ico/apple-touch-icon.png" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

<link href="<?=$URL_ROOT?>assets/css/screen.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="<?=$URL_ROOT?>assets/js/modernizr.min.js"></script>

</head>
<body id="login">
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<form role="form" action="?action=login" method="post" id="" enctype="multipart/form-data" autocomplete="off">
				<? if ($error>0 ){?>
					<div class="alert alert-danger">
						<p><?=$txt->form->alert->error->title?></p>
						<p><?=$txt->form->alert->error->text?></p>
						<ol>
							<?if ($validate["login"]["failed"]==1){?>
								<li><?=$txt->form->alert->error->login?></li>
							<?}?>
						</ol>
					</div>
				<?}?>
				
				<div class="row">
					<div class="col-12">
							<h1 id="logo"><a href="<?=$URL_ROOT?>site/"><span><?=$txt->logo?></span></a></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="form-group clearfix">
							<label for="email" class="hidden"><?=$txt->form->login->email->title?></label>
							<input type="email" class="form-control input-lg" name="email" id="email" placeholder="<?=$txt->form->login->email->holder?>" title="<?=$txt->form->login->email->title?>" value="<?=$_POST['email']?>" required autofocus>			    
						</div>
						<div class="form-group">
							<label for="password" class="hidden"><?=$txt->form->login->password->title?></label>
							<input type="password" class="form-control input-lg" name="password" id="password" placeholder="<?=$txt->form->login->password->holder?>" title="<?=$txt->form->login->password->title?>" value="" required>			    
						</div>
						<p><input type="submit" name="submit" class="btn btn-primary" value="<?=$txt->form->submit->signin?>"></p>
					</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script src="<?=$URL_ROOT?>assets/js/jquery-2.1.3.min.js"></script>
<script src="<?=$URL_ROOT?>assets/js/bootstrap.min.js"></script>
</body>
</html>