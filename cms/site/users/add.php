<?
require_once("../../php/init.php");
require_once("../../site/php/inc-general.php");
require_once("../../php/class.upload.php");
require_once("../../site/php/inc-users-add.php");
$section="users";
//if ($id AND $_SESSION['user']['id'] AND $id!=$_SESSION['user']['id']){
	checkPermissions($section);
//}
$title=$txt->users->add->title;
$js[]="change.js";
$js[]="countdown.js";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="users">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1><?=(empty($users_lang["id"])?$txt->users->add->h1:$txt->users->update->h1)?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<? if ($warning>0){?>
			<div class="alert alert-warning">
				<?if ($attention["active"]["draft"]==1){?>
				<p><?=$txt->form->alert->warning->title?></p>
				<?}?>
			</div>
			<?}?>
			<?if ($update=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->update?></p>
			</div>
			<?};?>
			<?if ($insert=="ok"){?>
			<div class="alert alert-success">
				<p><?=$txt->form->alert->success->title?></p>
			</div>
			<?};?>
		</div>
	</div>
	<? if ($error>0 ){?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<p><?=$txt->form->alert->error->title?></p>
				<p><?=$txt->form->alert->error->text?></p>
				<ol>
					<?if ($validate["headline"]["duplicated"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->nickname->title?></strong> <?=$txt->form->alert->error->duplicated?></li>
					<?}?>
					<?if ($validate["headline"]["isnumber"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->nickname->title?></strong> <?=$txt->form->alert->error->isnumber?></li>
					<?}?>
					<?if ($validate["avatar"]["upload"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->avatar->title?></strong> <?=$txt->form->alert->error->required?></li>
					<?}?>
					<?if ($validate["perms"]["noselected"]==1){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->roles->title?></strong> <?=$txt->form->alert->error->hasnoelements?></li>
					<?}?>
					<?if ($validate["avatar"]["upload"]==2){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->avatar->title?></strong> <?=$txt->form->alert->error->uploadimage?></li>
					<?}?>
					<?if ($validate["avatar"]["upload"]==3){?>
						<li><?=$txt->form->alert->error->label?> <strong><?=$txt->form->users->avatar->title?></strong> <?=$txt->form->alert->error->processimage?></li>
					<?}?>
				</ol>
			</div>
		</div>
	</div>
	<?}?>
	<form action="add.php" method="post" enctype="multipart/form-data" autocomplete="off">
		<div class="row">
			<div class="col-md-8">
				<?if ($users_lang["id"]!=""){?><input type="hidden" name="id" value="<?=$users_lang["id"];?>"><?}?>
				<fieldset>
					<legend><?=$txt->form->users->legend?></legend>
					<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["headline"])){?>has-error<?}?>">
					  <label for="nickname"><?=$txt->form->users->nickname->title?> <span>*</span></label>
					  <input type="text" class="form-control" name="nickname" id="nickname" placeholder="<?=$txt->form->users->nickname->holder?>" title="<?=$txt->form->users->nickname->title?>" value="<?=htmlspecialchars(stripslashes($users_lang["nickname"]));?>" required>				    
					</div>
					<div class="form-group">
					  <label for="name"><?=$txt->form->users->name->title?> <span>*</span></label>
					  <input type="text" class="form-control" name="name" id="name" placeholder="<?=$txt->form->users->name->holder?>" title="<?=$txt->form->users->name->title?>" value="<?=htmlspecialchars(stripslashes($users_lang["name"]));?>" required>				    
					</div>
					<div class="form-group">
					  <label for="lastname"><?=$txt->form->users->lastname->title?> <span>*</span></label>
					  <input type="text" class="form-control" name="lastname" id="lastname" placeholder="<?=$txt->form->users->lastname->holder?>" title="<?=$txt->form->users->lastname->title?>" value="<?=htmlspecialchars(stripslashes($users_lang["lastname"]));?>" required>				    
					</div>
					<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["headline"])){?>has-error<?}?>">
					  <label for="email"><?=$txt->form->users->email->title?> <span>*</span></label>
					  <input type="email" class="form-control" name="email" id="email" placeholder="<?=$txt->form->users->email->holder?>" title="<?=$txt->form->users->email->title?>" value="<?=htmlspecialchars(stripslashes($users_lang["email"]));?>" required>				    
					</div>
					<div class="form-group">
					  <label for="password"><?=$txt->form->users->password->title?> <span>*</span></label>
					  <input type="text" class="form-control" name="password" id="password" placeholder="<?=$txt->form->users->password->holder?>" title="<?=$txt->form->users->password->title?>" value="<?=htmlspecialchars(stripslashes($users_lang["password"]));?>" required>			    
					</div>
					<div class="form-group file <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["avatar"])){?>has-error<?}?>">
					  <label for="avatar"><?=$txt->form->users->avatar->title?></label>
					  <div class="selector">
					  	<input type="file"  name="avatar" id="avatar" title="<?=$txt->form->users->avatar->title?>" accept="image/*" />
					  </div>
					  <p class="info"><?=sprintf($txt->form->users->avatar->info, 42, 42)?></p>
					  <?if ($users_lang["avatar"]!=""){?>
					  <p><img src="<?=$URL_ROOT?>uploads/users/<?=$users_lang["avatar"]?>" alt=""/></p>
					   <?}?>
					</div>
				</fieldset>
				<fieldset>
					<legend><?=$txt->form->status->legend?></legend>
					<div class="form-group">
						<label for="meta-desc" class="hide"><?=$txt->form->status->legend?></label>
						<select class="form-control" name="active" id="active" required>
							<option value="Yes" <? if ($users_lang['active']=='Yes' OR empty($users_lang['active'])){?>selected="selected"<?}?> ><?=$txt->form->status->active?></option>
							<option value="Draft" <? if ($users_lang['active']=='Draft'){?>selected="selected"<?}?>><?=$txt->form->status->draft?></option>
						</select>
					</div>
				</fieldset>
			</div>
			<div class="col-md-4">
				<fieldset class="general">
					<legend><?=$txt->form->users->roles->title?></legend>
					<div class="form-group <?if ($error AND $lang_active==$current_lang['lang'] AND !empty($validate["perms"])){?>has-error<?}?>">
					<? /*
						<div class="checkbox">
							<label><input type="checkbox" name="perm_front" id="perm_front" value="Y" <?if ($users_lang["perm_front"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->front?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_products" id="perm_products" value="Y" <?if ($users_lang["perm_products"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->products->models?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_productscategories" id="perm_productscategories" value="Y" <?if ($users_lang["perm_productscategories"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->products->productscategories?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_downloads" id="perm_downloads" value="Y" <?if ($users_lang["perm_downloads"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->downloads->downloads?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_downloadscategories" id="perm_downloadscategories" value="Y" <?if ($users_lang["perm_downloadscategories"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->downloads->downloadscategories?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_projects" id="perm_projects" value="Y" <?if ($users_lang["perm_projects"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->projects?></label>
						</div>
						*/?>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_news" id="perm_news" value="Y" <?if ($users_lang["perm_news"]=='Y'){?>checked="checked"<?}?>> <?=$txt->nav->main->news?></label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_contact" id="perm_contact" value="Y" <?if ($users_lang["perm_contact"]=='Y'){?>checked="checked"<?}?>> <?=$txt->nav->main->contact->title?></label>
						</div>
						<? /*<div class="checkbox">
							<label><input type="checkbox" name="perm_maintenance" id="perm_maintenance" value="Y" <?if ($users_lang["perm_maintenance"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->contact->title?> - <?=$txt->nav->main->contact->maintenance?></label>
						</div> */?>
						<? /*<div class="checkbox">
							<label><input type="checkbox" name="perm_documentation" id="perm_documentation" value="Y" <?if ($users_lang["perm_documentation"]=='Y'){?>checked="checked"<?}?>><?=$txt->nav->main->contact->title?> - <?=$txt->nav->main->contact->documentation?></label>
						</div>*/?>
						<div class="checkbox">
							<label><input type="checkbox" name="perm_users" id="perm_users" value="Y" <?if ($users_lang["perm_users"]=='Y'){?>checked="checked"<?}?>> <?=$txt->nav->footer->users?></label>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<div class="row">
	<div class="col-md-12">
		<div class="form-group">
		  <p class="required"><span>*</span> <?=$txt->form->required->title?></p>
		  <?if($_SESSION['user']['id']==1 OR $id!=1){?>
		  <p><input type="submit" name="submit" class="btn btn-primary" value="<?=$users_lang["id"]!=""?$txt->form->submit->users->update:$txt->form->submit->users->add?>"> 
		  <?if ($id!=1){?><?if ($users_lang["id"]!=""){?><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#erase-es"><?=$txt->form->submit->users->erase?></button><?}?><?}?></p>
		  <?}?>
		  <div class="modal fade" id="erase-es" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
				  <p class="h2 modal-title"><?=$txt->form->submit->users->erase?></p>
				</div>
				<div class="modal-body">
				  <p><?=$txt->form->submit->users->modal?></p>
				</div>
				
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal"><?=$txt->nav->cancel?></button>
				 <a class="btn btn-success" href="?id=<?=$id?>&action=delete"><?=$txt->nav->confirm?></a>
				</div>
				
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div>
	</form>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>