<?
require_once("../../php/init.php");
require_once("../../site/php/inc-users.php");
$section="users";
checkPermissions($section);
$title=$txt->users->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="users">
<?require("{$DOC_ROOT}site/includes/header.php")?>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<h1><?=$txt->users->h1?></h1>
		</div>
		<div class="col-sm-4 col-md-4">
			<p class="link link-add"><a class="btn btn-primary plus" href="<?=$URL_ROOT?>site/users/add.php"><?=$txt->users->link?></a></p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?if(!empty($users)){?>
			<div class="table-responsive">
				<table class="table table-striped">
				     <thead>
				       <tr>
				         <th><?=$txt->users->table->nickname?></th>
				         <th><?=$txt->users->table->name?></th>
				         <th><?=$txt->users->table->lastname?></th>
				       </tr>
				     </thead>
				     <tbody>
						<?foreach ($users as $current_user){?>
				     	<tr>
				     	  <td><a href="<?=$URL_ROOT?>site/users/add.php?id=<?=$current_user["id"]?>"><?=htmlspecialchars($current_user["nickname"])?></a></td>
				     	  <td><?=htmlspecialchars($current_user["name"])?></td>
				     	  <td><?=htmlspecialchars($current_user["lastname"])?></td>
				     	</tr>
						<?}?>
				     </tbody>
				   </table>
			   </div>
			<?}?>
		</div>
	</div>
	<? if ($actual_page>1 or $actual_page<$total_pages){?>
	<div class="row">
		<div class="col-md-12">
			<nav>
			  <ul class="pager">
			    <? if ($actual_page<$total_pages){?><li class="previous"><a href="<?=$URL_ROOT?>site/users/<?='?page='.($actual_page+1)?>"><?=$txt->nav->prev?></a></li><?}?>
				 <? if ($actual_page>1){?><li class="next"><a href="<?=$URL_ROOT?>site/users/<?if (($actual_page-1)>1){echo '?page='.($actual_page-1);};?>"><?=$txt->nav->next?></a></li><?}?>
			  </ul>
			</nav>
		</div>
	</div>
	<?}?>
</div>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>