Options -Indexes
Options +FollowSymlinks

RewriteEngine on

# Protejo los directorios .git y los archivos .git* para que no sean accesibles, tienen información del source code
RedirectMatch 404 /\.git

### Manu
# Attempt to load files from production if
# they're not in our local version
# RewriteCond %{REQUEST_FILENAME} !-d
# RewriteRule ^uploads/(.*) \
# https://www.wind2grid.com/uploads/$1 [NC,L]
### Fin Manu

# CONSTANTE PARA EL PROTOCOLO, HTTP OR HTTPS -----
RewriteCond %{HTTPS}s ^(on(s)|offs)$
RewriteRule ^ - [env=s:%2]
# FIN CONSTANTE ----------------------------------

# main domain (si no es www.wind2grid.com, www.xxx.fr, www.xxx.de, como ejemplo estos últimos de un addon domain sin idioma)
RewriteCond %{HTTP_HOST} !^www\.wind2grid\.localhost\.com$ [NC]
RewriteCond %{HTTP_HOST} !^(|wind2grid\.)localhost(|:8887|:8056)$ [NC]

# la siguiente la descomento en local
RewriteRule ^(.*)$ http://%1localhost%2/wind2grid/$1 [R=301,L]


# index.php to / Cuando metan index.php lo quito para que se quede en / también para subcarpetas
RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /.*index\.php\ HTTP/
RewriteRule ^(.*)index\.php$ /wind2grid/$1 [R=301,L]

#
# Trailing slash check Cuando no se ponga la barra al final, lo redirecciono 301 a la url con la barra al final.

# Don't fix direct file links
RewriteCond %{REQUEST_FILENAME} !-f

# Ni las requesturis de .../gracias.html, o cualquier otra que sea un archivo(punto algo) .html, .js, .css, .jpg, .png, etc.. 
RewriteCond %{REQUEST_URI} !\.(.+)$

RewriteCond %{REQUEST_URI} !(.*)/$
RewriteRule ^(.*)$ http%{ENV:s}://%{HTTP_HOST}/wind2grid/$1/ [L,R=301]

#<IfModule mod_expires.c>
#ExpiresActive On
#ExpiresDefault "access plus 1 month"
#ExpiresByType image/x-icon "access plus 1 year"
#ExpiresByType image/gif "access plus 1 month"
#ExpiresByType image/png "access plus 1 month"
#ExpiresByType image/jpg "access plus 1 month"
#ExpiresByType image/jpeg "access plus 1 month"
#ExpiresByType text/css "access 1 month"
#ExpiresByType application/javascript "access plus 1 year"
#</IfModule>

# Redireccionar correo
# RewriteRule  ^correo/?$ https://login.microsoftonline.com/es [L] 
# RewriteRule  ^correo/(.*)$ https://login.microsoftonline.com/es [L] 

# Redirect 301 ----------------------------------------------------------

# REDIRECTS ESPAÑOL ------------------------------------------------

# REDIRECTS INGLES ------------------------------------------------

# REDIRECTS FRANCES --------------------------------------------------------

# Fin redirects 301

# ERROR HANDLING ----------------------------------------------------------

# Error 404 para imágenes/ archivos que no sean páginas o documentos
<filesmatch "\.(jpg|JPG|gif|GIF|png|PNG|css|ico)$">
ErrorDocument 404 "/wind2grid/assets/img/no-image.png"
</filesmatch>

# Resto de páginas

ErrorDocument 400 /wind2grid/site/error.php?s1=error&error=400
ErrorDocument 401 /wind2grid/site/error.php?s1=error&error=401
ErrorDocument 403 /wind2grid/site/error.php?s1=error&error=403
ErrorDocument 404 /wind2grid/site/error.php?s1=error&error=404
ErrorDocument 500 /wind2grid/site/error.php?s1=error&error=500

# QUERY STRING - Creo una variable de entorno con el query string para poder añadirlo cuando quiera %{ENV:Qs}&
RewriteCond %{QUERY_STRING} ^(.+)$ [NC]
RewriteRule .? - [E=Qs:%1]   # Copy QueryString in Qs Enviroment variable

# PRIMERO LAS URLS DE DOMINIOS SIN IDIOMA

#--------------------------------------------------------------------------------------------

# Europa wind2grid.com

# --- URLS CON(AJAX) O SIN QUERY STRING

# -- OTHER PAGES --- #

# Para los dominios que no sean localhost me salto todas estas reglas que son aplicables únicamente a los dominios localhost
RewriteCond %{HTTP_HOST} !^www\.wind2grid\.localhost\.com$ [NC]
RewriteCond %{HTTP_HOST} !^(|wind2grid\.)localhost(|:8887|:8056)$ [NC]
RewriteRule .? - [S=23]   # me salto las siguientes 23 Reglas
RewriteRule ^(400|401|403|404|500)/$ /wind2grid/site/error.php?s1=error&error=$1 [L]
RewriteRule ^challenge/$ /wind2grid/site/challenge/challenge.php?s1=challenge [L]
RewriteRule ^project/$ /wind2grid/site/project/project.php?s1=project [L]
RewriteRule ^consortium/$ /wind2grid/site/consortium/consortium.php?s1=consortium [L]
RewriteRule ^contact/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto [L]
RewriteRule ^contact/sent/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto&status=success [L]
RewriteRule ^downloads/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^downloads/([0-9]+)/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]
RewriteRule ^downloads/([^\.]+)\.([^/]+)$  /wind2grid/site/downloads/downloads.php?s1=downloads&s2=download&slug=$1 [L]
RewriteRule ^scoopitnews/$ /wind2grid/site/scoopitnews/scoopitnews.php?s1=scoopitnews [L]
RewriteRule ^news/$ /wind2grid/site/news/news.php?s1=news [L]
RewriteRule ^news/category/([^/]+)/([0-9]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$1&page=$2 [L]
RewriteRule ^news/category/([^/]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$1 [L]
RewriteRule ^news/([0-9]+)/$ /wind2grid/site/news/news.php?s1=news&page=$1 [L]
RewriteRule ^news/([^/]+)/$ /wind2grid/site/news/add.php?s1=news&s2=new&slug=$1 [L]
RewriteRule ^legal-notice/$ /wind2grid/site/legal/legal.php?s1=legal [L]
RewriteRule ^privacy-policy/$ /wind2grid/site/privacy/privacy.php?s1=privacy [L]
RewriteRule ^cookies-policy/$ /wind2grid/site/cookies/cookies.php?s1=cookies [L]
RewriteRule ^assets/js/projects\.js$ /wind2grid/assets/js/projects\.js\.php [L]
RewriteRule ^assets/js/contact\.js$ /wind2grid/assets/js/contact\.js\.php [L]
RewriteRule ^assets/js/recaptcha3\.js$ /wind2grid/assets/js/recaptcha3\.js\.php [L]
RewriteRule ^assets/js/newsletter\.js$ /wind2grid/assets/js/newsletter\.js\.php [L]
RewriteRule ^$ /wind2grid/site/?s1=home [L]

# -------------------------------------------------------------------------------------------

# AHORA LAS URLS DE DOMINIOS CON IDIOMA


RewriteRule ^(en)(|-[a-z]{2})/(400|401|403|404|500)/$ /wind2grid/site/error.php?s1=error&error=$2 [L]

RewriteRule ^en(|-[a-z]{2})/mail_test/$ /wind2grid/site/mail_test.php [L]

# páginas en Ingles ----------------------------------------------------------


# --- URLS CON(AJAX) O SIN QUERY STRING
# -- OTHER PAGES --- #

	# Challenge
RewriteRule ^en(|-[a-z]{2})/challenge/$ /wind2grid/site/challenge/challenge.php?s1=challenge [L]

	# project
RewriteRule ^en(|-[a-z]{2})/project/$ /wind2grid/site/project/project.php?s1=project [L]

	# consortium
RewriteRule ^en(|-[a-z]{2})/consortium/$ /wind2grid/site/consortium/consortium.php?s1=consortium [L]

#-------Desarrollo--------------------

	# Repositorio de elementos
RewriteRule ^en(|-[a-z]{2})/repositorio/$ /wind2grid/site/repositorio.php?s1=repositorio [L]

#-------Fin urls para desarrollo-------

	    # Contact form (with query string[if ajax] and without querys string)
RewriteRule ^en(|-[a-z]{2})/contact/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto [L]
RewriteRule ^en(|-[a-z]{2})/contact/sent/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto&status=success [L]

	# sectorial news
RewriteRule ^en(|-[a-z]{2})/scoopitnews/$ /wind2grid/site/scoopitnews/scoopitnews.php?s1=scoopitnews [L]

		# News Home
RewriteRule ^en(|-[a-z]{2})/news/$ /wind2grid/site/news/news.php?s1=news [L]

		# News Categoria
RewriteRule ^en(|-[a-z]{2})/news/category/([^/]+)/([0-9]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2&page=$3 [L]

		# News Paginacion
RewriteRule ^en(|-[a-z]{2})/news/category/([^/]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2 [L]
RewriteRule ^en(|-[a-z]{2})/news/([0-9]+)/$ /wind2grid/site/news/news.php?s1=news&page=$2 [L]

		# News Ficha
RewriteRule ^en(|-[a-z]{2})/news/([^/]+)/$ /wind2grid/site/news/add.php?s1=news&s2=new&slug=$2 [L]
		
	# Legal Advice
RewriteRule ^en(|-[a-z]{2})/legal-notice/$ /wind2grid/site/legal/legal.php?s1=legal [L]

	# Politica privacidad
RewriteRule ^en(|-[a-z]{2})/privacy-policy/$ /wind2grid/site/privacy/privacy.php?s1=privacy [L]

	# Cookies Policy
RewriteRule ^en(|-[a-z]{2})/cookies-policy/$ /wind2grid/site/cookies/cookies.php?s1=cookies [L]

	# PROJECTS MAPS JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/projects\.js$ /wind2grid/assets/js/projects\.js\.php [L]
		
	# CONTACT MAPS JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/contact\.js$ /wind2grid/assets/js/contact\.js\.php [L]

	# RECAPCHA3 JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/recaptcha3\.js$ /wind2grid/assets/js/recaptcha3\.js\.php [L]

	# NEWSLETTER JS
RewriteRule	 ^en(|-[a-z]{2})/assets/js/newsletter\.js$ /wind2grid/assets/js/newsletter\.js\.php [L]

# Descargas Home
RewriteRule ^en(|-[a-z]{2})/downloads/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^en(|-[a-z]{2})/downloads/([0-9]+)/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]

# Descargas Fichero
RewriteRule ^en(|-[a-z]{2})/downloads/([^\.]+)\.([^/]+)$  /wind2grid/site/downloads/downloads.php?s1=downloads&s2=download&slug=$2 [L]


# páginas en Español ----------------------------------------------------------

# --- URLS CON(AJAX) O SIN QUERY STRING
# -- OTHER PAGES --- #

	# Challenge
RewriteRule ^es(|-[a-z]{2})/reto/$ /wind2grid/site/challenge/challenge.php?s1=challenge [L]

	# project
RewriteRule ^es(|-[a-z]{2})/proyecto/$ /wind2grid/site/project/project.php?s1=project [L]

	# consortium
RewriteRule ^es(|-[a-z]{2})/consorcio/$ /wind2grid/site/consortium/consortium.php?s1=consortium [L]

	# Contact form (with query string[if ajax] and without querys string)
RewriteRule ^es(|-[a-z]{2})/contacto/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto [L]
RewriteRule ^es(|-[a-z]{2})/contacto/enviado/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto&status=success [L]

	# News Home
RewriteRule ^es(|-[a-z]{2})/noticias/$ /wind2grid/site/news/news.php?s1=news [L]

	# sectorial news
RewriteRule ^es(|-[a-z]{2})/noticiasscoopit/$ /wind2grid/site/scoopitnews/scoopitnews.php?s1=scoopitnews [L]

	# News Categoria
RewriteRule ^es(|-[a-z]{2})/noticias/category/([^/]+)/([0-9]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2&page=$3 [L]

	# News Paginacion
RewriteRule ^es(|-[a-z]{2})/noticias/category/([^/]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2 [L]
RewriteRule ^es(|-[a-z]{2})/noticias/([0-9]+)/$ /wind2grid/site/news/news.php?s1=news&page=$2 [L]

	# News Ficha
RewriteRule ^es(|-[a-z]{2})/noticias/([^/]+)/$ /wind2grid/site/news/add.php?s1=news&s2=new&slug=$2 [L]
		
	# Legal Advice
RewriteRule ^es(|-[a-z]{2})/aviso-legal/$ /wind2grid/site/legal/legal.php?s1=legal [L]

	# Politica privacidad
RewriteRule ^es(|-[a-z]{2})/politica-privacidad/$ /wind2grid/site/privacy/privacy.php?s1=privacy [L]

	# Cookies Policy
RewriteRule ^es(|-[a-z]{2})/politica-cookies/$ /wind2grid/site/cookies/cookies.php?s1=cookies [L]

	# PROJECTS MAPS JS
RewriteRule	 ^es(|-[a-z]{2})/assets/js/projects\.js$ /wind2grid/assets/js/projects\.js\.php [L]
		
	# CONTACT MAPS JS
RewriteRule	 ^es(|-[a-z]{2})/assets/js/contact\.js$ /wind2grid/assets/js/contact\.js\.php [L]

	# RECAPCHA3 JS
RewriteRule	 ^es(|-[a-z]{2})/assets/js/recaptcha3\.js$ /wind2grid/assets/js/recaptcha3\.js\.php [L]

	# NEWSLETTER JS
RewriteRule	 ^es(|-[a-z]{2})/assets/js/newsletter\.js$ /wind2grid/assets/js/newsletter\.js\.php [L]

    # Descargas Home
RewriteRule ^es(|-[a-z]{2})/descargas/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^es(|-[a-z]{2})/descargas/([0-9]+)/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]

    # Descargas Fichero
RewriteRule ^es(|-[a-z]{2})/descargas/([^\.]+)\.([^/]+)$  /wind2grid/site/downloads/downloads.php?s1=downloads&s2=download&slug=$2 [L]


# páginas en Euskara ----------------------------------------------------------

# --- URLS CON(AJAX) O SIN QUERY STRING
# -- OTHER PAGES --- #

	# Challenge
RewriteRule ^eu(|-[a-z]{2})/erronka/$ /wind2grid/site/challenge/challenge.php?s1=challenge [L]

	# project
RewriteRule ^eu(|-[a-z]{2})/proiektua/$ /wind2grid/site/project/project.php?s1=project [L]

	# consortium
RewriteRule ^eu(|-[a-z]{2})/partzuergoa/$ /wind2grid/site/consortium/consortium.php?s1=consortium [L]

	# Contact form (with query string[if ajax] and without querys string)
RewriteRule ^eu(|-[a-z]{2})/kontaktua/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto [L]
RewriteRule ^eu(|-[a-z]{2})/kontaktua/bidalita/$ /wind2grid/site/contact/contact.php?%{ENV:Qs}&s1=contacto&status=success [L]

	# scoopit news
RewriteRule ^eu(|-[a-z]{2})/scoopitberriak/$ /wind2grid/site/scoopitnews/scoopitnews.php?s1=scoopitnews [L]

		# News Home
RewriteRule ^eu(|-[a-z]{2})/berriak/$ /wind2grid/site/news/news.php?s1=news [L]

		# News Categoria
RewriteRule ^eu(|-[a-z]{2})/berriak/category/([^/]+)/([0-9]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2&page=$3 [L]

		# News Paginacion
RewriteRule ^eu(|-[a-z]{2})/berriak/category/([^/]+)/$ /wind2grid/site/news/category.php?s1=news&s2=category&category=$2 [L]
RewriteRule ^eu(|-[a-z]{2})/berriak/([0-9]+)/$ /wind2grid/site/news/news.php?s1=news&page=$2 [L]

		# News Ficha
RewriteRule ^eu(|-[a-z]{2})/berriak/([^/]+)/$ /wind2grid/site/news/add.php?s1=news&s2=new&slug=$2 [L]

	# Legal Advice
RewriteRule ^eu(|-[a-z]{2})/lege-oharra/$ /wind2grid/site/legal/legal.php?s1=legal [L]

	# Politica privacidad
RewriteRule ^eu(|-[a-z]{2})/pribatasun-politika/$ /wind2grid/site/privacy/privacy.php?s1=privacy [L]

	# Cookies Policy
RewriteRule ^eu(|-[a-z]{2})/cookien-politika/$ /wind2grid/site/cookies/cookies.php?s1=cookies [L]

	# PROJECTS MAPS JS
RewriteRule	 ^eu(|-[a-z]{2})/assets/js/projects\.js$ /wind2grid/assets/js/projects\.js\.php [L]
		
	# CONTACT MAPS JS
RewriteRule	 ^eu(|-[a-z]{2})/assets/js/contact\.js$ /wind2grid/assets/js/contact\.js\.php [L]

	# RECAPCHA3 JS
RewriteRule	 ^eu(|-[a-z]{2})/assets/js/recaptcha3\.js$ /wind2grid/assets/js/recaptcha3\.js\.php [L]

	# NEWSLETTER JS
RewriteRule	 ^eu(|-[a-z]{2})/assets/js/newsletter\.js$ /wind2grid/assets/js/newsletter\.js\.php [L]

# Descargas Home
RewriteRule ^eu(|-[a-z]{2})/deskargak/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads [L]
RewriteRule ^eu(|-[a-z]{2})/deskargak/([0-9]+)/$  /wind2grid/site/downloads/downloads.php?%{ENV:Qs}&s1=downloads&page=$2 [L]

	# Descargas Fichero
RewriteRule ^eu(|-[a-z]{2})/deskargak/([^\.]+)\.([^/]+)$  /wind2grid/site/downloads/downloads.php?s1=downloads&s2=download&slug=$2 [L] 

# General ----------------------------------------------------------
RewriteRule ^(en|es|eu)(|-[a-z]{2})-php/(.*)$ /wind2grid/site/php/$3 [L]
RewriteRule ^(en|es|eu)(|-[a-z]{2})/(.+)$ /wind2grid/site/$3 [L]
RewriteRule ^(en|es|eu)(|-[a-z]{2})/$ /wind2grid/site/index.php?s1=home [L]

