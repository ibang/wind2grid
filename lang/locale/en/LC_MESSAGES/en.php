<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-02-27 12:26+0100
PO-Revision-Date: 2021-05-06 08:56+0200
Language: en
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
',
      ),
      'THE CHALLENGE' => 
      array (
        0 => '',
      ),
      'THE PROJECT' => 
      array (
        0 => '',
      ),
      'CONSORTIUM' => 
      array (
        0 => '',
      ),
      'ACTIVITIES' => 
      array (
        0 => '',
      ),
      'NEWS' => 
      array (
        0 => '',
      ),
      'CONTACT' => 
      array (
        0 => '',
      ),
      'Downloads' => 
      array (
        0 => '',
      ),
      'Contact' => 
      array (
        0 => '',
      ),
      'AN INNOVATIVE CONCEPT OF <span class=\'orange\'>INTEGRATED FLOATING SUBSTATION</span> DEVELOPED IN BASQUE COUNTRY' => 
      array (
        0 => '',
      ),
      'Collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating substation.' => 
      array (
        0 => '',
      ),
      'FLOATING OFFSHORE SUBSTATIONS, AN EXCELLENT OPPORTUNITY' => 
      array (
        0 => '',
      ),
      'At present, there are no commercial floating substations in operation. Accordingly, the market opportunity that will be created by developing floating and fixed offshore wind power at increasing depths goes hand-in-hand with a lack of developers with recognised experience in the field. Together, this creates a unique and unmissable window of opportunity, considering the wealth of experience in offshore wind power technology that we have accumulated in the Basque Country.' => 
      array (
        0 => '',
      ),
      'The following participants are involved in this project:' => 
      array (
        0 => '',
      ),
      'COORDINATOR' => 
      array (
        0 => '',
      ),
      'Technology centres' => 
      array (
        0 => '',
      ),
      'Expert Technical Advisors' => 
      array (
        0 => '',
      ),
      'Clusters' => 
      array (
        0 => '',
      ),
      'ACTIVITIES<br>AND NEWS' => 
      array (
        0 => '',
      ),
      'Keep up to date with all the latest activities and results from the project and developments in the sector.' => 
      array (
        0 => '',
      ),
      'ALL THE NEWS' => 
      array (
        0 => '',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => '',
      ),
      'CONTACT WIND2GRID' => 
      array (
        0 => '',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => '',
      ),
      'Basque Energy Cluster' => 
      array (
        0 => '',
      ),
      'The Basque Maritime Forum' => 
      array (
        0 => '',
      ),
      'As the offshore wind power sector evolves, it faces a series of challenges relating to structural design, the creation of new products, the optimisation of existing products and the use of operating information to help reduce costs, opening up a great potential market undergoing immense development. This applies to turbines and power evacuation infrastructures, <strong>one of the pivotal elements being the substation</strong>.' => 
      array (
        0 => '',
      ),
      'SIX KEY TRENDS' => 
      array (
        0 => '',
      ),
      'Growing increase of turbine power and bigger wind farms at greater depths.' => 
      array (
        0 => '',
      ),
      'Developing floating solutions for substations at great depths. Flexible, reliable and modular, they can be adapted to a range of powers and demands.' => 
      array (
        0 => '',
      ),
      'Innovative materials and antidegradation systems.' => 
      array (
        0 => '',
      ),
      'Digitalised substations for optimised operation.' => 
      array (
        0 => '',
      ),
      'Improved O&M activities for offshore substations.' => 
      array (
        0 => '',
      ),
      'Costs reduction: CAPEX and OPEX.' => 
      array (
        0 => '',
      ),
      'The <strong>WIND2GRID</strong> consortium will develop new solutions to address applied research to these 6 trends. There is no commercial floating substation on the market today.' => 
      array (
        0 => '',
      ),
      'New market opportunity with the development of floating and fixed wind farms at ever greater depths + shortage of developers with recognised experience = window of opportunities for businesses in the wind and maritime sectors.' => 
      array (
        0 => '',
      ),
      'The Basque wind and maritime sectors encompass more than 200 businesses with years of experience in developing offshore wind technology, all of whom may benefit from this new energy sector trend.' => 
      array (
        0 => '',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => '',
      ),
      'See project phases and the role of each participant.' => 
      array (
        0 => '',
      ),
      '<strong>WIND2GRID</strong> aims to promote collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating offshore substation.' => 
      array (
        0 => '',
      ),
      'Main aims' => 
      array (
        0 => '',
      ),
      'To develop new floating structure concepts valid for multiple locations, including shallow (60-100 m) and deep (around 1000 m) waters.' => 
      array (
        0 => '',
      ),
      'Advanced numerical modelling applied to research into the hydrodynamic behaviour of complex structures and dynamic umbilical cables.' => 
      array (
        0 => '',
      ),
      'Research into the dynamics of the mooring systems of large structures at varying depths.' => 
      array (
        0 => '',
      ),
      'Adaptability to future networks, research into novel power transmission systems (HVAC, HVDC).' => 
      array (
        0 => '',
      ),
      'Research into specific high-performance concrete for marine structures.' => 
      array (
        0 => '',
      ),
      'Research into hybrid coatings for rust-proofing metal components.' => 
      array (
        0 => '',
      ),
      'Implantation of Lean Manufacturing concepts and BIM methodologies for large marine structures.' => 
      array (
        0 => '',
      ),
      'Innovative submarine cable repair systems to reduce O&M costs.' => 
      array (
        0 => '',
      ),
      'Augmented reality and virtual reality concepts to optimise O&M (developing a virtual model of the substation).' => 
      array (
        0 => '',
      ),
      'Work packages' => 
      array (
        0 => '',
      ),
      '<h3 class=\'introDestacado\'><span class=\'workBlue\'>WP0.</span> Project coordination</h3>' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP1.</strong></span> Design conditions and requirement specification' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP4.</strong></span> Advanced manufacturing and integrated logistics' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP2.</strong></span> Conceptual development of floating substation' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP5.</strong></span> Innovations in O&M for offshore substations' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP3.</strong></span> New materials and antidegradation systems' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP6.</strong></span> Dissemination and exploitation of results' => 
      array (
        0 => '',
      ),
      'WP0. Project coordination' => 
      array (
        0 => '',
      ),
      'The purpose of the work package is to ensure the achievement of the technical objectives set and to monitor the progress of the project.' => 
      array (
        0 => '',
      ),
      'The technical progress of the project will be monitored to ensure the achievement of the objectives and the economic monitoring of the project\'s development to avoid significant deviations in the budget.' => 
      array (
        0 => '',
      ),
      'WP1. Design conditions and requirement specification' => 
      array (
        0 => '',
      ),
      'WP LEADER' => 
      array (
        0 => '',
      ),
      'Participants' => 
      array (
        0 => '',
      ),
      'The purpose of the first work package is to define the fundamentals of design to be implemented in all work packages.' => 
      array (
        0 => '',
      ),
      'The fundamentals of design to be defined will enable: (i) addressing the development of different subcomponents; (ii) validating the design; and (iii) implementing it in case studies.' => 
      array (
        0 => '',
      ),
      'WP2. Conceptual development of floating substation' => 
      array (
        0 => '',
      ),
      'The purpose of this work package is to develop an innovative solution for a floating substation.' => 
      array (
        0 => '',
      ),
      'Based on the design conditions identified in WP1, a comprehensive design will be undertaken, including the floating structure, the topside and the mooring system.' => 
      array (
        0 => '',
      ),
      'The dynamics of interarray and export cables will be analysed to define their design conditions. The development of key ancillary elements, including access and evacuation systems, will be addressed.' => 
      array (
        0 => '',
      ),
      'WP3. New materials and antidegradation systems' => 
      array (
        0 => '',
      ),
      'The purpose of this work package is to analyse new materials for use in offshore structures.' => 
      array (
        0 => '',
      ),
      'It will focus on two types of materials: coatings for the protection against offshore erosion, and new concretes for large offshore structures.' => 
      array (
        0 => '',
      ),
      'In the case of coatings, the goal is the development and validation of a new coating.' => 
      array (
        0 => '',
      ),
      'In parallel, work will be carried out on the development of ultra-high performance concrete in order to increase the resistance of large structures while reducing weight.' => 
      array (
        0 => '',
      ),
      'In both cases, the ultimate goal is cost-reduction of offshore structures.' => 
      array (
        0 => '',
      ),
      'WP4. Advanced manufacturing and integrated logistics' => 
      array (
        0 => '',
      ),
      'The purpose of this work package is to demonstrate the technical and financial viability of floating substation operations to transport topside.' => 
      array (
        0 => '',
      ),
      'It seeks to define the means and procedures needed to manufacture, transport and install a floating substation in each of the phases of development of a construction project.' => 
      array (
        0 => '',
      ),
      'The manufacture, installation and operation of the topside, the floating structure, the mooring, and the cables will condition the design. Such conditions must be identified in the early design stages so that the elements facilitating the operations are included from the beginning.' => 
      array (
        0 => '',
      ),
      'WP5. Innovations in O&M for offshore substations' => 
      array (
        0 => '',
      ),
      'The final purpose of this work package is cost-reduction. In this case, it will be achieved through the improvement of different activities related to the operation and maintenance phase:' => 
      array (
        0 => '',
      ),
      'Development and validation of an undersea tool for repairing offshore cables, which would imply a cost reduction in state-of-the-art technology. The goal is to develop a concept tool and a set of manoeuvres that will help reduce the work time.' => 
      array (
        0 => '',
      ),
      'Reduction of installation time lines and provision of greater traceability of the devices installed by means of the development of a real-time management logic platform.' => 
      array (
        0 => '',
      ),
      'Production of a virtual model of a floating substation allowing systems and subsystems to be visualised, so it can be used as tool for maintenance optimisation.' => 
      array (
        0 => '',
      ),
      'WP6. Dissemination and exploitation of results' => 
      array (
        0 => '',
      ),
      'This will contribute to place the Consortium members as well as Basque companies at the technological and industrial forefront of the power sector, both in the Basque Country and internationally.' => 
      array (
        0 => '',
      ),
      'A plan to exploit the project results will be defined and launched. This plan will focus on technological development, as well as facilitating training for project partners and Basque supply chain participants in the offshore wind power sector.' => 
      array (
        0 => '',
      ),
      'FURTHER INFORMATION ABOUT THE CONSORTIUM' => 
      array (
        0 => '',
      ),
      'Information related to the companies participating in the consortium and their role' => 
      array (
        0 => '',
      ),
      'COLLABORATION BETWEEN 11 BENCHMARK BASQUE COMPANIES IN THE WIND AND MARINE SECTORS' => 
      array (
        0 => '',
      ),
      'Eleven companies and two technology centres have come together in the WIND2GRID project to design and develop a new floating substation concept. The project is expected to help improve the position of the Basque industry in the offshore wind sector and allow the Basque maritime sector occupy the best market position for its future development.' => 
      array (
        0 => '',
      ),
      'Leading wind sector engineering firm and project coordinator' => 
      array (
        0 => '',
      ),
      'Manufacturer of handling tools and systems (grapples, tongs, etc.)' => 
      array (
        0 => '',
      ),
      'Specialists in fastening solutions' => 
      array (
        0 => '',
      ),
      'Specialist in surface treatments and new materials' => 
      array (
        0 => '',
      ),
      'Leading international offshore wind operator' => 
      array (
        0 => '',
      ),
      'Manufacturer of O&M ships for offshore wind farms' => 
      array (
        0 => '',
      ),
      'Developer of floating platforms' => 
      array (
        0 => '',
      ),
      'Manufacturer of transition parts' => 
      array (
        0 => '',
      ),
      'Manufacturers of high-quality concretes' => 
      array (
        0 => '',
      ),
      'Leader in electrical facilities for naval and offshore sectors, power, industry, etc. Key O&M supplier' => 
      array (
        0 => '',
      ),
      'Sector leader in construction and monitoring of concrete structures' => 
      array (
        0 => '',
      ),
      'Technology<br>centers' => 
      array (
        0 => '',
      ),
      'SEE ACTIVITIES' => 
      array (
        0 => '',
      ),
      'See activities and developments in projects' => 
      array (
        0 => '',
      ),
      'Keep up to date with our latest activities.' => 
      array (
        0 => '',
      ),
      'SEE LATEST NEWS' => 
      array (
        0 => '',
      ),
      'See all the sector news' => 
      array (
        0 => '',
      ),
      'ALL LATEST NEWS' => 
      array (
        0 => '',
      ),
      'DIGITALISATION<br />COMPANIES' => 
      array (
        0 => '',
      ),
      'BASQUE NETWORK<br />AGENTS' => 
      array (
        0 => '',
      ),
      'WORKING GROUP ACTIVITIES' => 
      array (
        0 => '',
      ),
      'Launch meeting of the Wind Turbine Data Sharing Work Group.' => 
      array (
        0 => '',
      ),
      'Bilateral meetings to compile the specifications of the data demanded by component manufacturers.' => 
      array (
        0 => '',
      ),
      '<strong>Filter</strong>:' => 
      array (
        0 => '',
      ),
      'See the latest news and developments surrounding Daekin' => 
      array (
        0 => '',
      ),
      'Scroll Up' => 
      array (
        0 => 'Subir',
      ),
      'All' => 
      array (
        0 => '',
      ),
      'Activities' => 
      array (
        0 => '',
      ),
      'Outcomes' => 
      array (
        0 => '',
      ),
      'PREVIOUS' => 
      array (
        0 => '',
      ),
      'NEXT' => 
      array (
        0 => '',
      ),
      'DAEKIN launched at Digitalisation Technology Forum' => 
      array (
        0 => '',
      ),
      'SECTOR NEWS' => 
      array (
        0 => '',
      ),
      'Energy Cluster Association' => 
      array (
        0 => '',
      ),
      'Read more' => 
      array (
        0 => '',
      ),
      'Back to' => 
      array (
        0 => '',
      ),
      'News' => 
      array (
        0 => '',
      ),
    ),
  ),
);