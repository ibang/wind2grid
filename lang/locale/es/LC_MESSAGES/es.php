<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-05-20 14:18+0200
PO-Revision-Date: 2021-05-06 17:26+0200
Language: es
X-Generator: Poedit 2.4.3
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
X-Poedit-SearchPathExcluded-16: assets/js
',
      ),
      'THE CHALLENGE' => 
      array (
        0 => 'RETO',
      ),
      'THE PROJECT' => 
      array (
        0 => 'PROYECTO',
      ),
      'CONSORTIUM' => 
      array (
        0 => 'CONSORCIO',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => 'EL CONSORCIO',
      ),
      'ACTIVITIES' => 
      array (
        0 => 'ACTIVIDADES',
      ),
      'NEWS' => 
      array (
        0 => 'NOTICIAS',
      ),
      'CONTACT' => 
      array (
        0 => 'CONTACTO',
      ),
      'Downloads' => 
      array (
        0 => 'Descargas',
      ),
      'Contact' => 
      array (
        0 => 'Contacto',
      ),
      'AN INNOVATIVE CONCEPT OF <span class=\'orange\'>INTEGRATED FLOATING SUBSTATION</span> DEVELOPED IN BASQUE COUNTRY' => 
      array (
        0 => 'UN CONCEPTO INNOVADOR DE <span class=\'orange\'>SUBESTACIÓN FLOTANTE</span> DESARROLLADA EN EL PAÍS VASCO',
      ),
      'Collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating substation.' => 
      array (
        0 => 'Investigación y desarrollo colaborativo e integrador de tecnologías, componentes y sistemas para subestaciones offshore de nuevas prestaciones, alrededor de un desarrollo de subestación flotante.',
      ),
      'FLOATING OFFSHORE SUBSTATIONS, AN EXCELLENT OPPORTUNITY' => 
      array (
        0 => 'SUBESTACIONES FLOTANTES, NICHO DE OPORTUNIDAD',
      ),
      'At present, there are no commercial floating substations in operation. Accordingly, the market opportunity that will be created by developing floating and fixed offshore wind power at increasing depths goes hand-in-hand with a lack of developers with recognised experience in the field. Together, this creates a unique and unmissable window of opportunity, considering the wealth of experience in offshore wind power technology that we have accumulated in the Basque Country.' => 
      array (
        0 => 'No existe hoy en día ninguna subestación flotante comercial. Por tanto, a la oportunidad de mercado que se va a abrir con el desarrollo de la eólica flotante y la eólica fija a profundidades cada vez mayores, se une la ausencia de desarrolladores con experiencia reconocida, lo que proporciona una ventana de oportunidad que no se puede dejar pasar, teniendo en cuenta la experiencia en el desarrollo de tecnología para eólica offshore que hay en el País Vasco.',
      ),
      'The following participants are involved in this project:' => 
      array (
        0 => 'En este proyecto convergemos diversos agentes:',
      ),
      'COORDINATOR' => 
      array (
        0 => 'Coordinador',
      ),
      'Technology centres' => 
      array (
        0 => 'Centros tecnológicos',
      ),
      'Expert Technical Advisors' => 
      array (
        0 => 'Asesores Técnicos Expertos',
      ),
      'Clusters' => 
      array (
        0 => '',
      ),
      'ACTIVITIES<br>AND NEWS' => 
      array (
        0 => 'ACTIVIDADES Y NOTICIAS',
      ),
      'Keep up to date with all the latest activities and results from the project and developments in the sector.' => 
      array (
        0 => 'Mantente al día con las últimas actividades y resultados del proyecto y las novedades del sector.',
      ),
      'ALL THE NEWS' => 
      array (
        0 => 'TODAS LAS NOTICIAS',
      ),
      'CONTACT WIND2GRID' => 
      array (
        0 => 'PONTE EN CONTACTO CON WIND2GRID',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => 'Escríbenos si quieres más información, colaborar o darnos tu opinión.',
      ),
      'Basque Energy Cluster' => 
      array (
        0 => 'Cluster Vasco de Energía',
      ),
      'The Basque Maritime Forum' => 
      array (
        0 => 'Foro Marítimo Vasco',
      ),
      'As the offshore wind power sector evolves, it faces a series of challenges relating to structural design, the creation of new products, the optimisation of existing products and the use of operating information to help reduce costs, opening up a great potential market undergoing immense development. This applies to turbines and power evacuation infrastructures, <strong>one of the pivotal elements being the substation</strong>.' => 
      array (
        0 => 'La evolución del sector eólico offshore hacia tecnología flotante conlleva retos de diseño estructural, ideación de nuevos productos, optimización de los existentes y utilización de información de operación a fin de contribuir a la tendencia de reducción de costes que permita aprovechar un gran mercado potencial en pleno desarrollo. Esto es aplicable a los aerogeneradores y a las infraestructuras de evacuación de la energía generada, <strong>uno de cuyos elementos más importantes es la subestación</strong>.',
      ),
      'SIX KEY TRENDS' => 
      array (
        0 => 'SEIS TENDENCIAS CLAVE',
      ),
      'Growing increase of turbine power and bigger wind farms at greater depths.' => 
      array (
        0 => 'Incremento creciente de la potencia de las turbinas, parques eólicos más grandes y a mayores profundidades.',
      ),
      'Developing floating solutions for substations at great depths. Flexible, reliable and modular, they can be adapted to a range of powers and demands.' => 
      array (
        0 => 'Desarrollo de soluciones flotantes para subestaciones en grandes profundidades: flexibles, fiables y modulares para adaptarse a diferentes potencias y requerimientos.',
      ),
      'Innovative materials and antidegradation systems.' => 
      array (
        0 => 'Materiales innovadores y sistemas anti-degradación.',
      ),
      'Digitalised substations for optimised operation.' => 
      array (
        0 => 'Subestaciones digitalizadas de cara a optimizar el funcionamiento.',
      ),
      'Improved O&M activities for offshore substations.' => 
      array (
        0 => 'Facilitación de actividades de O&M de subestaciones offshore.',
      ),
      'Costs reduction: CAPEX and OPEX.' => 
      array (
        0 => 'Reducción de costes: CAPEX y OPEX.',
      ),
      'The <strong>WIND2GRID</strong> consortium will develop new solutions to address applied research to these 6 trends. There is no commercial floating substation on the market today.' => 
      array (
        0 => 'El consorcio <strong>WIND2GRID</strong> va a desarrollar nuevas soluciones para abordar la investigación aplicada a estas 6 tendencias. No existe hoy en día ninguna subestación flotante comercial en el mercado.',
      ),
      'New market opportunity with the development of floating and fixed wind farms at ever greater depths + shortage of developers with recognised experience = window of opportunities for businesses in the wind and maritime sectors.' => 
      array (
        0 => 'Nueva oportunidad de mercado con el desarrollo de la eólica flotante y la eólica fija a profundidades cada vez mayores + Ausencia de desarrolladores con experiencia reconocida = Ventana de oportunidad para empresas del sector eólico y marítimo.',
      ),
      'The Basque wind and maritime sectors encompass more than 200 businesses with years of experience in developing offshore wind technology, all of whom may benefit from this new energy sector trend.' => 
      array (
        0 => 'Los sectores eólico y marítimo vascos acogen a más de 200 empresas, con años de experiencia en el desarrollo de tecnología eólica offshore, pueden beneficiarse de esta nueva tendencia del sector energético.',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => 'CONOCE EL PROYECTO',
      ),
      'See project phases and the role of each participant.' => 
      array (
        0 => 'Descubre las fases del proyecto y el roll de cada participante.',
      ),
      '<strong>WIND2GRID</strong> aims to promote collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating offshore substation.' => 
      array (
        0 => 'El proyecto <strong>WIND2GRID</strong> pretende impulsar la investigación y el desarrollo colaborativo e integrador de tecnologías, componentes y sistemas para subestaciones offshore de nuevas prestaciones, alrededor de un desarrollo de subestación flotante.',
      ),
      'Main aims' => 
      array (
        0 => 'Objetivos principales',
      ),
      'To develop new floating structure concepts valid for multiple locations, including shallow (60-100 m) and deep (around 1000 m) waters.' => 
      array (
        0 => 'Desarrollo de nuevos conceptos de estructuras flotantes válidas para diversas ubicaciones incluyendo tanto aguas someras (60 - 100 m) como aguas profundas (en torno a los 1000 m).',
      ),
      'Advanced numerical modelling applied to research into the hydrodynamic behaviour of complex structures and dynamic umbilical cables.' => 
      array (
        0 => 'Modelos numéricos avanzados aplicados a la investigación del comportamiento hidrodinámico de estructuras complejas y cables umbilicales dinámicos.',
      ),
      'Research into the dynamics of the mooring systems of large structures at varying depths.' => 
      array (
        0 => 'Investigación de dinámica de sistemas de fondeo de grandes estructuras, para diversas profundidades.',
      ),
      'Adaptability to future networks, research into novel power transmission systems (HVAC, HVDC).' => 
      array (
        0 => 'Adaptabilidad a las redes del futuro, investigación de sistemas novedosos de transmisión eléctrica (HVAC, HVDC).',
      ),
      'Research into specific high-performance concrete for marine structures.' => 
      array (
        0 => 'Investigación de hormigones específicos de altas prestaciones para estructuras marinas.',
      ),
      'Research into hybrid coatings for rust-proofing metal components.' => 
      array (
        0 => 'Investigación de revestimientos híbridos aplicables a componentes metálicos para evitar la corrosión.',
      ),
      'Implantation of Lean Manufacturing concepts and BIM methodologies for large marine structures.' => 
      array (
        0 => 'Implantación de conceptos “Lean Manufacturing” o adopción de metodologías “BIM” para grandes estructuras marinas.',
      ),
      'Innovative submarine cable repair systems to reduce O&M costs.' => 
      array (
        0 => 'Sistemas innovadores para la reparación de cables submarinos para reducir costes de O&M.',
      ),
      'Augmented reality and virtual reality concepts to optimise O&M (developing a virtual model of the substation).' => 
      array (
        0 => 'Adaptación de conceptos de realidad aumentada y realidad virtual para la optimización de O&M (Desarrollo de un modelo virtual de la subestación).',
      ),
      'Work packages' => 
      array (
        0 => 'Paquetes de trabajo',
      ),
      '<h3 class=\'introDestacado\'><span class=\'workBlue\'>WP0.</span> Project coordination</h3>' => 
      array (
        0 => '<h3 class=\'introDestacado\'><span class=\'workBlue\'>PT0.</span> Coordinación del proyecto</h3>',
      ),
      '<span class=\'orange\'><strong>WP1.</strong></span> Design conditions and requirement specification' => 
      array (
        0 => '<span class=\'orange\'><strong>PT1.</strong></span> Condiciones de diseño y especificación de requisitos',
      ),
      '<span class=\'orange\'><strong>WP4.</strong></span> Advanced manufacturing and integrated logistics' => 
      array (
        0 => '<span class=\'orange\'><strong>PT4.</strong></span> Fabricación avanzada y logística integrada',
      ),
      '<span class=\'orange\'><strong>WP2.</strong></span> Conceptual development of floating substation' => 
      array (
        0 => '<span class=\'orange\'><strong>PT2.</strong></span> Desarrollo conceptual de subestación flotante',
      ),
      '<span class=\'orange\'><strong>WP5.</strong></span> Innovations in O&M for offshore substations' => 
      array (
        0 => '<span class=\'orange\'><strong>PT5.</strong></span> Innovaciones en O&M para subestaciones offshore',
      ),
      '<span class=\'orange\'><strong>WP3.</strong></span> New materials and antidegradation systems' => 
      array (
        0 => '',
      ),
      '<span class=\'orange\'><strong>WP6.</strong></span> Dissemination and exploitation of results' => 
      array (
        0 => '<span class=\'orange\'><strong>PT6.</strong></span> Difusión y explotación de los resultados',
      ),
      'WP0. Project coordination' => 
      array (
        0 => 'PT0. Coordinación del proyecto',
      ),
      'The purpose of the work package is to ensure the achievement of the technical objectives set and to monitor the progress of the project.' => 
      array (
        0 => 'El objetivo del paquete de trabajo es garantizar la consecución de los objetivos técnicos planteados y realizar el seguimiento del avance del proyecto.',
      ),
      'The technical progress of the project will be monitored to ensure the achievement of the objectives and the economic monitoring of the project\'s development to avoid significant deviations in the budget.' => 
      array (
        0 => 'Se realizará un seguimiento del avance técnico del proyecto para garantizar la consecución de los objetivos y un seguimiento económico del desarrollo del proyecto para evitar desviaciones significativas en el presupuesto.',
      ),
      'WP1. Design conditions and requirement specification' => 
      array (
        0 => 'PT1. Condiciones de diseño y especificación de requisitos',
      ),
      'WP LEADER' => 
      array (
        0 => 'LIDER PT',
      ),
      'Participants' => 
      array (
        0 => 'Participantes',
      ),
      'The purpose of the first work package is to define the fundamentals of design to be implemented in all work packages.' => 
      array (
        0 => 'El objetivo del paquete de trabajo inicial del proyecto es sentar las bases del diseño que se desarrollará a lo largo del resto de paquetes de trabajo.',
      ),
      'The fundamentals of design to be defined will enable: (i) addressing the development of different subcomponents; (ii) validating the design; and (iii) implementing it in case studies.' => 
      array (
        0 => 'Se trata de definir las bases del diseño que permitan: (i) abordar los desarrollos de diferentes subcomponentes, (ii) la validación del diseño y (iii) su aplicación a una serie de casos de estudio.',
      ),
      'WP2. Conceptual development of floating substation' => 
      array (
        0 => 'PT2. Desarrollo conceptual de subestación flotante',
      ),
      'The purpose of this work package is to develop an innovative solution for a floating substation.' => 
      array (
        0 => 'El objetivo de este paquete de trabajo es el desarrollo de una solución innovadora de subestación flotante.',
      ),
      'Based on the design conditions identified in WP1, a comprehensive design will be undertaken, including the floating structure, the topside and the mooring system.' => 
      array (
        0 => 'Se partirá de condiciones de diseño identificadas en el PT1 y se abordará un diseño integral, incluyendo no sólo la estructura flotante sino también el propio topside y el sistema de fondeo.',
      ),
      'The dynamics of interarray and export cables will be analysed to define their design conditions. The development of key ancillary elements, including access and evacuation systems, will be addressed.' => 
      array (
        0 => 'Se analizará la dinámica de los cables interarray y export para definir sus condicionantes de diseño. También se abordará el desarrollo de los principales elementos auxiliares, como sistemas de acceso y evacuación.',
      ),
      'WP3. New materials and antidegradation systems' => 
      array (
        0 => 'PT3. Nuevos materiales y sistemas anti-degradación',
      ),
      'The purpose of this work package is to analyse new materials for use in offshore structures.' => 
      array (
        0 => 'El objetivo de este paquete de trabajo es analizar nuevos materiales para su aplicación a estructuras offshore.',
      ),
      'It will focus on two types of materials: coatings for the protection against offshore erosion, and new concretes for large offshore structures.' => 
      array (
        0 => 'Se pondrá el foco en dos tipos de materiales: recubrimientos para protección contra la degradación offshore, y nuevos hormigones para su aplicación en grandes estructuras offshore.',
      ),
      'In the case of coatings, the goal is the development and validation of a new coating.' => 
      array (
        0 => 'En el caso de los recubrimientos, el objetivo es desarrollar y validar un nuevo recubrimiento.',
      ),
      'In parallel, work will be carried out on the development of ultra-high performance concrete in order to increase the resistance of large structures while reducing weight.' => 
      array (
        0 => 'En paralelo, se trabajará en el desarrollo de hormigones de ultra altas prestaciones, con el objetivo de aumentar la resistencia de grandes estructuras a la vez que se reduce el peso.',
      ),
      'In both cases, the ultimate goal is cost-reduction of offshore structures.' => 
      array (
        0 => 'En ambos casos, el objetivo final es la reducción de costes de estructuras offshore.',
      ),
      'WP4. Advanced manufacturing and integrated logistics' => 
      array (
        0 => 'PT4. Fabricación avanzada y logística integrada',
      ),
      'The purpose of this work package is to demonstrate the technical and financial viability of floating substation operations to transport topside.' => 
      array (
        0 => 'El objetivo del PT4 es precisamente demostrar la viabilidad técnica y económica de las operaciones de las subestaciones flotantes para transportar el topside.',
      ),
      'It seeks to define the means and procedures needed to manufacture, transport and install a floating substation in each of the phases of development of a construction project.' => 
      array (
        0 => 'Se pretende definir, para cada fase de desarrollo de un proyecto constructivo, los medios y procedimientos necesarios para fabricar, transportar e instalar una subestación flotante.',
      ),
      'The manufacture, installation and operation of the topside, the floating structure, the mooring, and the cables will condition the design. Such conditions must be identified in the early design stages so that the elements facilitating the operations are included from the beginning.' => 
      array (
        0 => 'La fabricación, instalación y operación del topside, la estructura flotante, los fondeos y los cables impondrán condicionantes al diseño. Se debe identificar esos condicionantes desde las primeras fases del diseño, de manera que éste incorpore ya elementos que faciliten esas operaciones.',
      ),
      'WP5. Innovations in O&M for offshore substations' => 
      array (
        0 => 'PT5. Innovaciones en O&M para subestaciones offshore',
      ),
      'The final purpose of this work package is cost-reduction. In this case, it will be achieved through the improvement of different activities related to the operation and maintenance phase:' => 
      array (
        0 => 'El objetivo final de este PT es la reducción de costes, en este caso a través de la mejora en diferentes actividades asociadas a la fase de operación y mantenimiento:',
      ),
      'Development and validation of an undersea tool for repairing offshore cables, which would imply a cost reduction in state-of-the-art technology. The goal is to develop a concept tool and a set of manoeuvres that will help reduce the work time.' => 
      array (
        0 => 'Desarrollo y validación de un manipulador submarino aplicado a la reparación de cables offshore que suponga una reducción de costes respecto al estado del arte. El objetivo es desarrollar un concepto de manipulador y unas maniobras que permitan reducir los tiempos de trabajo.',
      ),
      'Reduction of installation time lines and provision of greater traceability of the devices installed by means of the development of a real-time management logic platform.' => 
      array (
        0 => 'Reducir los tiempos de instalación y proporcionar una mayor trazabilidad de los equipos instalados mediante el desarrollo de una plataforma lógica de gestión en tiempo real.',
      ),
      'Production of a virtual model of a floating substation allowing systems and subsystems to be visualised, so it can be used as tool for maintenance optimisation.' => 
      array (
        0 => 'Obtener una representación virtual de una subestación flotante que permita la visualización de sistemas y subsistemas, y que sirva como herramienta de optimización del mantenimiento.',
      ),
      'WP6. Dissemination and exploitation of results' => 
      array (
        0 => 'PT6. Difusión y explotación de resultados',
      ),
      'This will contribute to place the Consortium members as well as Basque companies at the technological and industrial forefront of the power sector, both in the Basque Country and internationally.' => 
      array (
        0 => 'Se contribuirá al posicionamiento de las empresas del consorcio y del País Vasco como referentes tecnológicos e industriales en el segmento de la energía tanto en el ámbito del País Vasco como en el marco internacional.',
      ),
      'A plan to exploit the project results will be defined and launched. This plan will focus on technological development, as well as facilitating training for project partners and Basque supply chain participants in the offshore wind power sector.' => 
      array (
        0 => 'Se definirá y pondrá en marcha un plan de explotación de los resultados del proyecto, tanto en lo referente al desarrollo de tecnología, como en lo relativo a la capacitación de los socios del proyecto y de la cadena de suministro de Euskadi en el sector de la eólica offshore.',
      ),
      'FURTHER INFORMATION ABOUT THE CONSORTIUM' => 
      array (
        0 => 'AMPLÍA INFORMACIÓN SOBRE EL CONSORCIO',
      ),
      'Information related to the companies participating in the consortium and their role' => 
      array (
        0 => 'Información sobre las empresas que conforman el consorcio y la función que desempeñan',
      ),
      'COLLABORATION BETWEEN 11 BENCHMARK BASQUE COMPANIES IN THE WIND AND MARINE SECTORS' => 
      array (
        0 => 'COLABORACIÓN ENTRE 11 EMPRESAS VASCAS DE REFERENCIA EN EL SECTOR EÓLICO Y MARÍTIMO',
      ),
      'Eleven companies and two technology centres have come together in the WIND2GRID project to design and develop a new floating substation concept. The project is expected to help improve the position of the Basque industry in the offshore wind sector and allow the Basque maritime sector occupy the best market position for its future development.' => 
      array (
        0 => '11 empresas y 2 centros tecnológicos aúnan fuerzas en el proyecto WIND2GRID para diseñar y desarrollar un nuevo concepto de subestación flotante. Se espera que el proyecto contribuya a mejorar el posicionamiento de la industria vasca en el sector eólico offshore y que el sector marítimo vasco se posicione de la mejor manera posible en este mercado clave para su futuro.',
      ),
      'Leading wind sector engineering firm and project coordinator' => 
      array (
        0 => 'Ingeniería líder del sector y coordinador del proyecto',
      ),
      'Manufacturer of handling tools and systems (grapples, tongs, etc.)' => 
      array (
        0 => 'Fabricante de herramientas y sistemas de manipulación (pulpos o pinzas)',
      ),
      'Specialists in fastening solutions' => 
      array (
        0 => 'Especialista en soluciones de fijación',
      ),
      'Specialist in surface treatments and new materials' => 
      array (
        0 => 'Empresa dedicada al tratamiento de superficies y desarrollo de nuevos materiales',
      ),
      'Leading international offshore wind operator' => 
      array (
        0 => 'Operador de eólica offshore líder a nivel mundial',
      ),
      'Manufacturer of O&M ships for offshore wind farms' => 
      array (
        0 => 'Fabricante de buques para la O&M de parques eólicos offshore',
      ),
      'Developer of floating platforms' => 
      array (
        0 => 'Desarrollador de plataforma flotante',
      ),
      'Manufacturer of transition parts' => 
      array (
        0 => 'Fabricante de piezas de transición',
      ),
      'Manufacturers of high-quality concretes' => 
      array (
        0 => 'Fabricante de hormigones de alta calidad',
      ),
      'Leader in electrical facilities for naval and offshore sectors, power, industry, etc. Key O&M supplier' => 
      array (
        0 => 'Líder en instalaciones eléctricas sectores naval y offshore, energía, industria... Referente en O&M.',
      ),
      'Sector leader in construction and monitoring of concrete structures' => 
      array (
        0 => 'Empresa líder del sector de la construcción y la monitorización de estructuras de hormigón',
      ),
      'Technology<br>centers' => 
      array (
        0 => 'Centros<br>Tecnológicos',
      ),
      'SEE ACTIVITIES' => 
      array (
        0 => 'DESCUBRE LAS ACTIVIDADES',
      ),
      'See activities and developments in projects' => 
      array (
        0 => 'Descubre las actividades y la evolución de los proyectos',
      ),
      'Keep up to date with our latest activities.' => 
      array (
        0 => 'Mantente al día con nuestras últimas actividades',
      ),
      'SEE LATEST NEWS' => 
      array (
        0 => 'CONOCE LAS ÚLTIMAS NOTICIAS',
      ),
      'See all the sector news' => 
      array (
        0 => 'Conoce todas las noticias del sector',
      ),
      'ALL LATEST NEWS' => 
      array (
        0 => ' NOTICIAS DEL SECTOR',
      ),
      'ACTIVITIES AND<br />OUTCOMES' => 
      array (
        0 => 'ACTIVIDADES Y<br />RESULTADOS',
      ),
      'All' => 
      array (
        0 => 'Todos',
      ),
      'Scroll Up' => 
      array (
        0 => 'Subir',
      ),
      'Activities' => 
      array (
        0 => 'Actividades',
      ),
      'PREVIOUS' => 
      array (
        0 => 'ANTERIOR',
      ),
      'NEXT' => 
      array (
        0 => 'SIGUIENTE',
      ),
      'SECTOR NEWS' => 
      array (
        0 => 'NOTICIAS DEL SECTOR',
      ),
      'Energy Cluster Association' => 
      array (
        0 => 'Asociación del Cluster de Energía',
      ),
      'Project funded by the Department of Economic Development and Infrastructure of the Basque Government (HAZITEK programme) and the European Regional Development Fund (ERDF)' => 
      array (
        0 => 'Proyecto financiado por el Departamento de Desarrollo Económico e Infraestructuras del Gobierno Vasco (Programa HAZITEK) y el Fondo Europeo de Desarrollo Regional (FEDER)',
      ),
      'Read more' => 
      array (
        0 => 'Leer más',
      ),
      'Back to' => 
      array (
        0 => 'Regresar a',
      ),
      'News' => 
      array (
        0 => 'Noticias',
      ),
      'Download' => 
      array (
        0 => 'Descargar',
      ),
      'Keep up to date with our latest news.' => 
      array (
        0 => 'Mantente al día con nuestras últimas noticias.',
      ),
    ),
  ),
);