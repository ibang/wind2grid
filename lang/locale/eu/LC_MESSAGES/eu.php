<?php return array (
  'domain' => NULL,
  'plural-forms' => 'nplurals=2; plural=(n != 1);',
  'messages' => 
  array (
    '' => 
    array (
      '' => 
      array (
        0 => 'Project-Id-Version: Daekin
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-05-20 14:18+0200
PO-Revision-Date: 2020-05-20 14:22+0200
Language: eu_ES
X-Generator: Poedit 2.3.1
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: cms/php
X-Poedit-SearchPath-2: assets/js
X-Poedit-SearchPathExcluded-0: .git
X-Poedit-SearchPathExcluded-1: alerts
X-Poedit-SearchPathExcluded-2: lang
X-Poedit-SearchPathExcluded-3: uploads
X-Poedit-SearchPathExcluded-4: assets/css
X-Poedit-SearchPathExcluded-5: assets/fonts
X-Poedit-SearchPathExcluded-6: assets/ico
X-Poedit-SearchPathExcluded-7: assets/img
X-Poedit-SearchPathExcluded-8: assets/less
X-Poedit-SearchPathExcluded-9: cms/alerts
X-Poedit-SearchPathExcluded-10: cms/assets
X-Poedit-SearchPathExcluded-11: cms/lang
X-Poedit-SearchPathExcluded-12: cms/logs
X-Poedit-SearchPathExcluded-13: cms/site
X-Poedit-SearchPathExcluded-14: cms/uploads
X-Poedit-SearchPathExcluded-15: backup
X-Poedit-SearchPathExcluded-16: assets/js/tools/sanitizer.js
',
      ),
      'THE CHALLENGE' => 
      array (
        0 => 'ERRONKA',
      ),
      'THE PROJECT' => 
      array (
        0 => 'PROIEKTUA',
      ),
      'CONSORTIUM' => 
      array (
        0 => 'PARTZUERGOA',
      ),
      'ACTIVITIES' => 
      array (
        0 => 'JARDUERAK',
      ),
      'NEWS' => 
      array (
        0 => 'BERRIAK',
      ),
      'CONTACT' => 
      array (
        0 => 'KONTAKTUA',
      ),
      'Downloads' => 
      array (
        0 => 'Deskargak',
      ),
      'Contact' => 
      array (
        0 => 'Kontaktua',
      ),
      'AN INNOVATIVE CONCEPT OF <span class=\'orange\'>INTEGRATED FLOATING SUBSTATION</span> DEVELOPED IN BASQUE COUNTRY' => 
      array (
        0 => 'EUSKADIN GARATUTAKO <span class=\'orange\'>AZPIESTAZIO FLOTATZAILEAREN</span> KONTZEPTU BERRITZAILEA',
      ),
      'Collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating substation.' => 
      array (
        0 => 'Prestazio berriak dituzten offshore azpiestazioetarako teknologien, osagaien eta sistemen lankidetza bidezko ikerketa eta garapen integratzailea, azpiestazio flotatzailearen garapenaren inguruan.',
      ),
      'FLOATING OFFSHORE SUBSTATIONS, AN EXCELLENT OPPORTUNITY' => 
      array (
        0 => 'AZPIESTAZIO FLOTAGARRIAK, AUKEREN NITXOA',
      ),
      'At present, there are no commercial floating substations in operation. Accordingly, the market opportunity that will be created by developing floating and fixed offshore wind power at increasing depths goes hand-in-hand with a lack of developers with recognised experience in the field. Together, this creates a unique and unmissable window of opportunity, considering the wealth of experience in offshore wind power technology that we have accumulated in the Basque Country.' => 
      array (
        0 => 'Egun ez dago azpiestazio flotagarri komertzialik. Horren harira, eoliko flotagarriaren nahiz gero eta sakonera handiagoetako eoliko finkoaren garapenak irekiko duen merkatu-aukerari, esperientzia aitortua duten garatzaileen eskasia gehitu behar zaio, eta honek inondik inora galdu behar ez den aukera-leihoa zabaltzen du, Euskadik offshore eolikorako teknologiaren garapenean duen esperientzia kontuan izanda.',
      ),
      'The following participants are involved in this project:' => 
      array (
        0 => 'Proiektu honekin bat egin dugu hainbat eragilek:',
      ),
      'COORDINATOR' => 
      array (
        0 => 'Koordinatzailea',
      ),
      'Technology centres' => 
      array (
        0 => 'Zentro teknologikoak',
      ),
      'Expert Technical Advisors' => 
      array (
        0 => 'Aholkulari Tekniko Adituak',
      ),
      'Clusters' => 
      array (
        0 => 'Klusterrak',
      ),
      'ACTIVITIES<br>AND NEWS' => 
      array (
        0 => 'JARDUERAK ETA<br>ALBISTEAK',
      ),
      'Keep up to date with all the latest activities and results from the project and developments in the sector.' => 
      array (
        0 => 'Egunean egon zaitez proiektuaren azken jarduera eta proiektuei buruz eta sektorearen berritasunei buruz.',
      ),
      'ALL THE NEWS' => 
      array (
        0 => 'ALBISTE GUZTIAK',
      ),
      'THE CONSORTIUM' => 
      array (
        0 => 'PARTZUERGOA',
      ),
      'CONTACT WIND2GRID' => 
      array (
        0 => 'HARREMANETAN JARRI WIND2GRID-EKIN',
      ),
      'Contact us for further information or if you wish to collaborate with us or send us your feedback.' => 
      array (
        0 => 'Idatz iezaiguzu informazio gehiago lortzeko, laguntzeko edo zure iritzia emateko.',
      ),
      'Basque Energy Cluster' => 
      array (
        0 => 'Euskal Energia Klusterra',
      ),
      'The Basque Maritime Forum' => 
      array (
        0 => 'Euskal itsas foroa',
      ),
      'As the offshore wind power sector evolves, it faces a series of challenges relating to structural design, the creation of new products, the optimisation of existing products and the use of operating information to help reduce costs, opening up a great potential market undergoing immense development. This applies to turbines and power evacuation infrastructures, <strong>one of the pivotal elements being the substation</strong>.' => 
      array (
        0 => 'Offshore sektore eolikoak teknologia flotagarriaren alde egin duen bilakaerak hainbat erronka ekarri ditu egituren diseinuan, produktu berriak asmatu eta daudenak optimizatzerakoan eta eragiketa arloko informazioa erabiltzean, betiere kostuak murrizteko joeran laguntzeko eta garapen betean sartuta dagoen merkatu potentzial handia aprobetxatu ahal izateko. Hau aerosorgailuei eta ebakuazioko azpiegiturei ere aplika dakieke, eta <strong>azpiestazioa da horien elementu garrantzitsuenetako bat</strong>.',
      ),
      'SIX KEY TRENDS' => 
      array (
        0 => 'GILTZARRIZKO SEI JOERA',
      ),
      'Growing increase of turbine power and bigger wind farms at greater depths.' => 
      array (
        0 => 'Turbinen potentziaren hazkunde gero eta handiagoa, gero eta parke eoliko handiagoak eta gero eta sakonera handiagoetan.',
      ),
      'Developing floating solutions for substations at great depths. Flexible, reliable and modular, they can be adapted to a range of powers and demands.' => 
      array (
        0 => 'Sakonera handietako azpiestazioetarako soluzio flotagarrien garapena: malguak, fidagarriak eta modularrak, hainbat potentzia era betekizunetara egokitu ahal izateko.',
      ),
      'Innovative materials and antidegradation systems.' => 
      array (
        0 => 'Material berritzaileak eta degradazioaren aurkako sistemak.',
      ),
      'Digitalised substations for optimised operation.' => 
      array (
        0 => 'Azpiestazio digitalizatuak funtzionamendua optimizatzeko.',
      ),
      'Improved O&M activities for offshore substations.' => 
      array (
        0 => 'Offshore azpiestazioetako O&M jarduerak erraztea.',
      ),
      'Costs reduction: CAPEX and OPEX.' => 
      array (
        0 => 'Kostuen murrizketa: CAPEX eta OPEX.',
      ),
      'The <strong>WIND2GRID</strong> consortium will develop new solutions to address applied research to these 6 trends. There is no commercial floating substation on the market today.' => 
      array (
        0 => '<strong>WIND2GRID</strong> partzuergoak soluzio berriak garatuko ditu 6 joera horiei aplikatutako ikerketari ekiteko. Gaur egun ez dago merkataritzako azpiestazio flotatzailerik merkatuan.',
      ),
      'New market opportunity with the development of floating and fixed wind farms at ever greater depths + shortage of developers with recognised experience = window of opportunities for businesses in the wind and maritime sectors.' => 
      array (
        0 => 'Merkatu-aukera berria eoliko flotagarriaren eta gero eta sakonera handiagoetako eoliko finkoaren garapenari esker + Esperientzia aitortua duen garatzailerik eza = Aukera-leihoa sektore eoliko eta itsas-sektoreko enpresentzat.',
      ),
      'The Basque wind and maritime sectors encompass more than 200 businesses with years of experience in developing offshore wind technology, all of whom may benefit from this new energy sector trend.' => 
      array (
        0 => 'Euskadiko sektore eolikoak eta itsas-sektoreak offshore eolikoaren teknologiaren garapenean urteetako esperientzia duten 200dik gora enpresa biltzen dituzte, eta horiek guztiek onura atera dezakete energia-sektorearen joera berri honetatik.	',
      ),
      'LEARN MORE ABOUT THE PROJECT' => 
      array (
        0 => 'EZAGUTU PROIEKTUA',
      ),
      'See project phases and the role of each participant.' => 
      array (
        0 => 'Ezagutu proiektuaren faseak eta partaide bakoitzaren eginkizuna.',
      ),
      '<strong>WIND2GRID</strong> aims to promote collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating offshore substation.' => 
      array (
        0 => '<strong>WIND2GRID</strong> proiektuak prestazio berriak dituzten offshore azpiestazioetarako teknologien, osagaien eta sistemen lankidetza bidezko ikerketa eta garapen integratzailea bultzatzea du helburu, azpiestazio flotagarriaren garapenaren inguruan.',
      ),
      'Main aims' => 
      array (
        0 => 'Helburu nagusiak',
      ),
      'To develop new floating structure concepts valid for multiple locations, including shallow (60-100 m) and deep (around 1000 m) waters.' => 
      array (
        0 => 'Hainbat kokapenetarako balio duten egitura flotagarriei buruzko kontzeptu berrien garapena, sakonera txikiak (60-100 m.) zein sakonera handiak (1000 m. inguru) barne hartuta.',
      ),
      'Advanced numerical modelling applied to research into the hydrodynamic behaviour of complex structures and dynamic umbilical cables.' => 
      array (
        0 => 'Egitura konplexuen zein zilbor-kable dinamikoen portaera hidrodinamikoaren ikerketari aplikatutako eredu numeriko aurreratuen garapena.',
      ),
      'Research into the dynamics of the mooring systems of large structures at varying depths.' => 
      array (
        0 => 'Egitura handiak sakonera desberdinetako uretan ainguratzeko sistemen dinamikari buruzko ikerketa.',
      ),
      'Adaptability to future networks, research into novel power transmission systems (HVAC, HVDC).' => 
      array (
        0 => 'Etorkizuneko sareetarako moldagarritasuna, elektrizitatea transmititzeko sistema berritzaileen inguruko ikerketa (HVAC, HVDC).',
      ),
      'Research into specific high-performance concrete for marine structures.' => 
      array (
        0 => 'Itsas-egituretarako prestazio handiak dituzten hormigoi espezifikoei buruzko ikerketa.',
      ),
      'Research into hybrid coatings for rust-proofing metal components.' => 
      array (
        0 => 'Metalezko osagaietan aplikatu eta korrosioa saihes dezaketen estaldura hibridoei buruzko ikerketa.',
      ),
      'Implantation of Lean Manufacturing concepts and BIM methodologies for large marine structures.' => 
      array (
        0 => 'Itsas-egitura handietan “Lean Manufacturing” kontzeptuak nahiz “BIM” metodologiak ezartzea.',
      ),
      'Innovative submarine cable repair systems to reduce O&M costs.' => 
      array (
        0 => 'Urpeko kableak konpontzeko sistema berritzaileak, O&M arloko kostuak murrizteko.',
      ),
      'Augmented reality and virtual reality concepts to optimise O&M (developing a virtual model of the substation).' => 
      array (
        0 => 'Errealitate aregotuko zein errealitate birtualeko kontzeptuen egokitzapena O&M optimizatzeko (azpiestazioaren eredu birtualaren garapena).',
      ),
      'Work packages' => 
      array (
        0 => 'Lan-paketeak',
      ),
      '<h3 class=\'introDestacado\'><span class=\'workBlue\'>WP0.</span> Project coordination</h3>' => 
      array (
        0 => '<h3 class=\'introDestacado\'><span class=\'workBlue\'>0.LP.</span> Proiektuaren koordinazioa</h3>',
      ),
      '<span class=\'orange\'><strong>WP1.</strong></span> Design conditions and requirement specification' => 
      array (
        0 => '<span class=\'orange\'><strong>1.LP.</strong></span> Diseinuaren baldintzak eta betekizunen zehaztapena',
      ),
      '<span class=\'orange\'><strong>WP4.</strong></span> Advanced manufacturing and integrated logistics' => 
      array (
        0 => '<span class=\'orange\'><strong>4.LP.</strong></span> Fabrikazio aurreratua eta logistika integratua',
      ),
      '<span class=\'orange\'><strong>WP2.</strong></span> Conceptual development of floating substation' => 
      array (
        0 => '<span class=\'orange\'><strong>2.LP.</strong></span> Azpiestazio flotagarriaren garapen kontzeptuala',
      ),
      '<span class=\'orange\'><strong>WP5.</strong></span> Innovations in O&M for offshore substations' => 
      array (
        0 => '<span class=\'orange\'><strong>5.LP.</strong></span> Offshore azpiestazioetarako O&M arloko berrikuntzak',
      ),
      '<span class=\'orange\'><strong>WP3.</strong></span> New materials and antidegradation systems' => 
      array (
        0 => '<span class=\'orange\'><strong>3.LP.</strong></span> Degradazioaren aurkako material eta sistema berriak',
      ),
      '<span class=\'orange\'><strong>WP6.</strong></span> Dissemination and exploitation of results' => 
      array (
        0 => '<span class=\'orange\'><strong>6.LP.</strong></span> Emaitzen zabalkundea eta ustiapena',
      ),
      'WP0. Project coordination' => 
      array (
        0 => '0.LP. Proiektuaren koordinazioa',
      ),
      'The purpose of the work package is to ensure the achievement of the technical objectives set and to monitor the progress of the project.' => 
      array (
        0 => 'Lan-paketearen helburua da planteatutako helburu teknikoak lortzen direla bermatzea eta proiektuaren aurrerapenaren jarraipena egitea.',
      ),
      'The technical progress of the project will be monitored to ensure the achievement of the objectives and the economic monitoring of the project\'s development to avoid significant deviations in the budget.' => 
      array (
        0 => 'Proiektuaren aurrerapen teknikoaren jarraipena egingo da, helburuak lortzen direla bermatzeko eta proiektuaren garapenaren jarraipen ekonomikoa egiteko, aurrekontuan desbideratze nabarmenik gerta ez dadin.',
      ),
      'WP1. Design conditions and requirement specification' => 
      array (
        0 => '1.LP. Diseinuaren baldintzak eta betekizunen zehaztapena',
      ),
      'WP LEADER' => 
      array (
        0 => 'LP-KO LIDERRA',
      ),
      'Participants' => 
      array (
        0 => 'Parte-hartzaileak',
      ),
      'The purpose of the first work package is to define the fundamentals of design to be implemented in all work packages.' => 
      array (
        0 => 'Proiektuaren hasierako lan-paketearen helburua gainerako lan-paketeetan garatuko den diseinuaren oinarriak finkatzea da.',
      ),
      'The fundamentals of design to be defined will enable: (i) addressing the development of different subcomponents; (ii) validating the design; and (iii) implementing it in case studies.' => 
      array (
        0 => 'Diseinuaren oinarriak finkatuz, zera lortu nahi da: (i) hainbat azpiosagairen garapenari heltzea, (ii) diseinua baliozkotzea eta (iii) zenbait azterketa-kasutan aplikatzea.',
      ),
      'WP2. Conceptual development of floating substation' => 
      array (
        0 => '2.LP. Azpiestazio flotagarriaren garapen kontzeptuala',
      ),
      'The purpose of this work package is to develop an innovative solution for a floating substation.' => 
      array (
        0 => 'Lan-pakete honen helburua azpiestazio flotagarrirako soluzio berritzaile bat garatzea da.',
      ),
      'Based on the design conditions identified in WP1, a comprehensive design will be undertaken, including the floating structure, the topside and the mooring system.' => 
      array (
        0 => '1.LPan zehaztutako diseinu-baldintzetatik abiatuta, diseinu integralari helduko zaio, egitura flotagarria barne hartuz, baita topsidea bera eta ainguratze-sistema ere.',
      ),
      'The dynamics of interarray and export cables will be analysed to define their design conditions. The development of key ancillary elements, including access and evacuation systems, will be addressed.' => 
      array (
        0 => 'Interarray eta export kableen dinamika aztertuko da diseinu arloan dituzten baldintzapenak zehazteko. Horrekin batera, osagai laguntzaile nagusien diseinuari helduko zaio, sarbide eta ebakuaziorako sistemak kasu.',
      ),
      'WP3. New materials and antidegradation systems' => 
      array (
        0 => '3.LP. Degradazioaren aurkako material eta sistema berriak',
      ),
      'The purpose of this work package is to analyse new materials for use in offshore structures.' => 
      array (
        0 => 'Lan-pakete honen helburua offshore egituretan aplika daitezkeen material berriak aztertzea da.',
      ),
      'It will focus on two types of materials: coatings for the protection against offshore erosion, and new concretes for large offshore structures.' => 
      array (
        0 => 'Bi material-motari erreparatuko zaie nagusiki: offshore degradaziotik babesteko estaldurak eta offshore egitura handietan aplikatzeko moduko hormigoi berriak.',
      ),
      'In the case of coatings, the goal is the development and validation of a new coating.' => 
      array (
        0 => 'Estalduren kasuan, helburua estaldura berri bat garatu eta baliozkotzea da.',
      ),
      'In parallel, work will be carried out on the development of ultra-high performance concrete in order to increase the resistance of large structures while reducing weight.' => 
      array (
        0 => 'Aldi berean, prestazio ultra altuak dituzten hormigoien garapenean lan egingo da, egitura handien erresistentzia areagotu eta pisua murrizteko asmoz.',
      ),
      'In both cases, the ultimate goal is cost-reduction of offshore structures.' => 
      array (
        0 => 'Kasu batean zein bestean, azken helburua offshore egituren kostuak murriztea da.',
      ),
      'WP4. Advanced manufacturing and integrated logistics' => 
      array (
        0 => '4.LP. Fabrikazio aurreratua eta logistika integratua',
      ),
      'The purpose of this work package is to demonstrate the technical and financial viability of floating substation operations to transport topside.' => 
      array (
        0 => '4.LParen helburua azpiestazio flotagarrien eragiketaren bideragarritasun tekniko eta ekonomikoa frogatzea da, topsidea garraiatzeko.',
      ),
      'It seeks to define the means and procedures needed to manufacture, transport and install a floating substation in each of the phases of development of a construction project.' => 
      array (
        0 => 'Eraikuntza-proiektu bat garatzeko fase bakoitzean, azpiestazio flotagarria fabrikatu, garraiatu eta instalatzeko behar diren bitartekoak eta prozedurak zehaztu nahi dira.',
      ),
      'The manufacture, installation and operation of the topside, the floating structure, the mooring, and the cables will condition the design. Such conditions must be identified in the early design stages so that the elements facilitating the operations are included from the beginning.' => 
      array (
        0 => 'Topsidea, egitura flotagarria, ainguratzeak eta kableak fabrikatu, instalatu eta eragiketan jartzeak baldintzapenak jarriko dizkio diseinuari. Baldintzapen horiek zehaztu behar dira diseinuaren lehenengo faseetatik, diseinuak eragiketa horiek erraztuko dituzten elementuak izan ditzan.',
      ),
      'WP5. Innovations in O&M for offshore substations' => 
      array (
        0 => '5.LP. Offshore azpiestazioetarako O&M arloko berrikuntzak',
      ),
      'The final purpose of this work package is cost-reduction. In this case, it will be achieved through the improvement of different activities related to the operation and maintenance phase:' => 
      array (
        0 => 'LP honen azken helburua kostuak murriztea da, kasu honetan, eragiketa eta mantentze-lanen faseari lotutako hainbat jardueraren hobekuntzaren bitartez:',
      ),
      'Development and validation of an undersea tool for repairing offshore cables, which would imply a cost reduction in state-of-the-art technology. The goal is to develop a concept tool and a set of manoeuvres that will help reduce the work time.' => 
      array (
        0 => 'Offshore kableen konponketari aplikatutako itsaspeko manipulatzaile bat garatu eta baliozkotzea, egungoak baino kostu murritzagoak lortzeko asmoz. Helburua lan-denborak murrizten lagunduko duten manipulatzailearen kontzeptu bat eta maniobrak garatzea da.',
      ),
      'Reduction of installation time lines and provision of greater traceability of the devices installed by means of the development of a real-time management logic platform.' => 
      array (
        0 => 'Instalazio-denborak murriztu eta instalatutako ekipoen trazagarritasun handiagoa lortzea, denbora errealeko kudeaketarako plataforma logiko baten garapenaren bitartez.',
      ),
      'Production of a virtual model of a floating substation allowing systems and subsystems to be visualised, so it can be used as tool for maintenance optimisation.' => 
      array (
        0 => 'Azpiestazio flotagarriaren irudikapen birtuala lortzea, sistemak eta azpisistemaka bistaratu ahal izateko, mantentze-lanak optimizatzeko tresnatzat erabiliz.',
      ),
      'WP6. Dissemination and exploitation of results' => 
      array (
        0 => '6.LP. Emaitzen zabalkundea eta ustiapena',
      ),
      'This will contribute to place the Consortium members as well as Basque companies at the technological and industrial forefront of the power sector, both in the Basque Country and internationally.' => 
      array (
        0 => 'Partzuergoko eta Euskadiko enpresak energiaren segmentuko erreferente teknologiko eta industrialtzat posizionatzen lagunduko du, Euskadin zein nazioartean.',
      ),
      'A plan to exploit the project results will be defined and launched. This plan will focus on technological development, as well as facilitating training for project partners and Basque supply chain participants in the offshore wind power sector.' => 
      array (
        0 => 'Proiektuaren emaitzak ustiatzeko plana definitu eta abiaraziko da, offshore eolikoaren sektoreko teknologiaren garapenari zein proiektuko eta Euskadiko banaketa-kateko bazkideen gaikuntzari dagokienez.',
      ),
      'FURTHER INFORMATION ABOUT THE CONSORTIUM' => 
      array (
        0 => 'INFORMAZIO GEHIAGO LORTU PARTZUERGOARI BURUZ',
      ),
      'Information related to the companies participating in the consortium and their role' => 
      array (
        0 => 'Partzuergoa osatzen duten enpresen eta dagokien betekizunen gaineko informazioa',
      ),
      'COLLABORATION BETWEEN 11 BENCHMARK BASQUE COMPANIES IN THE WIND AND MARINE SECTORS' => 
      array (
        0 => 'EOLIKOAN NAHIZ ITSAS SEKTOREAN ERREFERENTZIA DIREN 11 EUSKAL ENPRESAREN ARTEKO LANKIDETZA',
      ),
      'Eleven companies and two technology centres have come together in the WIND2GRID project to design and develop a new floating substation concept. The project is expected to help improve the position of the Basque industry in the offshore wind sector and allow the Basque maritime sector occupy the best market position for its future development.' => 
      array (
        0 => '11 enpresak eta 2 zentro teknologikok indarrak bildu dituzte WIND2GRID proiektuaren inguruan, azpiestazio flotagarriaren kontzeptu berri bat diseinatu eta garatzeko. Proiektu honek euskal industriak offshore sektore eolikoan duen posizionamendua hobetzen eta euskal itsas sektoreari etorkizuneko giltzarrizko merkatu horretan ahalik eta ondoen posizionatzen lagunduko diola espero da.',
      ),
      'Leading wind sector engineering firm and project coordinator' => 
      array (
        0 => 'Sektoreko ingeniaritza liderra eta proiektuaren koordinatzailea',
      ),
      'Manufacturer of handling tools and systems (grapples, tongs, etc.)' => 
      array (
        0 => 'Manipulazio-tresnen eta sistemen fabrikatzailea (olagarroak edo pintzak)',
      ),
      'Specialists in fastening solutions' => 
      array (
        0 => 'Finkatze-soluzioetan espezializatua',
      ),
      'Specialist in surface treatments and new materials' => 
      array (
        0 => 'Azaleren tratamenduan zein material berrien garapenean diharduen enpresa',
      ),
      'Leading international offshore wind operator' => 
      array (
        0 => 'Offshore eolikoaren mundu mailako operadorea',
      ),
      'Manufacturer of O&M ships for offshore wind farms' => 
      array (
        0 => 'Offshore parke eolikoen O&Mrako itsasontzien fabrikatzailea',
      ),
      'Developer of floating platforms' => 
      array (
        0 => 'Plataforma flotagarriaren garatzailea',
      ),
      'Manufacturer of transition parts' => 
      array (
        0 => 'Trantsizioko piezen fabrikatzailea',
      ),
      'Manufacturers of high-quality concretes' => 
      array (
        0 => 'Kalitate handiko hormigoien fabrikatzailea',
      ),
      'Leader in electrical facilities for naval and offshore sectors, power, industry, etc. Key O&M supplier' => 
      array (
        0 => 'Ontzigintza, offshore, energia zein industria sektoreetako instalazio elektrikoetako liderra. O&M arloko erreferentea.',
      ),
      'Sector leader in construction and monitoring of concrete structures' => 
      array (
        0 => 'Hormigoizko egituren monitorizazioan trebatutako eraikuntza sektoreko enpresa liderra',
      ),
      'Technology<br>centers' => 
      array (
        0 => 'ZENTRO<br>TEKNOLOGIKOAK',
      ),
      'SEE ACTIVITIES' => 
      array (
        0 => 'EZAGUTU JARDUERAK',
      ),
      'See activities and developments in projects' => 
      array (
        0 => 'Ezagutu jarduerak eta proiektuen bilakaera',
      ),
      'Keep up to date with our latest activities.' => 
      array (
        0 => 'Gure azken jarduerekin jarraitu.',
      ),
      'SEE LATEST NEWS' => 
      array (
        0 => 'IKUSI AZKEN BERRIAK',
      ),
      'See all the sector news' => 
      array (
        0 => 'Ikus itzazu sektore guztietako berriak',
      ),
      'ALL LATEST NEWS' => 
      array (
        0 => 'AZKEN BERRIAK',
      ),
      'All' => 
      array (
        0 => 'Dena',
      ),
      'Scroll Up' => 
      array (
        0 => 'Igo',
      ),
      'Activities' => 
      array (
        0 => 'Jarduerak',
      ),
      'Outcomes' => 
      array (
        0 => 'Emaitzak',
      ),
      'PREVIOUS' => 
      array (
        0 => 'AURREKOA',
      ),
      'NEXT' => 
      array (
        0 => 'HURRENGOA',
      ),
      'DAEKIN launched at Digitalisation Technology Forum' => 
      array (
        0 => 'DAEKINen aurkezpena Digitalizazioaren Foro Teknologikoan',
      ),
      'SECTOR NEWS' => 
      array (
        0 => 'SEKTOREKO BERRIAK',
      ),
      'Energy Cluster Association' => 
      array (
        0 => 'Energia Kluster Elkartea',
      ),
      'Project funded by the Department of Economic Development and Infrastructure of the Basque Government (HAZITEK programme) and the European Regional Development Fund (ERDF)' => 
      array (
        0 => 'Eusko Jaurlaritzako Ekonomiaren Garapen eta Azpiegitura Sailak (HAZITEK programa) eta Eskualde Garapeneko Europako Funtsak (FEDER) finantzatutako proiektua',
      ),
      'Read more' => 
      array (
        0 => 'Irakurri gehiago',
      ),
      'Back to' => 
      array (
        0 => 'Itzuli',
      ),
      'News' => 
      array (
        0 => 'Berrira',
      ),
      'Download' => 
      array (
        0 => 'Deskargatu',
      ),
      'Keep up to date with our latest news.' => 
      array (
        0 => 'Jarrai ezazu gure azken berrien berri.',
      ),
    ),
  ),
);