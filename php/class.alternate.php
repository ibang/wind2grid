<?php
class AlternateURLS
{
	public $language = '';
	public $path_array = array('home');
	public $alternate_urls = array();
	public $slug_subsection = '';

	public function __construct($language='', $path_array = array('home')){
		if (!$language){
			$language = $db->get_default_lang();
		}
		$this->language = $language;
		$this->path_array = $path_array;
		$this->slug_subsection = count($this->path_array)>1 ? end($this->path_array) : '';
	}
	function AlternateURLS($language='', $path_array = array('home')){
		self::__construct($language, $path_array);
	}
	// read the other languages urls
	function getAlternateUrls(){
		global $DOC_ROOT, $URL_ROOT, $_GET, $db;
		$alternate_urls = array();
		$languages_array = $db->get_active_langs_array();
		
		//INICIALIZAMOS
		$subsection_langs_slug = '';
		$contact_type = '';
		$s2 = '';
		$page_slug = '';
		$_GET["type"] = (isset($_GET["type"])?$_GET["type"]:'');
		$_GET["page"] = (isset($_GET["page"])?$_GET["page"]:'');
		if (!empty($_GET["page"])){
			$page_slug = $_GET["page"].'/';
		}
		//
		switch ($this->slug_subsection){ //slug (s2)
			case 'category':
				$category = (isset($_GET["category"])?$_GET["category"]:'');
				$subsection_langs_slug = $db->get_news_category_langs($this->language, $category);
			break;
			case 'new':
				$slug = (isset($_GET["slug"])?$_GET["slug"]:'');
				$subsection_langs_slug = $db->get_news_article_langs($this->language, $slug);
			break;
			case 'project':
				$slug = (isset($_GET["slug"])?$_GET["slug"]:'');
				$subsection_langs_slug = $db->get_projects_sheet_langs($this->language, $slug);
			break;
			case 'contact-type':
				$s2 = $this->path_array[count($this->path_array)-2];
				$contact_type = $s2.'/'.$_GET["type"];
				if (!$_GET["type"]){
					$this->slug_subsection = '';
					$contact_type='';
				}
			break;
			default:
				// other case not use slug 
				$this->slug_subsection = '';
		}
		//
		$doc = new DOMDocument();
		foreach ($languages_array as $language_aux){
			//if ($language_aux!= $this->language){
				if (@$doc->load($DOC_ROOT."lang/".strtolower($language_aux).".xml")){
					$xPath = new DOMXPath($doc);
					$section_aux='';
					$url_aux = '';
					if (!empty($this->path_array)){
						foreach ($this->path_array as $section){
							$section_aux = $section_aux.$section.'/';
							foreach( $xPath->query('/txt/'.$section_aux.'url') as $url ) {
								$url_aux = $url_aux.($url->nodeValue?$url->nodeValue.'/':''); // Will output bar1 and bar2 w\out whitespace
							}
						}
					}
					if ($this->slug_subsection ){
						if (!empty($subsection_langs_slug[$language_aux])){
							$alternate_urls[$language_aux] = $URL_ROOT.strtolower($language_aux).'/'.$url_aux.$subsection_langs_slug[$language_aux].'/'.$page_slug; 
						}
						elseif($contact_type){
							if ($s2!='contact_type'){
								foreach( $xPath->query('/txt/'.$s2.'/url') as $url ) {
									$url_aux = $url_aux.($url->nodeValue?$url->nodeValue.'/':''); // Will output bar1 and bar2 w\out whitespace
								}
							}
							foreach( $xPath->query('/txt/'.$contact_type.'/url') as $url ) {
								$url_aux = $url_aux.($url->nodeValue?$url->nodeValue.'/':''); // Will output bar1 and bar2 w\out whitespace
							}
							$alternate_urls[$language_aux] = $URL_ROOT.strtolower($language_aux).'/'.$url_aux.$page_slug; 
						}
					}
					else{
							$alternate_urls[$language_aux] = $URL_ROOT.strtolower($language_aux).'/'.$url_aux.$page_slug;
					}
				}
			//}
		}
		// lo cruzo con las localizaciones/urls disponibles
		$available_localizations_urls_data = $db->get_active_localizations_url_list_data();
		if (!empty($available_localizations_urls_data)){
			foreach ($available_localizations_urls_data as $currentLangUrl){
				if (empty($alternate_urls[$currentLangUrl['localization_url']])){
					if($currentLangUrl['lang_code']){
						if (!empty($alternate_urls[$currentLangUrl['lang_code']])){
							$alternate_urls[$currentLangUrl['localization_url']] = str_replace('/'.$currentLangUrl['lang_code'].'/', '/'.$currentLangUrl['localization_url'].'/', $alternate_urls[$currentLangUrl['lang_code']]);
						}
					}
				}
			}
		}
		//
		$this->alternate_urls = $alternate_urls;
		return $alternate_urls;
	}
}
// 
?>