<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Challenge - WIND2GRID");
$description=__("WIND2GRID - Developing floating solutions for substations at great depths. ");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="nosotros" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
            <div class="row w-100 no-gutters">
              <div class="col-md-12">
                <figure class="figure mb-sm-0">
                  <img src="<?=$URL_ROOT?>assets/img/challenge/hero.jpg" class="">
                </figure>
                <div class="text-box pr-2">
                  <h1 class="pl-1 pl-lg-4"><?=__("THE CHALLENGE");?></h1>
                  <p class="scroll data"></p>
                </div>
              </div>
            </div>
          </article>
      </div>
    </div>
    <div class="full-container">
      <div class="container">

        <article class="mt-2 pl-1 pr-1 pl-md-3 pr-md-1">
          <div class="row bloques show-on-scroll">
            <div class="col-md-6 mt-1 ml-md-2 mt-md-3">
                <p class="intro darkBlue"><?=__("As the offshore wind power sector evolves, it faces a series of challenges relating to structural design, the creation of new products, the optimisation of existing products and the use of operating information to help reduce costs, opening up a great potential market undergoing immense development. This applies to turbines and power evacuation infrastructures, <strong>one of the pivotal elements being the substation</strong>.");?></p>
            </div>
          </div>
        </article>

        <article id="challenge" class="mt-4 pl-1 pr-1 pl-md-3 pr-md-3 bloques show-on-scroll justify-content-center">
          <div class="row justify-content-center">
            <h2 class="col-md-9"><?=__("SIX KEY TRENDS");?></h2>
            <ul class="row list-unstyled justify-content-center">
              <li class="col-md-3 small text-left p-2 mr-2 icono-01">
                <p><?=__("Growing increase of turbine power and bigger wind farms at greater depths.");?></p>
              </li>
              <li class="col-md-3 text-left icono-02">
                <p ><?=__("Developing floating solutions for substations at great depths. Flexible, reliable and modular, they can be adapted to a range of powers and demands.");?></p>
              </li>
              <li class="col-md-3 text-left icono-03">
                <p><?=__("Innovative materials and antidegradation systems.");?></p>
              </li>
              <li class="col-md-3 text-left icono-04">
                <p><?=__("Digitalised substations for optimised operation.");?></p>
              </li>
              <li class="col-md-3 text-left icono-05">
                <p><?=__("Improved O&M activities for offshore substations.");?></p>
              </li>
              <li class="col-md-3 text-left icono-06">
                <p><?=__("Costs reduction: CAPEX and OPEX.");?></p>
              </li>
            </ul>
          </div>
        </article>

      </div>
    </div>

    <div class="full-container bg-gray mt-4 bloques show-on-scroll">
      <div class="container">
        <article class="row justify-content-center p-2 p-md-3">
          <div class="col-md-5">
            <img src="<?=$URL_ROOT?>assets/img/challenge/foto-consorcio-wind2grid.jpg">
          </div>
          <div class="col-md-6 align-self-end">
            <p class="intro darkBlue pt-2 pt-md-0"><?=__("The <strong>WIND2GRID</strong> consortium will develop new solutions to address applied research to these 6 trends. There is no commercial floating substation on the market today.");?></p>
            <p><?=__("New market opportunity with the development of floating and fixed wind farms at ever greater depths + shortage of developers with recognised experience = window of opportunities for businesses in the wind and maritime sectors.");?></p>
            <p><?=__("The Basque wind and maritime sectors encompass more than 200 businesses with years of experience in developing offshore wind technology, all of whom may benefit from this new energy sector trend.");?></p>
          </div>
        </article>
      </div>
    </div>


      <div class="container">
      <article class="banner mt-5 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/challenge/banner.jpg);background-size: cover; background-position: center bottom;">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("LEARN MORE ABOUT THE PROJECT");?></h3>
          <p class="text-white"><?=__("See project phases and the role of each participant.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=__("THE PROJECT");?></a>
      </article>
    </div>
  </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>