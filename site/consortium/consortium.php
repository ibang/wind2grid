<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Consortium - WIND2GRID");
$description=__("Basque engineering, manufacturing, development and research centers collaborate to develop this project");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="context" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/consortium/hero.jpg" class="">
                 </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4 pt-md-4"><?=__("CONSORTIUM");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>


    <div class="full-container">
      <div class="container pt-1 pt-md-3 pd-sm-3 pb-md-0">
        <article class="pl-1 pr-1 pl-md-4 pr-md-3">
          <div class="row bloques show-on-scroll">
            <div class="col-lg-8">
              <h2 class=""><?=__("COLLABORATION BETWEEN 11 BENCHMARK BASQUE COMPANIES IN THE WIND AND MARINE SECTORS");?></h2>
              <div class="mb-2 mb-sm-0 blue">
                <p class="intro darkBlue"><?=__("Eleven companies and two technology centres have come together in the WIND2GRID project to design and develop a new floating substation concept. The project is expected to help improve the position of the Basque industry in the offshore wind sector and allow the Basque maritime sector occupy the best market position for its future development.");?></p>
              </div>
           </div>
          </div>
       </article>
      </div>
    </div>

    <div class="full-container bg-gray bloques show-on-scroll">
      <div class="container mt-3">
          <div class="row bg-gray pl-md-1 pr-md-1 mb-3">
              <div class="col-md-3 bg-corporate1 shadow logo-box text-center"><img src="<?=$URL_ROOT?>assets/img/all/logo-wind2grid-blanco.png" class="mt-4 pt-2" width="150" /></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank" title="Idom" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" class="mt-4 pb-1" width="150" /><br /><strong><?=__("Leading wind sector engineering firm and project coordinator");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="http://blug.es/" target="_blank" title="Credeblug" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-blug.png" class="mt-4 pb-1" width="150" /><br /><strong><?=__("Manufacturer of handling tools and systems (grapples, tongs, etc.)");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="http://www.errekafasteningsolutions.com/en/" target="_blank" title="Erreka" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-erreka.png" class="mt-4 pt-1 pb-1" width="150" /><br /><strong><?=__("Specialists in fastening solutions");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="http://www.galvasala.com/" target="_blank" title="Galvasala" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-galvasala.png" class="mt-4 pb-1" width="150" /><br /><strong><?=__("Specialist in surface treatments and new materials");?></strong></a></div>
              <!--
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="https://www.iberdrola.es/" target="_blank" title="Iberdrola" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-iberdrola.png" class="mt-3 pb-1" width="150" /><br /><strong><'?=__("Leading international offshore wind operator");?></strong></a></div>
              -->
              <div class="col-md-3 shadow logo-box text-center"><a href="http://www.astillerosmurueta.com/" target="_blank" title="Astilleros Murueta" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-murueta.png" class="mt-4 pt-1 pb-1" width="150" /><br /><strong><?=__("Manufacturer of O&M ships for offshore wind farms");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="http://www.nautilusfs.com/" target="_blank" title="Nautilus" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-nautilus.png" class="mt-4 pt-1 pb-1" width="150" /><br /><strong><?=__("Developer of floating platforms");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center"><a href="https://www.navacel.com/" target="_blank" title="Navacel" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-navacel.png" class="mt-4 pb-1" width="150" /><br /><strong><?=__("Manufacturer of transition parts");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center mbn-3"><a href="https://www.ogerco.com/" target="_blank" title="Ogerco" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-ogerco.png" class="mt-4 pb-1" width="180" /><br /><strong><?=__("Manufacturers of high-quality concretes");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center mbn-3"><a href="https://pine.zimacorp.es/" target="_blank" title="Pine" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-pine.png" class="mt-3 pt-1 pb-1" width="150" /><br /><strong><?=__("Leader in electrical facilities for naval and offshore sectors, power, industry, etc. Key O&M supplier");?></strong></a></div>
              <div class="col-md-3 shadow logo-box text-center align-self-center mbn-3"><a href="https://viudadesainz.com/" target="_blank" title="Viuda de Sainz" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-viuda-de-sainz.png" class="mt-4 pb-1" width="150" /><br /><strong><?=__("Sector leader in construction and monitoring of concrete structures");?></strong></a></div>
              <div class="col-md-3"></div>
          </div>
      </div>
    </div>

    <div class="container pt-3 pb-3 pb-md-3">
        <article class="pl-1 pr-1 pl-md-4 pr-md-3 mt-md-3 bloques show-on-scroll">
          <div class="row">
              <h2 class="col-md-4"><?=__("Technology<br>centers");?></h2>
              <p class="col-md-4 grow"><a href="https://www.tecnalia.com/" target="_blank" title="Tecnalia" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" width="250" alt="Tecnalia" /></a></p>
              <p class="col-md-4 grow"><a href="https://www.bcmaterials.net/" target="_blank" title="Tecnalia" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-bcmaterials.png" width="250" alt="BC Materials" /></a></p>
          </div>
          <div class="row mt-3 pt-3 border-top">
              <h2 class="col-md-4"><?=__("Expert Technical Advisors");?></h2>
              <a class="col-md-4 grow" href="https://www.iberdrola.es/" title="Iberdrola" target="_blank" rel="nofollow"><img src="<?=$URL_ROOT?>assets/img/all/logo-iberdrola.png" width="200" alt="Iberdrola"></a>
          </div>
        </article>
    </div>

    <div class="container">
      <article class="banner mt-2 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/consortium/bg-activities.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("SEE ACTIVITIES");?></h3>
          <p class="text-white"><?=__("See activities and developments in projects");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/" class="btn btn-corporate1 shadow"><?=__("ACTIVITIES");?></a>
      </article>
    </div>
  </main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>