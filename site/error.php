<?
if (!isset($error)){
	$error = isset($_GET['error'])?$_GET['error']:'';
}
//si el error se produce por una imagen, css, js o fichero que no es una página, lo avisamos
if (!preg_match('/(\.php|\.html|\/)$/', $_SERVER['REQUEST_URI'])){
	$error_page = 'nohtml';
}
if ($error!='404'){
	$error = '404fired';
}
if (!isset($PRE)){
	$PRE = '../';
}
require_once($PRE."php/init.php");
$title=$txt->errors->error400->title;
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="error" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior" >
<?require("{$DOC_ROOT}site/includes/header.php")?>
<main>
	<div class="full-container">
		<div class="container">
			  <article class="hero infografiaAreas d-flex align-items-end">
				<div class="row">
					<div class="col-md-12">
						<h1><?=$txt->errors->error400->h1?></h1>
						<p class="subtitle"><?=$txt->errors->error400->subtitle?></p>
						<p class="link"><a href="<?=$URL_ROOT_BASE?>/" class="btn btn-primary btn-sm"><span><?=$txt->errors->error400->link?></span></a></p>
						<div class="embed-responsive embed-responsive-16by9">
							<!--<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/8_GI_S3V8nw?rel=0"></iframe>-->
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>