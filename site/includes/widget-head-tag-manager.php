<?if (defined('GOOGLETAGMANAGER_KEY') AND GOOGLETAGMANAGER_KEY!=''){?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?=GOOGLETAGMANAGER_KEY;?>');</script>
<!-- End Google Tag Manager -->
<?} elseif (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){ ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?=GOOGLEANALYTICS_KEY;?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?=GOOGLEANALYTICS_KEY;?>');
</script>
<?}?>
