<?
require_once("../php/init.php");
require_once("../site/php/inc-functions.php");
require_once("../site/php/inc-index.php");
$title=__("Home - WIND2GRID");
$description=__("WIND2GRID - Country's First Offshore Floating Substation");
$js[]="ekko-lightbox.min.js";
$js[]="lightbox.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="home" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
<?require("{$DOC_ROOT}site/includes/header.php")?>
    <main role="main">
      <div class="full-container">
        <div class="container">
          <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/home/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-1">
                    <h1 class="pl-1 pl-lg-5"><span class="small">WIND2GRID</span><?=__("AN INNOVATIVE CONCEPT OF <span class='orange'>INTEGRATED FLOATING SUBSTATION</span> DEVELOPED IN BASQUE COUNTRY");?></h1>
                    <p class="contact"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->contacto->url?>/" class=" pl-1 pl-md-5"><?=__("CONTACT");?></a></p>
                    <p class="scroll data"><?=__("Scroll");?></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>

    <div class="full-container bloques show-on-scroll">
      <div class="container pt-2 pb-3 pt-md-4 pt-lg-4 pb-md-3">
        <article class="pl-1 pr-1 pl-md-3 pr-md-3">
          <div class="row">
            <div class="col-lg-6 pl-md-3 align-self-center">
              <h2><?=__("THE PROJECT");?></h2>
              <p class="introDestacado darkBlue p-0"><?=__("Collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating substation.");?></p>
              <div class="mt-2 mb-2 mt-md-2 mt-lg-3">
                <a href="<?=$URL_ROOT_BASE?>/<?=$txt->project->url?>/" class="btn btn-corporate1 shadow"><?=__("THE PROJECT");?></a>
              </div>
           </div>
          </div>
          <div class="row mt-md-2 mt-lg-4 bg-corporate-inverse">
            <div class="col-lg-6 p-0 col-sm-12">
              <img src="<?=$URL_ROOT?>assets/img/home/challenge2.jpg" class="">
            </div>
            <div class="col-lg-6 col-sm-12 p-2 p-lg-4 align-self-center">
              <h2 class="text-white pt-md-4"><?=__("FLOATING OFFSHORE SUBSTATIONS, AN EXCELLENT OPPORTUNITY");?></h2>
              <p class="text-white mt-2 intro"><?=__("At present, there are no commercial floating substations in operation. Accordingly, the market opportunity that will be created by developing floating and fixed offshore wind power at increasing depths goes hand-in-hand with a lack of developers with recognised experience in the field. Together, this creates a unique and unmissable window of opportunity, considering the wealth of experience in offshore wind power technology that we have accumulated in the Basque Country.");?></p>
              <div class="mt-2 mb-2 mt-md-2 mt-lg-3">
                <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->challenge->url?>/" class="btn btn-corporate1 shadow"><?=__("THE CHALLENGE");?></a>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>



    <div class="container pb-md-5 pl-md-3 pr-md-3 bloques show-on-scroll">

      <div class="row">
        <div class="col-lg-4 pl-lg-4 pt-md-4">
         <h2 class=""><?=__("CONSORTIUM");?></h2>
          <p><?=__("The following participants are involved in this project:");?></p>
          <p class="mt-3 mb-3"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->consortium->url?>/" class="btn btn-corporate1 shadow"><?=__("THE CONSORTIUM");?></a></p>
        </div>
        <div class="col-12 col-lg-8 pt-md-3">
          <ul class="row list-unstyled text-center">
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="https://www.idom.com/sector/energia/offshore-marina/" title="IDOM" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-idom-home.png" width="180" alt="IDOM"><br /><span class="data darkBlue border-top mt-1 pt-1"><?=__("COORDINATOR");?></span></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="http://blug.es/" title="Blug - Credeblug s.l." target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-blug.png" width="180" alt="Blug - Credeblug s.l."></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="http://www.errekafasteningsolutions.com/en/" title="Erreka - Fastening Solutions" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-erreka.png" width="180" alt="Erreka - Fastening Solutions"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="http://www.galvasala.com/" title="Galvasala" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-galvasala.png" width="180" alt="Galvasala"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="http://www.astillerosmurueta.com/" title="Murueta - Astilleros-Shipyards" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-murueta.png" width="160" alt="Murueta - Astilleros-Shipyards"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://www.nautilusfs.com/" title="Nautilus" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-nautilus.png" width="160"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://www.navacel.com/" title="Navacel" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-navacel.png"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://www.ogerco.com/" title="Ogerco" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-ogerco.png" width="200"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://pine.zimacorp.es/" title="Pine" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-pine.png" width="160"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://viudadesainz.com/" title="Viuda de Sainz" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-viuda-de-sainz.png" width="160"></a></li>
          </ul>
          <h3 class="introDestacado pt-lg-3 darkBlue border-bottom"><?=__("Technology centres");?></h3>
          <ul class="row list-unstyled text-center">
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="https://www.tecnalia.com/" title="Tecnalia" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-tecnalia.svg" width="180"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="https://www.bcmaterials.net/" title="BC Materials" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-bcmaterials.png" width="180"></a></li>
          </ul>
          <h3 class="introDestacado pt-lg-3 darkBlue border-bottom"><?=__("Expert Technical Advisors");?></h3>
          <ul class="row list-unstyled text-center">
            <li class="col-4 pl-lg-2 pt-2 pb-lg-3 align-self-center grow"><a class="grow" href="https://www.iberdrola.es/" title="Iberdrola" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-iberdrola.png" width="150" alt="Iberdrola"></a></li>
          </ul>
          <h3 class="introDestacado pt-lg-3 darkBlue border-bottom"><?=__("Clusters");?></h3>
          <ul class="row list-unstyled text-center">
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="http://www.clusterenergia.com/" title="<?=__("Basque Energy Cluster");?>" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-cluster-energia.svg" width="180"></a></li>
            <li class="col-4 pl-lg-2 pt-2 pb-lg-2 align-self-center grow"><a class="grow" href="http://foromaritimovasco.com/es/" title="<?=__("The Basque Maritime Forum");?>" target="_blank"><img src="<?=$URL_ROOT?>assets/img/all/logo-foro-maritimo-vasco.png" width="180"></a></li>
          </ul>
        </div>
      </div>
      </div>
    </div>


    <div class="full-container bg-contenido-1">
          <div class="container pt-5 bloques show-on-scroll">
            <article class="project pb-1 pl-1 pr-1 pb-md-3 pl-md-3 pr-md-3">
              <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-4 ml-sm-2 ml-md-3 ml-lg-0 mb-4">
                  <h2><?=__("ACTIVITIES<br>AND NEWS");?></h2>
                  <p><?=__("Keep up to date with all the latest activities and results from the project and developments in the sector.");?></p>
                  <a href="<?=$URL_ROOT_BASE?>/<?=$txt->news->url?>/" class="btn btn-corporate1 shadow"><?=__("ALL THE NEWS");?></a>
                </div>
                <div class="col-lg-3">
                  <?if (!empty($last_newsevents)){?>
                    <ul class="list-unstyled">
                    <?foreach($last_newsevents as $current){?>
                      <li class="bg-white shadow pt-2 pr-2 pl-3 pb-4 mb-2">
                        <a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$current['type']}->url?>/<?=$current["slug"]?>/" class="news-home">
                          <span><?=$current['headline'];?></span>
                        </a>
                      </li>
                    <?}?>
                    </ul>
                   <?}?>
                </div>
                <div class="col-lg-5">
                  <div id='scoopit-container-3997583'>
                      <a href='https://www.scoop.it/topic/eolicaoffshore'>Eólica + Offshore</a>
                    </div>
                    <script type="text/javascript">
                     var scitOpt = {
                      displayInsight: "no",
                      removeHeadingInHtml: "yes",
                      removeScoopItFont: "yes",
                      nbPostPerPage: 2
                     };
                    </script>
                    <script type='text/javascript' src='https://www.scoop.it/embed-full-topic/3997583.js' ></script>
                </div>
              </div>
            </article>
          </div>
    </div>


    <div class="container">
      <article class="banner mt-5 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/home/bg-contact.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("CONTACT WIND2GRID");?></h3>
          <p class="text-white"><?=__("Contact us for further information or if you wish to collaborate with us or send us your feedback.");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->contacto->url?>/" class="btn btn-corporate1 shadow"><?=__("Contact");?></a>
      </article>
    </div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>