<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-news.php");
$title = __("Activities - WIND2GRID");
$description = __("Keep up to date with our latest news.");
$keywords = __("news");
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
?>
<?require("{$DOC_ROOT}site/includes/head.php");
 
?>
<body id="news" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?>">
	<?require("{$DOC_ROOT}site/includes/header.php")?>
	<main>
	<div class="full-container">
		<div class="container">
			<article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/news/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("ACTIVITIES");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
		</div>
	</div>
    <div class="full-container bg-gray mt-2 pt-3 pt-md-2 pb-3">
    	<div class="container box">
    		<section class="">
				<?if(!empty($front_news)){?>
				<div class="row d-flex justify-content-center">
					<?foreach ($front_news as $ind => $article){?>
						<div class="col-sm-5 ml-1 mr-1">
							<a class="" href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/">
							<article class="item-news bg-white shadow grow">
								<div class="object">
									<?if ($article["image1"]){?>
										<p class="photo"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><img class="img-responsive" src="<?=$URL_ROOT?>uploads/news/<?=$article["image1"]?>" alt="<?=($article["alt-image1"]?htmlspecialchars(strip_tags($article["alt-image1"])):str_replace("\"","'", $article["headline"]))?>" /></a></p>
									<?}?>
									<div class="content pt-3 pr-3 pl-3">
										<p class="data"><span <?if($article['type']=='eventos' AND !empty($article["date"])){?>class="date2"<?}?>><?=parsedate($article["date"],$language);?></span><?if($article['type']=='eventos' AND !empty($article["location"])){?> <span class="place"><?=$article["location"];?></span><?}?></p>
										<h2 class="title"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><?=$article["headline"]?></a></h2>
									</div>
								</div>
								<p class="hidden-xs pr-3 pb-3 pl-3"><?=$article["intro"]?><br /><br /><a class="link display-block orange" href="<?=$URL_ROOT_BASE?>/<?=$txt->{$article['type']}->url?>/<?=$article["slug"]?>/"><?=__("Read more");?> <i class="fa fa-angle-right"></i></a></p>
							</article>
						</a>
						</div>
					<?if(($ind%2)==1 AND $ind!=$lastArticleIndex){?>
				</div>
				<div class="row d-flex justify-content-center">
						<?}?>
					<?}?>
				</div>
				<?}?>
				<?if ($actual_page>1 or $actual_page<$total_pages){?>
				<div class="row">
					<div class="col-md-12">
						<nav>
							<ul class="pager list-unstyled text-center mt-3 mt-md-4">
							<? if ($actual_page>1){?><li class="previous"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$s1}->url?>/<?if($s2=='past'){?><?=$txt->{$s1}->{$s2}->url;?>/<?}?><?if (($actual_page-1)>1){echo ($actual_page-1).'/';};?>" role="button"><i class="fa fa-angle-left"></i> <?=__("PREVIOUS");?></a></li><?}?>
							<? if ($actual_page<$total_pages){?><li class="next"><a href="<?=$URL_ROOT_BASE?>/<?=$txt->{$s1}->url?>/<?if($s2=='past'){?><?=$txt->{$s1}->{$s2}->url;?>/<?}?><?=($actual_page+1);?>/" role="button"><?=__("NEXT");?> <i class="fa fa-angle-right"></i></a></li><?}?>
							</ul>
						</nav>
					</div>
				</div>
				<?}?>
			</section>
		</div> <!-- /.container -->
	</div> <!-- /.full-container -->

    <div class="container">
      <article class="banner mt-2 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/news/bg-actividades.jpg);background-size: cover; background-position: center bottom">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("SEE LATEST NEWS");?></h3>
          <p class="text-white"><?=__("See all the sector news");?></p>
        </div>
        <a  href="<?=$URL_ROOT_BASE?>/<?=$txt->scoopitnews->url?>/" class="btn btn-corporate1 shadow"><?=__("ALL LATEST NEWS");?></a>
      </article>
    </div>
</main>
<?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>