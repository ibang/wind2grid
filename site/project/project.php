<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Project - WIND2GRID");
$description=__("6 Basque companies have decided to join forces in the WIND2GRID project to co-ordinate and find synergies in the development of new offshore solutions");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="project" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure mb-sm-0">
                    <img src="<?=$URL_ROOT?>assets/img/project/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("THE PROJECT");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>

    <!-- PAQUETES DE TRABAJO -->

    <div class="full-container">
      <div class="container">
        <article class="mt-1 mt-md-4 pl-1 pr-1 pl-md-3 pr-md-3">
          <div class="row bloques show-on-scroll">
            <div class="col-md-7 mb-4">
              <p class="intro darkBlue pr-1 pr-lg-3"><?=__("<strong>WIND2GRID</strong> aims to promote collaborative research and development integrating technologies, components and systems for innovative offshore substations, based on the development of a floating offshore substation.");?></p>
            </div>
          </div>
          <div class="row bg-gray p-2 p-md-3">
            <h2><?=__("Main aims");?></h2>
            <ul class="list-arrow columnas">
              <li><?=__("To develop new floating structure concepts valid for multiple locations, including shallow (60-100 m) and deep (around 1000 m) waters.");?></li>
              <li><?=__("Advanced numerical modelling applied to research into the hydrodynamic behaviour of complex structures and dynamic umbilical cables.");?></li>
              <li><?=__("Research into the dynamics of the mooring systems of large structures at varying depths.");?></li>
              <li><?=__("Adaptability to future networks, research into novel power transmission systems (HVAC, HVDC).");?></li>
              <li class="avoid"><?=__("Research into specific high-performance concrete for marine structures.");?></li>
              <li><?=__("Research into hybrid coatings for rust-proofing metal components.");?></li>
              <li><?=__("Implantation of Lean Manufacturing concepts and BIM methodologies for large marine structures.");?></li>
              <li><?=__("Innovative submarine cable repair systems to reduce O&M costs.");?></li>
              <li><?=__("Augmented reality and virtual reality concepts to optimise O&M (developing a virtual model of the substation).");?></li>
            </ul>
          </div>
          <div id="wp" class="mb-md-5 pb-md-4"></div>
          <div class="packageChart mt-3 bloques show-on-scroll flex-column">
            <h2 class="text-center mb-3 mt-3 mt-md-5"><?=__("Work packages");?></h2>
            <div class="row justify-content-center wp1">
              <div class="col-md-6 mb-3">
                  <a href="#wp0" class="scroll alig-self-center">
                  <div class="row bg-white shadow justify-content-center work-packs">
                      <img class="text-right logos col-lg-4 ml-2 pt-2 pt-lg-0 pt-md-2" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" width="143" />
                      <div class="col-lg-8 pl-2 pt-lg-2 pr-1 pb-lg-2 text-left">
                        <p><?=__("<h3 class='introDestacado'><span class='workBlue'>WP0.</span> Project coordination</h3>");?></p>
                      </div>
                  </div>
                </a>
              </div> <!-- /col-md-12 -->
            </div>

              <ul class="row list-unstyled justify-content-center columnas">
                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-1">
                  <a href="#wp1" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-3 mt-1 pb-1" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" width="143" />
                        <div class="col text-left pb-1 pr-3 pr-md-0 ml-1 mb-1">
                          <p><?=__("<span class='orange'><strong>WP1.</strong></span> Design conditions and requirement specification");?></p>
                        </div>
                    </div>
                  </a>
                </li>

                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-3">
                  <a href="#wp2" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-4 mt-1 pb-1 pl-2" src="<?=$URL_ROOT?>assets/img/all/logo-nautilus.png" alt="Nautilus" />
                        <div class="col text-left pb-1 pr-3 pr-md-0 ml-1 mb-1">
                          <p><?=__("<span class='orange'><strong>WP2.</strong></span> Conceptual development of floating substation");?></p>
                        </div>
                    </div>
                  </a>
                </li>

                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-5">
                  <a href="#wp3" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-4 mt-1 pb-1 pl-2" src="<?=$URL_ROOT?>assets/img/all/logo-galvasala.png" alt="Galvasala" />
                        <div class="col text-left pb-1 pr-3 pr-md-0 ml-1 mb-1">
                          <p><?=__("<span class='orange'><strong>WP3.</strong></span> New materials and antidegradation systems");?></p>
                        </div>
                    </div>
                  </a>
                </li>

                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-4">
                   <a href="#wp4" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-3 mt-0 ml-1 pb-1" src="<?=$URL_ROOT?>assets/img/all/logo-navacel.png" alt="Navacel" />
                        <div class="col text-left ml-1 mb-1 pb-1 pr-3 pr-md-0">
                          <p><?=__("<span class='orange'><strong>WP4.</strong></span> Advanced manufacturing and integrated logistics");?></p>
                        </div>
                    </div>
                  </a>
                </li>

                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-2">
                   <a href="#wp5" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-3 mt-1 ml-1 pb-1" src="<?=$URL_ROOT?>assets/img/all/logo-pine.png" alt="Pine" />
                        <div class="col text-left pb-1 pr-3 pr-md-0 ml-1 mb-1">
                          <p><?=__("<span class='orange'><strong>WP5.</strong></span> Innovations in O&M for offshore substations");?></p>
                        </div>
                    </div>
                  </a>
                </li>

                <li class="col-md-5 mr-ml mb-1 mb-lg-3 order-6">
                  <a href="#wp6" class="scroll alig-self-center">
                    <div class="bg-white shadow arrow-right position-relative work-packs pt-1">
                        <img class="col-3 mt-1 pb-1" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" width="143" />
                        <div class="col text-left pb-1 pr-3 pr-md-0 ml-1 mb-1">
                          <p><?=__("<span class='orange'><strong>WP6.</strong></span> Dissemination and exploitation of results");?></p>
                        </div>
                    </div>
                  </a>
                </li>
              </ul>

        </article>
      </div>
    </div>

    <div id="wp0"></div>

    <div class="full-container bg-gray pt-3 pt-md-4 pb-3 pb-md-4 mt-3 mt-md-5 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container pt-3">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP0. Project coordination");?></h3>
        <div class="row">
          <div class="col-md-3">
            <h4 class="data"><?=__("WP LEADER");?>: IDOM</h4>
            <a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank"><img class="mb-2 multiply grow" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" width="140" /></a>
            <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
            <ul class="list-arrow">
              <li><?=__("All");?></li>
            </ul>
          </div>
          <div class="col-md-7 offset-md-1">
            <div class="row d-flex">
              <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp0.svg" width="250" class="col-md-4" />
              <div class="col-lg ml-lg-3 mt-lg-3 mt-2">
                <p><?=__("The purpose of the work package is to ensure the achievement of the technical objectives set and to monitor the progress of the project.");?></p>
                <p><?=__("The technical progress of the project will be monitored to ensure the achievement of the objectives and the economic monitoring of the project's development to avoid significant deviations in the budget.");?></p>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
        
          <div class="w-100 text-right position-sticky fixed-bottom mt-4"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>

    <div id="wp1"></div>

    <div class="full-container bg-gray pt-3 pt-md-4 pb-3 pb-md-4 mt-3 mt-md-5 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container pt-3">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP1. Design conditions and requirement specification");?></h3>
        <div class="row">
          <div class="col-md-3">
            <h4 class="data"><?=__("WP LEADER");?>: IDOM</h4>
            <a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank"><img class="mb-2 multiply grow" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" width="140" /></a>
            <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
            <ul class="list-arrow">
              <li><?=__("All");?></li>
            </ul>
          </div>
          <div class="col-md-7 offset-md-1">
            <div class="row d-flex">
              <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp1.svg" width="350" />
              <div class="col-lg ml-lg-3 mt-lg-3 mt-2">
                <p><?=__("The purpose of the first work package is to define the fundamentals of design to be implemented in all work packages.");?></p>
                <p><?=__("The fundamentals of design to be defined will enable: (i) addressing the development of different subcomponents; (ii) validating the design; and (iii) implementing it in case studies.");?></p>
              </div>
            </div>
          </div>
        </div><!-- /.row -->
        
          <div class="w-100 text-right position-sticky fixed-bottom mt-4"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>

    <div id="wp2"></div>
    
    <div class="container pt-lg-5 mt-lg-5 bloques show-on-scroll">
      <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP2. Conceptual development of floating substation");?></h3>
      <div class="row">
        <div class="col-md-3">
          <h4 class="data"><?=__("WP LEADER");?>: NAUTILUS</h4>
          <a href="https://www.ikerlan.es/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-nautilus.png" alt="Nautilus" width="150" /></a>
          <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
            <ul class="list-arrow">
              <li><a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank" title="Idom">IDOM</a></li>
              <li><a href="https://www.navacel.com/" target="_blank" title="Navacel">NAVACEL</a></li>
              <li><a href="https://pine.zimacorp.es/" target="_blank" title="Pine">PINE</a></li>
              <li><a href="http://www.astillerosmurueta.com/" target="_blank" title="Astilleros Murueta">ASTILLEROS MURUETA</a></li>
              <li><a href="http://www.errekafasteningsolutions.com/en/" target="_blank" title="Erreka">ERREKA</a></li>
              <li><a href="http://blug.es/" target="_blank" title="Credeblug">CREDEBLUG</a></li>
              <li><a href="https://www.iberdrola.es/" target="_blank" title="Iberdrola">IBERDROLA</a></li>
            </ul>
        </div>
        <div class="col-md-7 offset-md-1">
          <div class="row d-flex">
            <img src="<?=$URL_ROOT?>assets/img/project/gif-wp2.gif" class="rounded mt-md-3 col-md-4 text-center" />
            <div class="col-lg ml-lg-2 mt-lg-3 mt-2">
              <p><?=__("The purpose of this work package is to develop an innovative solution for a floating substation.");?></p>
              <ul>
                <li class="pb-1"><?=__("Based on the design conditions identified in WP1, a comprehensive design will be undertaken, including the floating structure, the topside and the mooring system.");?></li>
                <li class="pb-1"><?=__("The dynamics of interarray and export cables will be analysed to define their design conditions. The development of key ancillary elements, including access and evacuation systems, will be addressed.");?></li>
              </ul>
          </div>
        </div>
      </div>
      </div><!-- /.row -->
    <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
    </div>


    <div id="wp3"></div>


    <div class="full-container bg-gray mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP3. New materials and antidegradation systems");?></h3>
        <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: GALVASALA</h4>
              <a href="http://www.galvasala.com/" target="_blank"><img class="mb-2 grow multiply" src="<?=$URL_ROOT?>assets/img/all/logo-galvasala.png" alt="Galvasala" width="130" /></a>
              <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
              <ul class="list-arrow">
                <li><a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank" title="Idom">IDOM</a></li>
                <li><a href="http://www.nautilusfs.com/" target="_blank" title="Nautilus">NAUTILUS</a></li>
                <li><a href="https://www.iberdrola.es/" target="_blank" title="Iberdrola">IBERDROLA</a></li>
                <li><a href="https://www.ogerco.com/" target="_blank" title="Ogerco">OGERCO</a></li>
                <li><a href="http://www.errekafasteningsolutions.com/en/" target="_blank" title="Erreka">ERREKA</a></li>
                <li><a href="http://blug.es/" target="_blank" title="Credeblug">CREDEBLUG</a></li>
                <li><a href="https://viudadesainz.com/" target="_blank" title="Viuda de Sainz">VIUDA DE SAINZ</a></li>
              </ul>
            </div>
            <div class="col-md-7 offset-md-1">
              <div class="row d-flex">
                <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp3.svg" width="250" class="col-md-4" />
                <div class="col-lg ml-lg-2 mt-lg-3 mt-2">
                  <p><?=__("The purpose of this work package is to analyse new materials for use in offshore structures.");?></p>
                  <ul>
                    <li class="pb-1"><?=__("It will focus on two types of materials: coatings for the protection against offshore erosion, and new concretes for large offshore structures.");?></li>
                    <li class="pb-1"><?=__("In the case of coatings, the goal is the development and validation of a new coating.");?></li>
                    <li class="pb-1"><?=__("In parallel, work will be carried out on the development of ultra-high performance concrete in order to increase the resistance of large structures while reducing weight.");?></li>
                    <li class="pb-1"><?=__("In both cases, the ultimate goal is cost-reduction of offshore structures.");?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
        <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
      </div>
    </div>


    <div id="wp4"></div>


    <div class="full-container mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP4. Advanced manufacturing and integrated logistics");?></h3>
          <div class="row mb-lg-3">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: NAVACEL</h4>
              <a href="https://www.navacel.com/" target="_blank"><img class="mb-2 multiply grow" src="<?=$URL_ROOT?>assets/img/all/logo-navacel.png" alt="Navacel" /></a>
              <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
              <ul class="list-arrow">
                <li><?=__("All");?></li>
              </ul>
            </div>
            <div class="col-md-7 offset-md-1">
              <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp4.svg" class="mb-2" />
              <p><?=__("The purpose of this work package is to demonstrate the technical and financial viability of floating substation operations to transport topside.");?></p>
              <ul>
                <li class="pb-1"><?=__("It seeks to define the means and procedures needed to manufacture, transport and install a floating substation in each of the phases of development of a construction project.");?></li>
                <li class="pb-1"><?=__("The manufacture, installation and operation of the topside, the floating structure, the mooring, and the cables will condition the design. Such conditions must be identified in the early design stages so that the elements facilitating the operations are included from the beginning.");?></li>
              </ul>
            </div>
          </div><!-- /.row -->
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
      </div>
    </div>


    <div id="wp5"></div>


    <div class="full-container bg-gray mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP5. Innovations in O&M for offshore substations");?></h3>
          <div class="row mb-lg-3">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: PINE</h4>
              <a href="https://pine.zimacorp.es/" target="_blank"><img class="mb-2 grow" src="<?=$URL_ROOT?>assets/img/all/logo-pine.png" alt="Pine" width="140" /></a>
              <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
              <ul class="list-arrow">
                <li><a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank" title="Idom">IDOM</a></li>
                <li><a href="http://www.astillerosmurueta.com/" target="_blank" title="Astilleros Murueta">ASTILLEROS MURUETA</a></li>
                <li><a href="https://www.navacel.com/" target="_blank" title="Navacel">NAVACEL</a></li>
                <li><a href="http://www.nautilusfs.com/" target="_blank" title="Nautilus">NAUTILUS</a></li>
                <li><a href="https://www.iberdrola.es/" target="_blank" title="Iberdrola">IBERDROLA</a></li>
                <li><a href="https://www.ogerco.com/" target="_blank" title="Ogerco">OGERCO</a></li>
                <li><a href="http://www.errekafasteningsolutions.com/en/" target="_blank" title="Erreka">ERREKA</a></li>
                <li><a href="http://blug.es/" target="_blank" title="Credeblug">CREDEBLUG</a></li>
                <li><a href="https://viudadesainz.com/" target="_blank" title="Viuda de Sainz">VIUDA DE SAINZ</a></li>
              </ul>
            </div>
            <div class="col-md-7 offset-md-1">
              <div class="row d-flex">
                <img class="rounded col-md-3 mt-md-3" src="<?=$URL_ROOT?>assets/img/project/grafico-wp5.png" />
                <div class="col-lg ml-lg-2 mt-lg-3 mt-2">
                  <p><?=__("The final purpose of this work package is cost-reduction. In this case, it will be achieved through the improvement of different activities related to the operation and maintenance phase:");?></p>
                  <ul>
                    <li class="pb-1"><?=__("Development and validation of an undersea tool for repairing offshore cables, which would imply a cost reduction in state-of-the-art technology. The goal is to develop a concept tool and a set of manoeuvres that will help reduce the work time.");?></li>
                    <li class="pb-1"><?=__("Reduction of installation time lines and provision of greater traceability of the devices installed by means of the development of a real-time management logic platform.");?></li>
                    <li class="pb-1"><?=__("Production of a virtual model of a floating substation allowing systems and subsystems to be visualised, so it can be used as tool for maintenance optimisation.");?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div><!-- /.row -->

          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>


    <div id="wp6"></div>


    <div class="full-container mt-3 mt-md-5 pt-3 pt-md-5 pb-3 pb-md-4 mb-3 mb-md-4 bloques show-on-scroll">
      <div class="container">
        <h3 class="work mb-3 col-10 col-lg-7 orange"><?=__("WP6. Dissemination and exploitation of results");?></h3>
          <div class="row">
            <div class="col-md-3">
              <h4 class="data"><?=__("WP LEADER");?>: IDOM</h4>
              <a href="https://www.idom.com/sector/energia/offshore-marina/" target="_blank"><img class="mb-2 multiply grow" src="<?=$URL_ROOT?>assets/img/all/logo-idom.png" alt="Idom" /></a>
              <p class="small-2 border-bottom"><?=__("Participants");?>:</p>
              <ul class="list-arrow">
                <li><?=__("All");?></li>
              </ul>
            </div>
            <div class="col-md-9">
              <div class="row d-flex">
                <img src="<?=$URL_ROOT?>assets/img/project/grafico-wp6.svg" width="550" />
                <div class="col-lg ml-lg-2 mt-lg-3 mt-2">
                  <p><?=__("This will contribute to place the Consortium members as well as Basque companies at the technological and industrial forefront of the power sector, both in the Basque Country and internationally.");?></p>
                  <p><?=__("A plan to exploit the project results will be defined and launched. This plan will focus on technological development, as well as facilitating training for project partners and Basque supply chain participants in the offshore wind power sector.");?></p>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="w-100 text-right position-sticky fixed-bottom"><a href="#wp" class="goUp scroll"><?=__("Scroll Up");?></a></div>
        </div>
      </div>
    </div>


    <div class="container pb-2">
      <article class="banner mt-1 mb-3 p-2 p-md-4" style="background-image: url(<?=$URL_ROOT?>assets/img/project/bg-consorcio.jpg);background-size: cover; background-position: center">
        <div class="text">
          <h3 class="text-gray text-white"><?=__("FURTHER INFORMATION ABOUT THE CONSORTIUM");?></h3>
          <p class="text-white"><?=__("Information related to the companies participating in the consortium and their role");?></p>
        </div>
        <a href="<?=$URL_ROOT_BASE?>/<?=$txt->consortium->url?>/" class="btn btn-corporate1 shadow text-left"><?=__("THE CONSORTIUM");?></a>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>