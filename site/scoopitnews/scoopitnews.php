<?
require_once("../../php/init.php");
require_once("../../site/php/inc-functions.php");
require_once("../../site/php/inc-index.php");
$title=__("Project - DAEKIN");
$description=__("News about the DAEKIN sector");
$js[]="ekko-lightbox.min.js";
$js[]="historia.js";
$js[]="about.js";
if (defined('GOOGLEANALYTICS_KEY') AND GOOGLEANALYTICS_KEY!=''){
	$js[]="events.js";
}
$css[]="historia.css";
?>
<?require("{$DOC_ROOT}site/includes/head.php")?>
<body id="nosotros" class="<?=substr($language,0,2)?><?=(($langURL!=substr($language,0,2))?' '.$langURL:'')?> interior">
  <?require("{$DOC_ROOT}site/includes/header.php")?>
  <main>
    <div class="full-container">
      <div class="container">
        <article class="hero">
              <div class="row w-100 no-gutters">
                <div class="col-lg-12">
                  <figure class="figure">
                    <img src="<?=$URL_ROOT?>assets/img/sectorialnews/hero.jpg" class="">
                  </figure>
                  <div class="text-box pr-2">
                    <h1 class="pl-1 pl-lg-4"><?=__("SECTOR NEWS");?></h1>
                    <p class="scroll data"></p>
                  </div>
                </div>
            </div>
          </article>
      </div>
    </div>
    <div class="container">
      <article class="scoopit pl-1 pr-1 pl-md-3 pr-md-3">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div id='scoopit-container-6963037'>
              <a href='https://www.scoop.it/topic/proyecto-harsh'>Noticias eólica offshore</a>
            </div>
            <script type="text/javascript">
             var scitOpt = {
              displayInsight: "no",
              removeHeadingInHtml: "yes",
              removeScoopItFont: "yes",
              nbPostPerPage: 8
             };
            </script>
            <script type='text/javascript' src='https://www.scoop.it/embed-full-topic/6963037.js' ></script>
          </div>
          </div>
      </article>
    </div>
  </main>
  <?require("{$DOC_ROOT}site/includes/footer.php")?>
</body>
</html>